$(document).ready(function(){ 

	$('#testimonial-owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		autoplay:false,
		nav:false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});



   
    // -------------------- Navigation Scroll
    $(window).scroll(function() {    
      var sticky = $('.header'),
      scroll = $(window).scrollTop();
      if (scroll >= 190) sticky.addClass('fixed');
      else sticky.removeClass('fixed');

    });

});






