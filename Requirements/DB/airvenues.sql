-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 27, 2018 at 12:45 AM
-- Server version: 5.6.39-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airvenues`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_account`
--

CREATE TABLE `bank_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_holder_name` varchar(255) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `swift_code` varchar(100) NOT NULL,
  `zip_code` varchar(100) NOT NULL,
  `bussiness_email` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_account`
--

INSERT INTO `bank_account` (`id`, `user_id`, `bank_name`, `account_holder_name`, `account_number`, `swift_code`, `zip_code`, `bussiness_email`, `created_at`, `updated_at`) VALUES
(0, 49, 'ICICI', 'Kamlesh', '97987987987', '3455', '201012', 'kamlesh@gmail.com', '2018-07-25 14:38:29', '2018-07-25 14:38:37');

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE TABLE `mail` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mail`
--

INSERT INTO `mail` (`id`, `fname`, `lname`, `email`, `phone`, `location`, `created_at`) VALUES
(4, 'kamelsh', 'Kumar', 'kamleshwar.yadav@niletechnologies.com', '9897987987', 'India', '2018-07-13 14:09:21'),
(5, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:01'),
(6, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:06'),
(7, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:11'),
(8, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:12'),
(9, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:12'),
(10, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:13'),
(11, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:13'),
(12, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:13'),
(13, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:13'),
(14, 'Chris', 'Nash', 'bearrnash@comcast.net', '3031231234', 'Denver', '2018-07-13 14:48:13'),
(15, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:24:14'),
(16, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:24:31'),
(17, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:24:33'),
(18, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:24:45'),
(19, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:15'),
(20, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:15'),
(21, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:18'),
(22, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:18'),
(23, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:27'),
(24, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:28'),
(25, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:28'),
(26, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:33'),
(27, 'Jody', 'Brown', 'telluridejody@gmail.com', '9705191844', 'Telluride', '2018-07-13 18:25:33'),
(28, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:19'),
(29, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:23'),
(30, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:24'),
(31, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:27'),
(32, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:27'),
(33, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:27'),
(34, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:28'),
(35, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:29'),
(36, 'Jody', 'Brown', 'telluridejody@gmail.com', '970-519-1844', 'Telluride', '2018-07-13 18:26:29'),
(37, 'Lauren', 'Thomason', 'mackinnon.laurenm@gmail.com', '3039123225', 'Portland', '2018-07-20 03:36:16'),
(38, 'Lauren', 'Thomason', 'mackinnon.laurenm@gmail.com', '3039123225', 'Portland', '2018-07-20 03:36:17'),
(39, 'Lauren', 'Thomason', 'mackinnon.laurenm@gmail.com', '3039123225', 'Portland', '2018-07-20 03:36:21'),
(40, 'Lauren', 'Thomason', 'mackinnon.laurenm@gmail.com', '3039123225', 'Portland', '2018-07-20 03:36:26'),
(41, 'Alan', 'Brown', 'a.lamont@bresnan.net', '(719)275-4041', 'Canon City, CO 81212', '2018-07-26 16:12:27');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `subscription_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subscription_code` varchar(6) NOT NULL,
  `subscription_status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0:Inactive; 1:Active',
  `subscription_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `payment_amount` decimal(6,2) NOT NULL,
  `payment_status` varchar(50) NOT NULL,
  `payment_date` varchar(50) NOT NULL,
  `order_status` enum('0','1','2','3','4','5','6') NOT NULL COMMENT '0:Pending, 1:Processing, 2:On-Hold, 3:Completed, 4:Cancelled, 5:Refunded, 6:Failed',
  `order_qty` int(11) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_group` enum('1','2','3') NOT NULL DEFAULT '3' COMMENT '1:Admin, 2:Vendor, 3:User',
  `phone` varchar(15) NOT NULL,
  `otp` varchar(15) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `address` varchar(200) NOT NULL,
  `description` varchar(255) NOT NULL,
  `account_status` enum('0','1','2','3','4') NOT NULL DEFAULT '0' COMMENT '0=>Pending,2=>Verification Vending;3=>Approved, 4=>Deleted',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL,
  `latitude` varchar(30) NOT NULL,
  `longitude` varchar(30) NOT NULL,
  `profile_image` varchar(244) NOT NULL,
  `profile_thumbnail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `hash`, `password`, `user_group`, `phone`, `otp`, `device_id`, `address`, `description`, `account_status`, `created_date`, `updated_date`, `latitude`, `longitude`, `profile_image`, `profile_thumbnail`) VALUES
(4, 'Arun', '', 'arunverma1090@gmail.com', '', '', '1', '9873521653', '98888', '457444', '', '', '3', '2018-06-14 19:42:14', '2018-06-14 00:00:00', '', '', '', ''),
(5, 'Arun', 'Verma', 'arun.kumar@niletechnologies.com', '', '08232b2cab4ec18fd8626b9727bc9d08', '3', '+91-9873521653', '', '', 'New Delhi, India', '', '2', '2018-06-18 17:15:21', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/vr-profile.jpg', ''),
(6, 'Shivendra', 'Tiwari', 'shivendra.tiwari@niletechnologies.com', '', '59af8c94e54483c98be28e29b0229901', '3', '7376996812', '', '', '', '', '2', '2018-06-19 11:51:56', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/pro1.jpg', ''),
(7, 'Atul ', 'Kapoor', 'atul@niletechnologies.com', '', 'ef60a8ce2fb7cdb8d6fb33e2466e8bb5', '3', '', '', '', '', '', '2', '2018-06-23 00:16:32', '0000-00-00 00:00:00', '', '', '', ''),
(11, 'Nile', 'Technologies', 'technologiesnile@gmail.com', '', '93af31c57e6341d1a24d37072727fba1', '3', '', '', '', '', '', '2', '2018-06-25 07:17:58', '0000-00-00 00:00:00', '', '', '', ''),
(13, 'Arun', 'Kumar', 'arunverma910@yahoo.com', '02665e540894ace9ba6e9ae54ad78613', '795c4dd093f2a3de549f454cbee30492', '3', '', '', '', '', '', '2', '2018-06-25 22:40:58', '0000-00-00 00:00:00', '', '', '', ''),
(16, 'Aditi', 'Verma', 'aditi.verma.india@gmail.com', 'd87a2929c314fe7032dd26f55a8add62', '62116b59e23f0214d56c0672235e4017', '3', '', '', '', '', '', '2', '2018-06-25 22:54:33', '0000-00-00 00:00:00', '', '', '', ''),
(21, 'Atul', 'Kapoor', 'kapooratul@yahoo.com', '5bc12c6fb8b043c00a385bab70595958', 'c41a2e89463a0a40c0e01989391b6d49', '3', '', '', '', '', '', '2', '2018-06-26 05:54:01', '0000-00-00 00:00:00', '', '', '', ''),
(22, 'Nile', 'Technologies', 'technologiesnile@gmail.com', '207aabaeca6ee2dc6fed826459aa89d9', 'ae83964bffa89e8200b02a916687fda6', '3', '', '', '', '', '', '2', '2018-06-27 03:20:29', '0000-00-00 00:00:00', '', '', '', ''),
(24, 'Richard', 'Jebb', 'richard@wordsthat-work.co.uk', '7c8f2b24c21024f118640ba11520c84a', 'ed8646f21726551689efd50788d3c4a4', '2', '', '', '', '', '', '3', '2018-06-28 05:31:44', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/please_buy_me_food.jpg', ''),
(25, 'Isaiah', 'Thomason', 'straightike@gmail.com', '06732085c1ecd6f4863c7b5f44412e89', '13c3658ee3d0df6fe7d939481d1d954b', '2', '', '', '', '', '', '3', '2018-06-28 13:46:53', '0000-00-00 00:00:00', '', '', '', ''),
(26, 'Lauren', 'Thomason', 'Lthomason@airvenues.com', 'ee03d99660e7fc0dc4b72d016a455666', '4d2180336d46358dbeac6eeef9028ff0', '2', '', '', '', '', '', '3', '2018-06-28 15:14:51', '0000-00-00 00:00:00', '', '', '', ''),
(27, 'Lauren', 'Thomason', 'mackinnon.laurenm@gmail.com', '48e94e6085f10d700d3f1fca8096d660', '4d2180336d46358dbeac6eeef9028ff0', '2', '', '', '', '', '', '3', '2018-06-28 17:20:30', '0000-00-00 00:00:00', '', '', '', ''),
(28, 'Alaina', 'Nash', 'anash@jphllc.com', 'bdf4c90083a8015a6e78294ed5a96d41', 'f8136d70db30d3460a41b9e74238517b', '2', '(303) 968-9560', '', '', '10650 W. 74th Pl', 'Enthusiastic, entrepreneur seeking opportunities to make additional income to fund my hobbies!', '3', '2018-06-28 21:36:01', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/alaina_pic.jpg', ''),
(29, 'Bear', 'Nash', 'bearrnash@comcast.net', '26ef38e845a12dc56729354365893157', '4ae516f10fafd6d2fdcce33c5f31ed8c', '3', '', '', '', '', '', '2', '2018-06-28 21:53:04', '0000-00-00 00:00:00', '', '', '', ''),
(30, 'Bear', 'Nash', 'bearrnash@comcast.net', '26ef38e845a12dc56729354365893157', '4d0fd18d2e991c336077126df52bce69', '3', '', '', '', '', '', '2', '2018-06-28 21:56:57', '0000-00-00 00:00:00', '', '', '', ''),
(31, 'Patrick', 'Horn', 'fsupatrick@gmail.com', 'e5aaa5ed81c5a7d85bade81a5b94c93e', 'f12d4f7740e962369dfc73fdf5228fe2', '2', '', '', '', '', '', '3', '2018-06-29 04:46:33', '0000-00-00 00:00:00', '', '', '', ''),
(32, 'Alan', 'Brown', 'a.lamont@bresnan.net', 'd7f2cf88d9bb50cfd5728f4e6ba6a6c8', '72f25f11fece2272a390e3a494aee91f', '2', '', '', '', '', '', '3', '2018-07-01 09:15:07', '0000-00-00 00:00:00', '', '', '', ''),
(33, 'Anita', 'B', 'Aballeby@yahoo.com', '68584af89f2b3db62c1be34cc5341470', '9819f2f47a82e2017b81cc43815493e5', '2', '', '', '', '', '', '3', '2018-07-02 07:03:33', '0000-00-00 00:00:00', '', '', '', ''),
(34, 'Jody', 'Brown', 'telluridejody@gmail.com', '1b456c4493d6672be8aed6aa118b1e97', '3b43ebd6be4cd74bbc5ba6930a967e90', '2', '', '', '', '', '', '3', '2018-07-04 11:29:43', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/Koda.jpg', ''),
(35, 'Nile', 'Technologies', 'technologiesnile@gmail.com', '207aabaeca6ee2dc6fed826459aa89d9', '78a3370e747ebef1d215f230901550dd', '3', '', '', '', '', '', '2', '2018-07-06 09:42:02', '0000-00-00 00:00:00', '', '', '', ''),
(39, 'Jerrod', 'Dawson', 'OUFrogger@yahoo.com', '2692a30aaeb26638aa8882b896d8f1b6', 'cc06a5a5de235516a65b7a11964ffbd4', '2', '', '', '', '', '', '3', '2018-07-10 04:55:15', '0000-00-00 00:00:00', '', '', '', ''),
(40, 'Arun', 'Kumar', 'arunverma910@yahoo.com', '02665e540894ace9ba6e9ae54ad78613', 'a6840431e150aefa61832c52a4953db2', '3', '', '', '', '', '', '2', '2018-07-10 21:53:58', '0000-00-00 00:00:00', '', '', '', ''),
(41, 'Aayush', 'Katiyar', 'aayush.katiyar@niletechnologies', '62b27d0ca91453b2c57413b07b1866d6', '6a36abb05a0104668559bdc0bb8ecf0f', '2', '', '', '', '', '', '3', '2018-07-12 02:28:07', '0000-00-00 00:00:00', '', '', '', ''),
(42, 'Nile', 'Technologies', 'technologiesnile@gmail.com', '207aabaeca6ee2dc6fed826459aa89d9', '3937d532995cf61a2fce37103215e05c', '3', '', '', '', '', '', '2', '2018-07-12 05:55:37', '0000-00-00 00:00:00', '', '', '', ''),
(43, 'Tushar', 'Gupta', 'tushar.gupta@niletechnologies.com', '54ff9bf8d834c52fdb0f18086e526604', '0a1d93fc7c2c8fb5fe5c8b3de1d7d133', '2', '', '', '', '', '', '3', '2018-07-13 03:45:40', '0000-00-00 00:00:00', '', '', '', ''),
(44, 'Jackson ', 'Nash', 'alainanash76@gmail.com', '6207eae5a93f3f4667a334a4765d6986', '6642a8457353b80900ad3561a5a96c19', '2', '', '', '', '', '', '3', '2018-07-17 12:20:44', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/alaina_pic1.jpg', ''),
(45, 'Arun', 'Kumar', 'arunverma910@yahoo.com', '02665e540894ace9ba6e9ae54ad78613', '607b9a0694538911e129b0c8dd222d59', '3', '', '', '', '', '', '2', '2018-07-17 23:44:02', '0000-00-00 00:00:00', '', '', '', ''),
(46, 'Arun', 'Kumar', 'arunverma910@yahoo.com', '02665e540894ace9ba6e9ae54ad78613', '3f9c5afc65c3e47f379d94dc4e8c2f94', '3', '', '', '', '', '', '2', '2018-07-18 05:17:31', '0000-00-00 00:00:00', '', '', '', ''),
(47, 'Tushar', 'Gupta', 'tushar1.gupta@niletechnologies.com', '120149c0cfd0cf95f7c6c3f22e3fb70d', '785efe91ade0f3fd77855b6a9259d811', '2', '', '', '', '', '', '3', '2018-07-18 23:05:40', '0000-00-00 00:00:00', '', '', '', ''),
(48, 'tushar', 'gupta', 'tushar@gmail.com', '7e2a3837cce86cdc7d198ab20275984f', 'fc49ea20f5e8482d287af8b0f4ee3d2e', '2', '', '', '', '', '', '3', '2018-07-19 03:51:19', '0000-00-00 00:00:00', '', '', '', ''),
(49, 'Kamlesh', 'Kumar', 'kamleshwar.yadav@niletechnologies.com', '1eb1edd9cdb4763d78b2abbe93445a0c', 'd6e7b512e63fd4b033340d4c7c676a3b', '2', '9873521653', '', '', 'A-51, Sectory-57, Noida', 'descr', '3', '2018-07-20 00:45:36', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/kamlesh.jpg', ''),
(50, 'Rahul', 'Singh', 'rahul.singh@niletechnologies.com', '528d8456c73ca93902758a3cb01298d2', 'ebaaba27b32928a25f2ad6185fc0cc74', '2', '', '', '', '', '', '3', '2018-07-23 02:46:01', '0000-00-00 00:00:00', '', '', '', ''),
(51, 'Arun', 'Kumar', 'arunverma910@yahoo.com', '02665e540894ace9ba6e9ae54ad78613', '59cdcc589ae6d94e69a65e8293f686a3', '3', '', '', '', '', '', '2', '2018-07-23 03:00:07', '0000-00-00 00:00:00', '', '', '', ''),
(52, 'Alaina', 'Nash', 'anash@airvenues.com', '22a936a4a98b7cc671b99ddeb0c31c81', '261d3ee7ec10759ae7cef7285a932209', '2', '', '', '', '', '', '3', '2018-07-23 07:06:28', '0000-00-00 00:00:00', '', '', '', ''),
(53, 'Nile', 'Technologies', 'technologiesnile@gmail.com', '207aabaeca6ee2dc6fed826459aa89d9', '6c4a5a04d9c33a35bac1c935ccc18f02', '3', '', '', '', '', '', '2', '2018-07-23 10:12:07', '0000-00-00 00:00:00', '', '', '', ''),
(54, 'Anu', 'Mishra', 'anu.mishra@niletechnologies.com', 'cdbbd2abd4e4ed72035d5b6471b83d97', '4f85d22b2e3963abbaa815a32e0fdb8f', '2', '', '', '', '', '', '3', '2018-07-26 03:41:04', '0000-00-00 00:00:00', '', '', '', ''),
(55, 'leigh lain', 'walker', 'write2lolly@gmail.com', '8c71a4d81c1d147734f1c6161fcea4df', 'cf3167938043c5115b1ef95d5d9cfd15', '2', '805-654-3887', '', '', '', '', '3', '2018-07-26 10:35:59', '0000-00-00 00:00:00', '', '', 'https://www.niletechinnovations.com/projects/airvenues/uploads/profile_image/Globe_Icon_HiRes.jpg', ''),
(56, 'Patrick', 'Horn', 'fsupatrick@gmail.com', 'e5aaa5ed81c5a7d85bade81a5b94c93e', 'd618e55f65485caa9ed792063bc285cc', '3', '', '', '', '', '', '2', '2018-07-26 23:03:59', '0000-00-00 00:00:00', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `venues_space_id` int(11) NOT NULL,
  `space_title` varchar(255) NOT NULL,
  `space_description` text NOT NULL,
  `space_area` int(11) NOT NULL,
  `total_rooms` int(11) NOT NULL,
  `person_allowed` int(11) NOT NULL,
  `wifi_name/psw` varchar(255) NOT NULL,
  `access_space` varchar(30) NOT NULL,
  `space_image` int(11) NOT NULL,
  `available_days` varchar(255) NOT NULL,
  `available_from` varchar(40) NOT NULL,
  `available_to` varchar(40) NOT NULL,
  `hourly_rate` int(11) NOT NULL,
  `hours` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `cleaning` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `amenities` varchar(255) NOT NULL,
  `space_type` varchar(100) NOT NULL,
  `ownership_type` varchar(100) NOT NULL,
  `lattitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `user_id`, `venues_space_id`, `space_title`, `space_description`, `space_area`, `total_rooms`, `person_allowed`, `wifi_name/psw`, `access_space`, `space_image`, `available_days`, `available_from`, `available_to`, `hourly_rate`, `hours`, `discount`, `cleaning`, `capacity`, `amenities`, `space_type`, `ownership_type`, `lattitude`, `longitude`, `location`, `created_at`, `updated_at`) VALUES
(19, 5, 0, 'Nile Building', 'Include any rules about what your guests can and cannot do in the space. For example, if smoking is not allowed, let them know that here. Some common topics for this section include outside food, alcohol, and pets. Cleaning will be covered later, so skip those rules for now.', 10, 0, 1, '', '', 0, 'Mon/Tue/Fri', '2018-06-20', '2018-06-22', 10, 10, 10, 0, 20, 'Chairs/Flip Charts/Rooftop', '', '', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(20, 6, 0, 'Historic Urban Bar', 'such as your venue name or phone number.', 23, 0, 1, 'Make it easier for your guests to get online by', 'Arrival', 0, 'Tue/Wed/Thu', '2018-06-21', '2018-06-22', 300, 3, 45, 1, 3, 'WiFi/Chalkboard/Projector/Screen/Flip Charts/Monitor/Kitchen', '', '', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(21, 5, 0, 'MINIONS - Respect the Banana', 'Include any rules about what your guests can and cannot do in the space. For example, if smoking is not allowed, let them know that here. ', 120, 0, 1, 'Test/Test', 'Arrival', 0, 'Sun/Mon/Tue/Thu', '2018-06-22', '2018-06-30', 5, 10, 5, 1, 10, 'Tables/Screen/Outdoor Area', '', '', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(22, 5, 0, 'Downtown Loft with View', 'Easy to use', -5000, 0, 0, '', '', 0, 'Sun/Mon', '2018-06-05', '2018-06-22', 40, 6, 20, 0, 20, 'Screen', '', '', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(23, 0, 0, 'industrial', 'industrial', 250, 3, 0, '', '', 0, 'Mon/Thu', '01:45:00', '02:00:00', 85, 10, 10, 0, 25, 'Tables/Flip Charts', 'House', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(24, 0, 0, 'Test Space', 'Venue details', 100, 50, 0, '', '', 0, 'Mon/Thu', '01:45:00', '04:30:00', 10, 10, 5, 0, 20, 'WiFi/Tables/Screen/Flip Charts/Breakout Space/Kitchen/Rooftop/Parking Space(s)', 'Commercial Loft', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(25, 0, 0, 'NIle Space', 'Nile space details', 200, 10, 0, '', '', 0, 'Mon/Thu/Fri/Sat', '04:45:00', '23:00:00', 10, 10, 5, 0, 20, 'WiFi/Chalkboard/Flip Charts/Conference Phone', 'Commercial Loft', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(26, 0, 0, 'Beautiful Boat', 'luxurious abdceuhe[fgbosi[;jngls;ijb', 300, 4, 0, '', '', 0, 'Sun/Mon/Tue/Wed/Thu/Fri/Sat', '07:00:00', '21:30:00', 50, 6, 10, 0, 200, 'WiFi/Public Transportation', 'Boat', 'Private', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(27, 0, 0, 'test', 'test', 300, 2, 0, '', '', 0, 'Mon/Thu/Sat', '01:00:00', '21:45:00', 50, 4, 10, 0, 40, 'Kitchen', 'Brewery/Vineyard', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(28, 0, 0, 'Words that Work', 'Yes ', 45, 9, 0, '', '', 0, 'Wed/Fri', '02:00:00', '03:30:00', 50, 4, 9, 0, 454, 'Flip Charts/Swimming pool ', 'Production/Photo Studio', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(29, 0, 0, 'Elegant Backyard Retreat', 'Beautifully landscaped backyard space with swimming pool, yard and pergola for saying your vows!  ', 12000, 0, 0, '', '', 0, 'Sun/Thu/Fri/Sat', '07:00:00', '01:00:00', 600, 3, 10, 0, 50, 'WiFi/Screen', 'Garden/Terrace', 'Private', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(30, 0, 0, 'Brewery for Receptions', 'We invite you to.....', 5000, 1, 0, '', '', 0, 'Thu/Fri/Sat', '01:15:00', '05:00:00', 350, 4, 10, 0, 150, 'WiFi/Tables/Chairs/Public Transportation/Restrooms', 'Bar/Restaurant', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(31, 0, 0, 'Brown Family Backyard Oasis', 'Serene pond setting for your ideal event.', 10000, 1, 0, '', '', 0, 'Sun/Fri/Sat', '08:00:00', '21:00:00', 100, 3, 10, 0, 100, 'WiFi/Restrooms/Outdoor Area/Dressing Room/Parking Space(s)/waterfall', 'Garden/Terrace', 'Private', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(32, 0, 0, 'Pic for sale', 'Hot studio space on sunday', 800, 1, 0, '', '', 0, 'Sun', '02:00:00', '06:00:00', 200, 2, 10, 0, 20, 'WiFi/Restrooms/Kitchen', 'Production/Photo Studio', 'Commercial', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(33, 0, 0, 'Jody\'s Loft', 'Spectacular views of the Tokyo tower make this the most ideal loft to host a party, wedding, business luncheon, presentation, or intimate private dining experience. ', 2300, 3, 0, '', '', 0, 'Mon/Tue/Wed/Thu/Fri', '13:00:00', '02:00:00', 200, 4, 0, 0, 150, 'WiFi/Projector/Screen/Monitor/Public Transportation/Restrooms/Breakout Space/Kitchen/Dressing Room', 'Loft', 'Live / Work', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(36, 44, 0, 'Backyard Oasis', 'Welcome to our backyard -- perfect for parties, reunions,  small weddings, rehearsal dinners, church gatherings, you name it!  Offering space for up to 40 people with street  parking - you are sure  to enjoy our Oasis for  your next retreat. ', 1000, 0, 0, '', '', 0, 'Sun/Fri/Sat', '12:30:00', '22:45:00', 150, 3, 10, 0, 40, 'WiFi/Restrooms', 'Garden/Terrace', 'Private', '', '', '', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(37, 49, 2, 'Fusion Factory Event Space', 'Fusion Factory in total is a 450m2 subterranean location situated beneath Rosa-Luxemburg-Strasse 17 in the heart of Berlin, Mitte.  The neon stairwell descends into the 200m2 Factory hall and adjacent 50m2 Control room, featuring exposed brick walls and expansive parquet floors and staging around a granite tiled “dance floor.”', 500, 100, 0, '', '', 0, 'Mon/Tue/Wed/Thu/Fri', '08:30:00', '18:30:00', 100, 2, 10, 0, 1000, 'WiFi/Tables/Chairs/Whiteboard/Chalkboard/Projector/Screen/Monitor', '', 'Commercial', '39.7179089', '-105.05214790000002', 'Greater Denver USBC, Zenobia Street, Denver, CO, USA', '2018-07-24 23:07:28', '2018-07-26 14:10:06'),
(38, 5, 5, 'A unique place', 'xxxxxx bbbbbb yyyy', 1000, 2, 0, '', '', 0, 'Mon/Tue/Wed/Thu', '01:15:00', '16:45:00', 20, 8, 10, 0, 200, 'Whiteboard', '', 'Commercial', '40.4172871', '-82.90712300000001', 'Ohio, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(39, 5, 3, 'Downtown Creative Space', 'Office space in DTLA, great for all types of events!\r\n\r\n250 guests max  |  Office/Conference/Co-Working  |  10 Rooms  |  2 Bathrooms', 100, 10, 0, '', '', 0, 'Sun/Mon/Tue/Wed/Thu/Fri', '06:00:00', '23:30:00', 100, 8, 5, 0, 200, 'WiFi/Whiteboard/Screen/Flip Charts/Public Transportation/Wheelchair Accessible/Breakout Space/Rooftop/Dressing Room', '', 'Commercial', '34.040713', '-118.24676929999998', 'Downtown, Los Angeles, CA, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(40, 49, 1, 'Modern Space in the Heart of Manhattan', 'We offer our Chelsea store for art exhibitions, private parties, fund-raisers and etc. It is also available to rent for your own weekly classes and/or workshops.', 1000, 100, 0, '', '', 0, 'Mon/Tue/Wed/Thu/Fri', '10:00:00', '19:00:00', 100, 2, 10, 0, 100, 'Tables/Chalkboard/Flip Charts/Monitor', '', 'Commercial', '40.7127753', '-74.0059728', 'New York, NY, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(41, 49, 12, 'Brooklyn Gallery', 'This gallery offers emerging artists, musicians and teachers a platform for dreams to manifest. We host events ranging from open mics and film screenings, to yoga workshops and exhibitions.', 1000, 100, 0, '', '', 0, 'Mon/Tue/Wed/Thu/Fri', '09:45:00', '06:45:00', 100, 2, 10, 0, 100, 'Projector/Screen/Restrooms/Rooftop/Outdoor Area', '', 'Private', '40.789142', '-73.13496099999998', 'New York, NY, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(42, 49, 12, 'Cozy Photography Studio, Work Space', 'Perfect for new - mid level photographers who need that extra bit of space for their work!', 1000, 150, 0, '', '', 0, 'Mon/Tue/Wed/Thu', '09:30:00', '18:30:00', 50, 2, 10, 0, 100, 'WiFi/Tables/Chalkboard/Projector', '', 'Live/Work', '40.7158762', '-73.93304289999998', 'East Williamsburg, Brooklyn, NY, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(43, 27, 18, 'Wine cellar ', 'Wine cellar and adjoining private dinning room is the perfect spot for tastings, private dinners, engagement moments, special intimate occasions.', 900, 1, 0, '', '', 0, 'Sun/Wed/Thu/Sat', '15:30:00', '23:00:00', 100, 2, 0, 0, 25, 'WiFi/Tables/Chairs/Conference Phone/Restrooms', '', 'Private', '39.7392358', '-104.990251', 'Denver, CO, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(44, 5, 10, 'Testing Space', 'Testing Space', 21, 12, 0, '', '', 0, 'Sun/Sat', '01:15:00', '01:15:00', 100, 10, 1, 0, 20, 'WiFi/Tables', '', 'Commercial', '39.724049', '-105.001703', 'New York, NY, USA', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(45, 24, 23, 'Kensington Bookshop', 'A book shop in Kensington, London', 344, 3, 0, '', '', 0, 'Wed', '01:15:00', '02:00:00', 15, 4, 10, 0, 300, 'WiFi/Tables/Chalkboard/Projector/Breakout Space/Rooftop/Outdoor Area', '', 'Commercial', '51.5010095', '-0.1932793999999376', 'Kensington, London, UK', '2018-07-24 23:07:28', '0000-00-00 00:00:00'),
(46, 55, 19, 'Sunny Lawn Park', '175 acres of sunny grassland available for events. No electricity, generators required. ', 7666560, 1, 0, '', '', 0, 'Sun/Mon/Thu/Fri/Sat', '06:30:00', '02:00:00', 350, 10, 10, 0, 2000, 'Public Transportation/Outdoor Area/Parking Space(s)', '', 'Public', '34.2804923', '-119.29451990000001', 'Ventura, CA, USA', '2018-07-26 10:51:04', '0000-00-00 00:00:00'),
(47, 55, 2, 'The Music Factory', 'An office suite comprising approximately 1,100 square feet. Reception area, two private offices. Located on Market Street corridor, with easy access to both Ventura Freeway (101) and the Santa Paula Freeway (126).', 1100, 3, 0, '', '', 0, 'Sun/Mon/Tue/Wed/Thu/Fri/Sat', '11:00:00', '02:00:00', 500, 2, 2, 0, 75, 'WiFi/Tables/Chairs/Screen/Monitor/Conference Phone/Public Transportation/Restrooms/Wheelchair Accessible/Breakout Space/Parking Space(s)', '', 'Commercial', '34.2577534', '-119.23205669999999', '4531 Market Street, Ventura, CA 93003, USA', '2018-07-26 10:59:22', '0000-00-00 00:00:00'),
(48, 55, 12, 'Bell View Arts Factory', 'Bell Arts Factory works closely with several mission-aligned organizations including the City of Ventura, the Museum of Ventura, the VC Arts Council. ', 8753, 3, 0, '', '', 0, 'Sun/Mon/Tue/Thu/Fri/Sat', '09:00:00', '22:00:00', 200, 4, 0, 0, 250, 'WiFi/Chairs/Conference Phone/Restrooms/Breakout Space/Kitchen/Parking Space(s)', '', 'Commercial', '34.2804923', '-119.29451990000001', 'Ventura, CA, USA', '2018-07-26 11:10:19', '0000-00-00 00:00:00'),
(49, 28, 15, 'City View Loft', 'This beautiful loft can be used for retreats, collaboration for  business or study, weddings and receptions.  Fully furnished and ready for your best event!', 5000, 1, 0, '', '', 0, 'Sun/Fri/Sat', '12:00:00', '20:00:00', 300, 12, 10, 0, 40, 'WiFi/Tables/Chairs/Whiteboard/Chalkboard/Projector/Screen/Flip Charts/Monitor/Conference Phone/Public Transportation/Restrooms/Wheelchair Accessible/Breakout Space/Kitchen/Outdoor Area/Dressing Room/Parking Space(s)', '', 'Private residence', '39.854992', '-105.1317431', 'Arvada, CO 80005, USA', '2018-07-26 14:47:53', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `venues_spaces`
--

CREATE TABLE `venues_spaces` (
  `id` int(11) NOT NULL,
  `space_title` varchar(255) NOT NULL,
  `space_description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venues_spaces`
--

INSERT INTO `venues_spaces` (`id`, `space_title`, `space_description`) VALUES
(1, 'Production/Photo Studio', ''),
(2, 'Commercial Loft', ''),
(3, 'Apartment/Penthouse', ''),
(4, 'Auditorium/Theatre', ''),
(5, 'Banquet Hall/Ballroom', ''),
(6, 'Bar/Restaurant', ''),
(7, 'Boat', ''),
(8, 'Brewery/Vineyard', ''),
(9, 'Camper', ''),
(10, 'Commercial Kitchen', ''),
(11, 'Event Space', ''),
(12, 'Gallery', ''),
(13, 'Garden/Terrace', ''),
(14, 'House', ''),
(15, 'Loft', ''),
(16, 'Mansion/Estate', ''),
(17, 'Office/Conference/Co-Working', ''),
(18, 'Others', ''),
(19, 'Outdoor Space', ''),
(20, 'Recreational Facility', ''),
(21, 'Religious Building', ''),
(22, 'Rooftop', ''),
(23, 'Storefront', ''),
(24, 'Studio (dance/art/salon/etc)', ''),
(25, 'Townhouse', ''),
(26, 'Warehouse', ''),
(27, 'Not in listing', '');

-- --------------------------------------------------------

--
-- Table structure for table `venue_gallery`
--

CREATE TABLE `venue_gallery` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `thumb_url` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0:Pending; 1:Published',
  `is_main` enum('0','1') NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venue_gallery`
--

INSERT INTO `venue_gallery` (`id`, `user_id`, `venue_id`, `file_name`, `thumb_url`, `status`, `is_main`, `created_date`) VALUES
(10, 23, 25, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/bulldog-404.jpg', '', '1', '0', '2018-06-27 12:30:41'),
(11, 23, 25, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/bahrain-flag.png', '', '1', '0', '2018-06-27 12:30:41'),
(12, 23, 25, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/canada-flag.png', '', '1', '0', '2018-06-27 12:30:41'),
(13, 5, 26, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/eb6Sn51cRemMQf31d2yU7w.jpg', '', '1', '0', '2018-06-27 14:12:07'),
(14, 24, 27, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_4312.JPG', '', '1', '0', '2018-06-28 12:47:08'),
(15, 24, 27, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_4334.JPG', '', '1', '0', '2018-06-28 12:47:35'),
(16, 24, 27, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_4315.JPG', '', '1', '0', '2018-06-28 12:47:56'),
(17, 24, 27, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_4317.JPG', '', '1', '0', '2018-06-28 12:47:56'),
(18, 24, 28, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/1AFAE7F7-69A2-4500-8032-7F9787106C38.jpeg', '', '1', '0', '2018-06-28 20:51:09'),
(19, 24, 28, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/B920C884-BBD8-4A3D-877D-5D9ECD3738A1.jpeg', '', '1', '0', '2018-06-28 20:51:16'),
(20, 24, 28, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/4E83BA5D-DF47-4586-A662-E8BBE0D9FCF0.jpeg', '', '1', '0', '2018-06-28 20:51:19'),
(21, 24, 28, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/680D3D91-9C86-42B6-9C8B-EDDA7ED63CBC.jpeg', '', '1', '0', '2018-06-28 20:51:23'),
(22, 24, 28, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/D69263AD-82B2-431B-B9C9-3E195252DD52.jpeg', '', '1', '0', '2018-06-28 20:51:24'),
(23, 34, 33, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/interior-design-decor-reading-nook-10.jpg', '', '1', '0', '2018-07-04 18:37:05'),
(24, 34, 33, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/Profile_for_Air_BnB.jpg', '', '1', '0', '2018-07-04 18:37:06'),
(25, 44, 36, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_0094.jpg', '', '1', '0', '2018-07-17 19:45:09'),
(26, 44, 36, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/2017-09-04_06.30.37.jpg', '', '1', '0', '2018-07-17 19:46:00'),
(27, 44, 36, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/pool.jpg', '', '1', '0', '2018-07-17 19:46:19'),
(29, 5, 38, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/Photos_Library.png', '', '1', '0', '2018-07-20 09:47:00'),
(30, 5, 39, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/downtown-la.jpg', '', '1', '0', '2018-07-20 10:16:53'),
(31, 49, 40, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/dgqt0fmifweawrmqmnda1.jpg', '', '1', '0', '2018-07-20 12:09:25'),
(33, 49, 41, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/zkndtjshqvibswhb6krt.jpg', '', '1', '0', '2018-07-20 12:15:57'),
(34, 49, 42, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/fdsf.jpg', '', '1', '0', '2018-07-20 12:30:36'),
(35, 27, 43, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_5749.jpeg', '', '1', '0', '2018-07-22 13:28:27'),
(38, 5, 44, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/Admin-settings.png', '', '1', '0', '2018-07-24 06:41:16'),
(39, 5, 44, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/avatar3-min.jpg', '', '1', '0', '2018-07-24 06:41:52'),
(40, 5, 44, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/avatar2-min.jpg', '', '1', '0', '2018-07-24 06:42:37'),
(41, 24, 45, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_2063.JPG', '', '1', '0', '2018-07-24 12:07:48'),
(42, 24, 45, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_2062.JPG', '', '1', '0', '2018-07-24 12:07:50'),
(43, 24, 45, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_2061.JPG', '', '1', '0', '2018-07-24 12:07:59'),
(44, 24, 45, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_20621.JPG', '', '1', '0', '2018-07-24 12:10:15'),
(45, 24, 45, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_20631.JPG', '', '1', '0', '2018-07-24 12:10:20'),
(46, 24, 45, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/IMG_20611.JPG', '', '1', '0', '2018-07-24 12:10:22'),
(49, 55, 46, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/path.png', 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery_thumbnail/path.png', '1', '0', '2018-07-26 17:49:13'),
(50, 55, 47, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/mock_venue.jpg', 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery_thumbnail/mock_venue.jpg', '1', '0', '2018-07-26 17:59:19'),
(51, 55, 48, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/mock_venue_2.jfif', 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery_thumbnail/mock_venue_2.jfif', '1', '0', '2018-07-26 18:07:22'),
(52, 55, 48, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/mock_venue_21.jfif', 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery_thumbnail/mock_venue_21.jfif', '1', '0', '2018-07-26 18:08:48'),
(53, 55, 48, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/Green_Wall_pexels-photo-131683.jpeg', 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery_thumbnail/Green_Wall_pexels-photo-131683.jpeg', '1', '0', '2018-07-26 18:10:15'),
(54, 28, 49, 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery/Pool_and_mountains_image.jpg', 'https://www.niletechinnovations.com/projects/airvenues/uploads/venue_gallery_thumbnail/Pool_and_mountains_image.jpg', '1', '0', '2018-07-26 21:46:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venues_spaces`
--
ALTER TABLE `venues_spaces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_gallery`
--
ALTER TABLE `venue_gallery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `venues_spaces`
--
ALTER TABLE `venues_spaces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `venue_gallery`
--
ALTER TABLE `venue_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
