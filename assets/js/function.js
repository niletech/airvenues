$(document).ready(function(){ 
  $('#sponsors-owl-carousel').owlCarousel({
    loop:true,
    autoplay:true,
    margin:10,
    nav:true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    dots: true,
    paginationSpeed: 300,
    rewindSpeed: 400,
    items:4,
  })
  $('#testimonial-carousel').owlCarousel({
    loop:true,
    autoplay:true,
    margin:10,
    nav:true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    dots: true,
    paginationSpeed: 300,
    rewindSpeed: 400,
    items:3,
  })
})
 
$(document).ready(function(){ 
  $('#owl-event-media').owlCarousel({
    loop:true,
    autoplay:true,
    margin:10,
    nav:true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
    dots: true,
    paginationSpeed: 300,
    rewindSpeed: 400,
    items:1
})
});

$(document).ready(function(){ 
 $('.datepicker').datepicker()
});



$(document).ready(function() {
 var s3 = $("#ranged-value").freshslider({
        range: true,
        step:0.2,
        value:[10, 80],
        unit:'$',
        onchange:function(low, high){
            $("#low-price").val(low);
            $("#high-price").val(high);
            //console.log(low, high);
        }
    });
});

$(document).ready(function() {
  $(".scroll").click(function(event) {
  $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 500);
  });
});

jQuery(document).ready(function(){
  // This button will increment the value
  $('[data-quantity="plus"]').click(function(e){
      // Stop acting like a button
      e.preventDefault();
      // Get the field name
      fieldName = $(this).attr('data-field');
      // Get its current value
      var currentVal = parseInt($('input[name='+fieldName+']').val());
      // If is not undefined
      if (!isNaN(currentVal)) {
          // Increment
          $('input[name='+fieldName+']').val(currentVal + 1);
      } else {
          // Otherwise put a 0 there
          $('input[name='+fieldName+']').val(0);
      }
  });
  // This button will decrement the value till 0
  $('[data-quantity="minus"]').click(function(e) {
      // Stop acting like a button
      e.preventDefault();
      // Get the field name
      fieldName = $(this).attr('data-field');
      // Get its current value
      var currentVal = parseInt($('input[name='+fieldName+']').val());
      // If it isn't undefined or its greater than 0
      if (!isNaN(currentVal) && currentVal > 0) {
          // Decrement one
          $('input[name='+fieldName+']').val(currentVal - 1);
      } else {
          // Otherwise put a 0 there
          $('input[name='+fieldName+']').val(0);
      }
  });
});



$(document).ready(function(){
hide = false;
$('body').on("click", function () {
    if (hide) $('.filter-tab').removeClass('filteractive');
    hide = true;
});

// add and remove .active
$('body').on('click', '.filter-tab', function () {

    var self = $(this);

    /*if (self.hasClass('filteractive')) {
        //$('.filter-tab').removeClass('filteractive');
        return false;
    }*/

    $('.filter-tab').removeClass('filteractive');

    self.toggleClass('filteractive');
    hide = false;
});
});





