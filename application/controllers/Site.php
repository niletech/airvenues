<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','cookie','form','authme','common'));
		$this->load->library(array('session','form_validation','email','parser','authme'));
		$this->load->model(array('user_model'));
	}

	public function index(){
		$data['title'] = DEFAULT_SITE_TITLE;
		$data['description'] = DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/index';
		$this->load->view('includes/template',$data);
	}
	public function about(){
		$data['title'] = 'About Us - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'About Us - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/about';
		$this->load->view('includes/template',$data);
	}
	public function login(){
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if(!$this->form_validation->run()==FALSE)
		{
			 $email =$this->input->post('email');
			 $pass =$this->input->post('password');
			 $user_details=$this->user_model->check_user_login($email,$pass);
			 
			if($user_details->num_rows()>0){
				$row= $user_details->row();				
				$this->session->set_userdata(array(
					 'logged_in' => true,
					 'user_id'=>$row->user_id,
					 'user_group'=>$row->user_group,
					 'first_name'=>$row->first_name,
					 'last_name'=>$row->last_name,
					 'user_name'=>$row->first_name." ".$row->last_name,
					 'user_email'=>$row->email,
					 'address'=>$row->address,
					 'phone'=>$row->phone
					 ) 
					);
					
				 redirect('step1');
			 }
			 else{
				 $this->session->set_flashdata('erreor_login','Email and Password are incorrect!');
				 redirect('site/login');
				 
			 }
		}else{
			$data['title'] = 'Login - '. DEFAULT_SITE_TITLE;
			$data['description'] = 'Login - '.DEFAULT_SITE_TITLE;
			$data['main_content']	=	'site/login';
			$this->load->view('includes/template',$data);
		}
	}
	
	public function register(){ 
		$this->load->library('recaptcha');
		$recaptcha = $this->recaptcha->create_box();
		
		$is_valid = $this->recaptcha->is_valid();
		$data['recaptcha']		=	$recaptcha;
		$this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]');
		$this->form_validation->set_rules('first_name', 'First name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_message('is_unique', 'This %s is already registered');
			
		if($this->form_validation->run() == FALSE && (!$is_valid['success']) ){
			if($is_valid['success']!=''){
				$this->form_validation->set_message('recaptcha', 'Please enter valid captcha?');
			}
			$data['title'] = 'Register - '. DEFAULT_SITE_TITLE;
			$data['description'] = 'Register - '.DEFAULT_SITE_TITLE;
			$data['main_content']	=	'site/register';
			$this->load->view('includes/template',$data);
		}else{
			//print_r($is_valid);
			
			
			$user_data = array(
				'first_name'	=>	$this->input->post('first_name'),
				'last_name'		=>	$this->input->post('last_name'),
				'email'			=>	$this->input->post('email'),
				'hash'			=>	md5($this->input->post('email')),
				'password'		=>	md5($this->input->post('password')),
				'user_group'	=>	'2',
				'account_status'=>'3'
			);
			$user_id = $this->user_model->save('users',$user_data);
			$full_name = $this->input->post('first_name').' '.$this->input->post('last_name');
			//$this->session->set_userdata(array('logged_in' => true,'user_id'=>$user_id,'user_group'=>'3','first_name'=>$this->input->post('first_name'),'last_name'=>$this->input->post('last_name'),'user_name'=>$full_name,'user_email'=>$this->input->post('email') ) );
			$this->sendVerificatinEmail( $user_id,$this->input->post('email'),$full_name );
			redirect('site/register_success/'.$user_id);
			
		}
	}
	function register_success($user_id){
		$data['user_data'] 		= $this->user_model->getSingleRecord( 'users', array('user_id'=>$user_id) );
		$data['title']		 	= 'Accout created successfully';
		$data['description'] 	= 'Accout created successfully';
		$data['main_content']	=	'site/register_success';
		$this->load->view('includes/template',$data);
	}
	
	function forgot_password(){
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
		if($this->form_validation->run() == FALSE){
			$data['success'] = false;
		}else{
			$email = $this->input->post('email');
			$user_query = $this->user_model->getSingleRecord('users',array('email' => $email) );
			if($user_query->num_rows()){
				$user_data = $user_query->row();
				
				$this->sendResetPasswordEmail($user_data);
				$this->session->set_flashdata('success_msg', 'Reset password link has been sent to '.$email);
				redirect('login');
			}
		}
		$data['title'] = 'Forgot Password - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Forgot Password - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/forgot_password';
		$this->load->view('includes/template',$data);
	}
	
	public function contact(){
		$data['title'] = 'Contact Us - '. DEFAULT_SITE_TITLE;
		$data['description'] = DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/contact';
		$this->load->view('includes/template',$data);
	}
	
	function terms_and_conditions(){
		$data['title'] = 'Terms and Conditions - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Terms and Conditions '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/terms_and_conditions';
		$this->load->view('includes/template',$data);
	}
	
	function privacy_policy(){
		$data['title'] = 'Privacy Policy - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Privacy Policy '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/privacy_policy';
		$this->load->view('includes/template',$data);
	}

	function venues(){
		//print_r(json_decode(TYPE_OF_SPACE)) ;exit;
			$result = $this->user_model->filter_venues($this->input->get());
			$data['post_data'] = $this->input->get();
			$data['data'] = $result->result();
			$data['title'] = 'Venues - '.DEFAULT_SITE_TITLE;
			$data['description'] = 'Venues '.DEFAULT_SITE_TITLE;
			$data['space_lists'] = $this->user_model->get_venues_spaces_list();
			$data['main_content']	=	'site/venues';
			$this->load->view('includes/template',$data);
		
	}

	function venues_details(){
		$data['title'] = 'Venues Details - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Venues Details'.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/venues_details';
		$this->load->view('includes/template',$data);
	}
	
	
	function legal(){
		$data['title'] = 'Legal - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Legal - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/legal';
		$this->load->view('includes/template',$data);
	}
	
	function step1(){ 
		if(!logged_in()) redirect('site/login');
		if($this->session->userdata('user_id'))
		{
			$this->form_validation->set_rules('title','Title','required|is_unique[venues.space_title]');
			if($this->form_validation->run()== FALSE){		
				$data['venue_spaces'] = $this->user_model->get_venues_spaces_list();
				$data['title'] = 'Step1 - '.DEFAULT_SITE_TITLE;
				$data['description'] = 'Step1 - '.DEFAULT_SITE_TITLE;
				$data['main_content']	=	'site/step1';
				$this->load->view('includes/template',$data);
			}
			else
			{
				
				$step1_data = array(
					'user_id'=>$this->session->userdata('user_id'),
					'space_id'=>$this->input->post('space_type'),
					'ownership_type'=>$this->input->post('ownership_type'),
					'space_title'=>$this->input->post('title'),
					'space_description'=>$this->input->post('description'),
					'lattitude'=>$this->input->post('lattitude'),
					'longitude'=>$this->input->post('longitude'),
					'location'=>$this->input->post('location'),
					//'access_space'=>$this->input->post('get_access')
				);
				$this->session->set_userdata('step1', $step1_data);
				redirect('step2');
			}
		}
		
	}
	
	function step2(){

		if(!logged_in()) redirect('site/login');
		if($this->session->userdata('step1')){
			
			if($this->input->post('start_time')!='' && $this->input->post('end_time')!='' ){
				
					$days= implode('/',$this->input->post('day'));
					
				$step2_data = array(
					'available_days'=>$days,
					'available_from'=>$this->input->post('start_time'),
					'available_to'=>$this->input->post('end_time')
				);
				$this->session->set_userdata('step2', $step2_data);
				redirect('step3');
			}else{
				$this->session->set_flashdata('message','Please fill all required fields.');
				$data['title'] = 'Step2 - '.DEFAULT_SITE_TITLE;
				$data['description'] = 'Step2 - '.DEFAULT_SITE_TITLE;
				$data['main_content']	=	'site/step2';
				
				$this->load->view('includes/template',$data);
			}
		}else{
			$this->session->set_flashdata('message','Please fill all required in step1.');
			redirect('step1');
		}	
	} 
	
	function step3(){
		if(!logged_in()) redirect('site/login');
		if($this->session->userdata('step2')){
			if($this->input->post('hourly_rate')!='' && $this->input->post('hours')!='' ){
				//print_r($this->input->post());
					$amenities= implode('/',$this->input->post('amenities'));
					
				$step3_data = array(
					'hourly_rate'=>$this->input->post('hourly_rate'),
					'discount'=>$this->input->post('discount'),
					'capacity'=>$this->input->post('guests'),
					'hours'=>$this->input->post('hours'),
					'space_area'=>$this->input->post('sqft'),
					'total_rooms'=>$this->input->post('total_rooms'),
					'amenities'=>$amenities
					
				);
				$this->session->set_userdata('step3', $step3_data);
				redirect('step4');
			}else{
				$this->session->set_flashdata('message','Please fill all required fields.');
				$data['title'] = 'Step 3 - '.DEFAULT_SITE_TITLE;
				$data['description'] = 'Step 3 - '.DEFAULT_SITE_TITLE;
				$data['main_content']	=	'site/step3';
				
				$this->load->view('includes/template',$data);
			}
		}else{
			$this->session->set_flashdata('message','Please fill all required in step2.');
			redirect('step2');
		}	
	}
	
	function step4(){
		if(!logged_in()) redirect('site/login');
		if($this->session->userdata('step3')){
			
			if($this->input->post('fname')!='' && $this->input->post('lname')!='' ){
				$step4_data = array(
					'first_name'=>$this->input->post('fname'),
					'last_name'=>$this->input->post('lname'),
					'phone'=>$this->input->post('phone'),
				);
				$this->user_model->update('users',array('user_id'=>$this->session->userdata('user_id')), $step4_data);
				$this->session->set_userdata('step4', $step4_data);
				redirect('step5');
				
				 
			}else{
				$this->session->set_flashdata('message','Please fill all required fields.');
				$data['title'] = 'step4 - '.DEFAULT_SITE_TITLE;
				$data['description'] = 'step4 - '.DEFAULT_SITE_TITLE;
				$data['main_content']	=	'site/step4';
				$data['data']	= $this->user_model->getSingleRecord('users',array('email'=>$this->session->userdata('user_email')))->row();
				$this->load->view('includes/template',$data);
			}
		}else{
			$this->session->set_flashdata('message','Please fill all required in step3.');
			redirect('step3');
		}	
	}	
	function step5(){
		
		if(!logged_in()) redirect('site/login');
		if($this->session->userdata('step4')){
			
			if($this->input->post('complete')!='' ){
				$session_data=array_merge($this->session->userdata('step1'),$this->session->userdata('step2'),$this->session->userdata('step3'),$this->session->userdata('step4') );
				$session_data['slug'] = create_slug($session_data['space_title'],'venues');
				 
				$venue_id = $this->user_model->insert_venues($session_data);
				if($venue_id){
					$this->user_model->update('venue_gallery',array('user_id'=>$this->session->userdata('user_id'),'venue_id'=>'0' ),array('venue_id'=>$venue_id)); 
					redirect('site/venue_complete');
				 }
				 
			}else{
				$this->session->set_flashdata('message','Please fill all required fields.');
				$data['title'] = 'Step 5 - '.DEFAULT_SITE_TITLE;
				$data['description'] = 'Step 5 - '.DEFAULT_SITE_TITLE;
				$data['main_content']	=	'site/step5';
				$data['gallary']	= $this->user_model->getResultsByCondtion('venue_gallery',array('user_id'=>$this->session->userdata('user_id'),'venue_id'=>'0'));
				$this->load->view('includes/template',$data);
			}
		}else{
			$this->session->set_flashdata('message','Please fill all required in step3.');
			redirect('step3');
		}	
	}
	
	
	function venue_complete(){
		if(!logged_in()) redirect('site/login');
		$this->session->unset_userdata(array('step1','step2','step3','step4'));
				$data['title'] 			= 'Success - '.DEFAULT_SITE_TITLE;
				$data['description']	= 'success - '.DEFAULT_SITE_TITLE;
				$data['main_content']	=	'site/success';
				
				$this->load->view('includes/template',$data);
	}
	 function reset_password(){
		$data['success'] = false;
		$user_id = $this->uri->segment(2);
		if(!$user_id) show_error('Invalid reset code.');
		$hash = $this->uri->segment(3);
		if(!$hash) show_error('Invalid reset code.');
		
		$user_query = $this->user_model->getSingleRecord('users',array('user_id'=>$user_id) );
		if($user_query->num_rows()==0) show_error('Invalid reset code.');
		$user_data = $user_query->row();
		
		$slug = md5($user_data->user_id . $user_data->email . date('Ymd'));
		if($hash != $slug) show_error('Invalid reset code.');
		
		$this->form_validation->set_rules('password', 'Password', 'required|min_length['. $this->config->item('authme_password_min_length') .']');
		$this->form_validation->set_rules('password_conf', 'Confirm Password', 'required|matches[password]');
		if($this->form_validation->run()){
			$data_array = array( 'password'=>md5($this->input->post('password')), 'updated_date'=>date('Y-m-d H:i:s') );
			$this->user_model->update('users', array('user_id'=>$user_id ), $data_array );
			$data['success'] = true;
		}
		$data['title'] = 'Reset Password - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Reset Password - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/reset_password';
		$this->load->view('includes/template',$data);		
	}
		
	function upload_profile_image(){
		
		if (!empty($_FILES)) {
			if($this->input->post('old_image')){
				$file_url = FCPATH.'uploads/profile_image/'.$this->input->post('old_image');
				@unlink($file_url); 
			}
			$folder_path = './uploads/profile_image/';
				
			$config['upload_path']   = $folder_path; 
			$config['allowed_types'] = '*';
			$config['max_size']      = 0;

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$files_data = $this->upload->data();
			$file_url = base_url('uploads/profile_image/'.$files_data['file_name']);
			$arra_data = array( 'profile_image'	=>	$file_url );
			$this->user_model->update('users',array('user_id'=>$this->session->userdata('user_id') ),$arra_data);
			
			echo json_encode(array('msg'=>'success','id'=>$this->session->userdata('user_id')));
		}else{ echo "Please select atleast one file!";}	
	}
	function upload_venue_image(){
		if (!empty($_FILES)) {
			$folder_path = './uploads/venue_gallery/';
				
			$config['upload_path']   = $folder_path; 
			$config['allowed_types'] = '*';
			$config['max_size']      = 0;

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$files_data = $this->upload->data();
			$file_url = base_url('uploads/venue_gallery/'.$files_data['file_name']);
			$thumb_url = base_url('uploads/venue_gallery_thumbnail/'.$files_data['file_name']);
			$images = array();
			if($this->session->userdata('venue_images')){
				$images = $this->session->userdata('venue_images');
			}
			
			//$this->session->set_userdata('venue_images', );
			$arra_data = array(
							'user_id'		=>	$this->session->userdata('user_id'),
							'file_name'		=>	$file_url,
							'thumb_url'		=>  $thumb_url,
							'venue_id'		=>	'0',
							'is_main'		=>	'0',
							'created_date'	=>	date('Y-m-d H:i:s'),
						);
			$last_id = $this->user_model->save('venue_gallery',$arra_data);
			
			echo json_encode(array('msg'=>'success','id'=>$last_id,'filename'=>$files_data['file_name']));
			$this->do_resize($files_data['file_name']);
		}else{ echo "Please select atleast one file!";}	
	}
	
	
	function check_user_email(){
		if($this->input->post('email')!=''){
			$query = $this->user_model->getResultsByCondtion('users', array('email'=>$this->input->post('email') ) );
			if($query->num_rows()==0)
				echo "true";
			else
				echo "false";
		}else{
			return false;
		}
	}
	function sendVerificatinEmail($user_id,$user_email,$user_name){
		$this->load->helper('phpmailer');
		$hash = md5($user_email);
		$email_data = array(
			'user_name'		=>	$user_name,
			'account_activation_url'	=>	base_url('site/activate/').$user_id.'/'.$hash
		);
		$subject = 'AirVenues Registration Confirmation';
		$email_msg = $this->parser->parse('email_templates/account_activation', $email_data, TRUE);
		$from =  array( DEFAULT_EMAIL_FROM,DEFAULT_SITE_TITLE );
		phpmail_send($from, $user_email, $subject, $email_msg);
	}
	function activate($user_id,$hash){
		$data['user_data'] 		= $this->user_model->getSingleRecord( 'users', array('user_id'=>$user_id,'hash'=>$hash,'account_status'=>'2' ) );
		$data['title']		 	= 'Accout Activated Successfully';
		$data['description'] 	= 'Accout Activated Successfully';
		$data['main_content']	=	'site/activate';
		$this->load->view('includes/template',$data);
	}
	public function email_check($str){
		$query = $this->db->get_where('users', array('email' => $str), 1);
		if ($query->num_rows()== 1){
			 return true;
		}else{    
			 $this->form_validation->set_message('email_check', 'We couldn\'t find this email address in our system.');
			return false;
		}
	}
	function sendWelcomeEmail($user_id,$user_email,$user_name){
		$this->load->helper('phpmailer');
		$email_data = array(
			'user_name'		=>	$user_name,
			'user_email'	=>	$user_email,
			'website_url'	=>	base_url('site/login'),
		);
		$subject = 'Welcome to AirVenues';
		$email_msg = $this->parser->parse('email_templates/welcome_email', $email_data, TRUE);
		$from =  array( DEFAULT_EMAIL_FROM,DEFAULT_SITE_TITLE );
		phpmail_send($from, $user_email, $subject, $email_msg);
	}
	function sendResetPasswordEmail($user_data){
		$this->load->helper('phpmailer');
		$slug = md5($user_data->user_id . $user_data->email . date('Ymd'));
		
		$email_data = array(
			'user_name'	=>	$user_data->first_name.' '.$user_data->last_name,
			'user_email'=>	$user_data->email,
			'reset_link'=>	site_url('reset_password/'. $user_data->user_id .'/'. $slug),
			'expire_msg'=>	'Note: This reset code will expire after '. date('j M Y') .'.'
		);
		$subject = 'Password reset for '.DEFAULT_SITE_TITLE;
		$email_msg = $this->parser->parse('email_templates/reset_password_email', $email_data, TRUE);
		$from =  array( DEFAULT_EMAIL_FROM,DEFAULT_SITE_TITLE );
		@phpmail_send($from, $user_data->email, $subject, $email_msg);
	}
	
	function sign_out(){
		if(!logged_in()) redirect('/site/login');
		$this->authme->logout('/');
	}

	public function remove_drop_box_file($image_id = NULL){ 
		if($_POST['folder_name']=='venue_gallery'){ 
			$this->user_model->delete_by_condition('venue_gallery',array('id'=>$image_id));
			$file_url = FCPATH.'uploads/venue_gallery_thumbnail/'.$_POST['name'];
			@unlink($file_url); 
		}else if($_POST['folder_name']=='profile_image'){ 
			$this->user_model->update('users',array('user_id'=>$image_id), array('profile_image'=>''));
		}
		$file_url = FCPATH.'uploads/'.$_POST['folder_name'].'/'.$_POST['name'];
		@unlink($file_url); 
 		return 0;
	}

	public function make_default_image($imageId){
		$user_id = $this->session->userdata('user_id');
		$num = $this->user_model->getSingleRecord('venue_gallery',array('user_id'=>$user_id,'venue_id'=>'0','is_main'=>'1'));
		if($num->num_rows()>0){
			$this->db->where(array('user_id'=>$user_id,'venue_id'=>'0'));
			$this->db->update('venue_gallery',array('is_main'=>'0'));
		}

		$this->db->where(array('id'=>$imageId));
		$this->db->update('venue_gallery',array('is_main'=>'1'));
		die;
	}

	public function do_resize($filename)
	{
	    $source_path = FCPATH . '/uploads/venue_gallery/' . $filename;
	    $target_path = FCPATH . '/uploads/venue_gallery_thumbnail/'.$filename;
	    $config_manip = array(
	        'image_library' => 'gd2',
	        'source_image' => $source_path,
	        'new_image' => $target_path,
	        'maintain_ratio' => FALSE,
	        'create_thumb' => TRUE,
	        'thumb_marker' => '',
	        'width' => 450,
	        'height' => 388
	    );
	    $this->load->library('image_lib', $config_manip);
	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors();
	    }
	    $this->image_lib->clear();
	}

	public function add_custom_event(){
		if($_SERVER['REQUEST_METHOD']!=='POST') redirect('/');
		$custom_event = $this->input->post('event');
		$record = $this->user_model->getSingleRecord('venues_spaces',array('space_title'=>$custom_event));
		if($record->num_rows()){
			echo json_encode(array('msg'=>'Record already exist','status'=>1));
		}
		else{
			$this->db->insert('venues_spaces',array('space_title'=>$custom_event));
			echo json_encode(array('msg'=>'Created Successfully, Please select','status'=>2,'lists'=>$this->user_model->get_venues_spaces_list()));
		}
	}

	public function add_custom_type_of_space(){
		if($_SERVER['REQUEST_METHOD']!=='POST') redirect('/');
		$custom_event = $this->input->post('event');
		$record = $this->user_model->getSingleRecord('venues_spaces',array('space_title'=>$custom_event));
		if($record->num_rows()){
			echo json_encode(array('msg'=>'Record already exist','status'=>1));
		}
		else{
			//$this->db->insert('venues_spaces',array('space_title'=>$custom_event));
			echo json_encode(array('msg'=>'Created Successfully, Please select','status'=>2));
		}
	}

	function test(){
		$data['title'] = 'step1 - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'step4 - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'site/venues_backup';
		
		$this->load->view('site/test',$data);
	}
}
