<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('url','cookie','form','authme','text'));
		$this->load->library(array('session','form_validation','email','parser','authme'));
		$this->load->model(array('user_model'));
	}

	public function dashboard(){
		$data['user_query'] = $this->user_model->getSingleRecord('users',array('user_id'=>$this->session->userdata('user_id') ) );
		$data['venue_lists'] = $this->user_model->get_venues_by_user_id($this->session->userdata('user_id'));
		$data['title'] = DEFAULT_SITE_TITLE;
		$data['description'] = DEFAULT_SITE_TITLE;
		$data['main_content']	=	'user/dashboard';
		$this->load->view('includes/user_template',$data);
	}
	public function my_venues(){
		$data['title'] = 'My Venues - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'My Venues - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'user/my_venues';
		$data['user_query'] = $this->user_model->getSingleRecord('users',array('user_id'=>$this->session->userdata('user_id') ) );
		$data['venue_lists'] = $this->user_model->get_venues_by_user_id($this->session->userdata('user_id'));
		$this->load->view('includes/user_template',$data);
	}

	public function my_account(){
		$data['title'] = 'My Account - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'My Account - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'user/my_account';
		$data['user_query'] = $this->user_model->getSingleRecord('users',array('user_id'=>$this->session->userdata('user_id') ) );
		$data['bank']	= $this->user_model->getSingleRecord('bank_account',array('user_id'=>$this->session->userdata('user_id') ) )->row();
		$this->load->view('includes/user_template',$data);
	}

	public function update_user(){
		$arr = array(
			'first_name'=>$this->input->post('first_name'),
			'last_name'=>$this->input->post('last_name'),
			'phone'=>$this->input->post('phone'),
			'address'=>$this->input->post('address'),
			'description'=>$this->input->post('description'),
		);
		$this->user_model->update('users',array('user_id'=>$this->session->userdata('user_id')), $arr);
	}

	public function edit_venue($venue_id=0){
		$user_id = $this->session->userdata('user_id');
		$data['title'] = 'Edit Venue - '.DEFAULT_SITE_TITLE;
		$data['description'] = 'Edit Venue - '.DEFAULT_SITE_TITLE;
		$data['main_content']	=	'user/edit_venue';
		$data['user_query'] = $this->user_model->getSingleRecord('users',array('user_id'=>$user_id) );
		$data['venue_spaces'] = $this->user_model->get_venues_spaces_list();
		$data['venue_data']	= $this->user_model->get_venue_by_id($venue_id,$user_id);
		$data['venue_gallery']	= $this->user_model->getResultsByCondtion('venue_gallery',array('user_id'=>$user_id,'venue_id'=>$venue_id));
		$this->load->view('includes/user_template',$data);
	}

	public function save_user_account(){
		$account = $this->user_model->getSingleRecord('bank_account',array('user_id'=>$this->session->userdata('user_id') ) );
		$array = array(
			'bank_name'=>$this->input->post('bank_name'),
			'account_holder_name'=>$this->input->post('account_holder_name'),
			'account_number'=>$this->input->post('account_number'),
			'swift_code'=>$this->input->post('swift_code'),
			'zip_code'=>$this->input->post('zip_code'),
			'bussiness_email'=>$this->input->post('bussiness_email'),
			'user_id'=>$this->session->userdata('user_id'),
		);
		if($account->num_rows()){
			$array['updated_at'] = date('Y-m-d H:i:s');
			$this->user_model->update('bank_account',array('user_id'=>$this->session->userdata('user_id') ), $array);
		}else{
			$array['created_at'] = date('Y-m-d H:i:s');
			$this->user_model->update('bank_account',array('user_id'=>$this->session->userdata('user_id') ), $array);
			$this->user_model->save('bank_account',$array);
		}
		die;
		
	}

	public function update_venue(){
		$days = '';
		$amenities = '';
		if($this->input->post('day')){
			$days = implode('/', $this->input->post('day'));
		}
		if($this->input->post('amenities')){
			$amenities = implode('/', $this->input->post('amenities'));
		}
		$venue_id = $this->input->post('venue_id');

		$post_data = array(
						'venues_space_id'	=>	$this->input->post('space_type'),
						'ownership_type'	=>	$this->input->post('ownership_type'),
						'space_title'		=>	$this->input->post('title'),
						'space_description'	=>	$this->input->post('description'),
						'space_area'	=>	$this->input->post('space_area'),
						'total_rooms'	=>	$this->input->post('total_rooms'),
						'available_days'	=>	$days,
						'available_from'	=>	$this->input->post('start_time'),
						'available_to'	=>	$this->input->post('end_time'),
						'hourly_rate'	=>	$this->input->post('hourly_rate'),
						'discount'	=>	$this->input->post('discount'),
						'capacity'	=>	$this->input->post('guests'),
						'hours'	=>	$this->input->post('hours'),
						'amenities'	=>	$amenities,
						'lattitude'	=>	$this->input->post('lattitude'),
						'longitude'	=>	$this->input->post('longitude'),
						'location'	=>	$this->input->post('location'),
						'updated_at' => date('Y-m-d H:i:s')
					);

		$this->user_model->update('venues',array('id'=>$venue_id,'user_id'=>$this->session->userdata('user_id')), $post_data);
	}

	function upload_venue_image($venueId=0){
		
		if (!empty($_FILES)) {
			$folder_path = './uploads/venue_gallery/';
				
			$config['upload_path']   = $folder_path; 
			$config['allowed_types'] = '*';
			$config['max_size']      = 0;

			$this->load->library('upload', $config);
			$this->upload->do_upload('file');
			$files_data = $this->upload->data();
			$file_url = base_url('uploads/venue_gallery/'.$files_data['file_name']);
			$thumb_url = base_url('uploads/venue_gallery_thumbnail/'.$files_data['file_name']);
			$images = array();
			if($this->session->userdata('venue_images')){
				$images = $this->session->userdata('venue_images');
			}
			
			//$this->session->set_userdata('venue_images', );
			$arra_data = array(
							'user_id'		=>	$this->session->userdata('user_id'),
							'file_name'		=>	$file_url,
							'thumb_url'		=>  $thumb_url,
							'venue_id'		=>	$venueId,
							'is_main'		=>	'0',
							'created_date'	=>	date('Y-m-d H:i:s'),
						);
			$last_id = $this->user_model->save('venue_gallery',$arra_data);
			
			echo json_encode(array('msg'=>'success','id'=>$last_id,'filename'=>$files_data['file_name']));
			$this->do_resize($files_data['file_name']);
		}else{ echo "Please select atleast one file!";}	
	}

	public function do_resize($filename)
	{
	    $source_path = FCPATH . '/uploads/venue_gallery/' . $filename;
	    $target_path = FCPATH . '/uploads/venue_gallery_thumbnail/'.$filename;
	    $config_manip = array(
	        'image_library' => 'gd2',
	        'source_image' => $source_path,
	        'new_image' => $target_path,
	        'maintain_ratio' => FALSE,
	        'create_thumb' => TRUE,
	        'thumb_marker' => '',
	        'width' => 450,
	        'height' => 388
	    );
	    $this->load->library('image_lib', $config_manip);
	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors();
	    }
	    // clear //
	    $this->image_lib->clear();
	}
	
}
