	<div class="we-page-title">
	<div class="row">
		<div class="col-md-8 align-self-left">
			<h3 class="we-page-heading">Approvals</h3> 
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo base_url('user/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li class="breadcrumb-item active">Approvals</li>
			</ol>
		</div>
		<div class="col-md-4 text-right">
			<?php $this->load->view('includes/practice_logo');?>
		</div>
	</div>
</div>  


<div class="invoice">
	<div class="row">
		<div class="col-xs-12 col-md-12 table-responsive">
			<table id="files_table" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr><th>Client Name</th><th>Filename / File Description</th><th>Last Updated</th><th>Status</th><th>Uploaded By</th></tr>
				</thead>
				
			</table>
        </div>
        <!-- /.col -->
    </div>
</div>
<script type="text/javascript">
var table = $('#files_table').DataTable( {
	"pageLength" : 25,
	"ajax": {
		url : "<?php echo site_url("client/get_approvals_files/").$status;?>",
		type : 'GET'
	},
} );
function reload_table(){
	table.ajax.reload(null,false);
}
</script>