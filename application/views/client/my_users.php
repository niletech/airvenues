    
<div class="we-page-title">
	<div class="row">
		<div class="col-md-8 align-self-left">
			<h3 class="we-page-heading">My Users</h3> 
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo base_url('user/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li class="breadcrumb-item active">My Users</li>
			</ol>
		</div>
		<div class="col-md-4 text-right">
			<?php $this->load->view('includes/practice_logo');?>
		</div>
	</div>
</div>  

<div class="invoice">
	<div class="row">
        <div class="col-xs-12 col-md-12 table-responsive">
			<div id="success_message"></div>
			<div class="box-header">
				<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_form">+ Add New User</button>
				&nbsp; 
				<a href="<?php echo base_url('client/folders/');?>" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> View Documents</a>
				
			</div>
			<table id="clients_table" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>
						<th style="width:40px;">S No.</th><th>Clients User</th><th>Email ID</th><th>Job Title</th><th>Status</th><th style="width:70px;">Action</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
        </div>
        <!-- /.col -->
    </div>

</div>

<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New Client User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
		<form id="form" method="post">
			<input type="hidden" id="user_id" name="user_id" />
			<input type="hidden" id="parent_client" name="parent_client" value="<?php echo $client_data->user_id;?>" />
			<div class="modal-body">
				<p>Use the form below to create a new client member</p>
				<div class="form-group row">
					<label for="first_name" class="col-form-label col-md-3">First Name <b class="text-danger">*</b></label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="first_name" name="first_name" required />
					</div>	
				</div>
				<div class="form-group row">
					<label for="surname" class="col-form-label col-md-3">Surname <b class="text-danger">*</b></label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="surname" name="surname" required />
					</div>
				</div>
				<div class="form-group row">
					<label for="email" class="col-form-label col-md-3">Email <b class="text-danger">*</b></label>
					<div class="col-md-9">
						<input type="email" class="form-control" id="email" name="email" required />
					</div>	
				</div>
				<div class="form-group row">
					<label for="job_title" class="col-form-label col-md-3">Job Title</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="job_title" name="job_title" />
					</div>	
				</div>
				<div class="form-group row">
					<label for="client_name" class="col-form-label col-md-3">Client Name <b class="text-danger">*</b></label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="client_name" name="client_name" value="<?php echo $client_data->client_name;?>" disabled />
					</div>
				</div>
				
				<div id="error_message"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary" id="invite_user">Invite User</button>
			</div>
		</form>
    </div>
  </div>
</div>
<link href="<?php echo site_url('assets/css/select2.min.css');?>" rel="stylesheet" />
<script src="<?php echo site_url('assets/js/select2.min.js');?>"></script>
<script type="text/javascript">
var table = $('#clients_table').DataTable( {
	"pageLength" : 25,
	"ajax": {
		url : "<?php echo site_url("user/get_client_users/").$client_data->user_id?>",
		type : 'GET'
	},
} );
function reload_table(){
	table.ajax.reload(null,false);
}
	
$(document).ready(function(){
	$('.modal').on('hidden.bs.modal', function(){
		$('[name="user_id"]').val('');
		$('#invite_user').text('Invite User');
		$('#form')[0].reset();
	});
	
	
	$("#form").on('submit',(function(e){
		e.preventDefault();
		$("button[type='submit']").prop('disabled', true);
		$.ajax({
            url : "<?php echo site_url("user/saveClientUser") ?>",
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
			success: function(data){
				if(data.status==true){
					$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
					$("button[type='submit']").prop('disabled', false);
					$('#form')[0].reset();
					$('#modal_form').modal('hide');
					table.ajax.reload( null, false );
				}else{
					$("button[type='submit']").prop('disabled', false);
					$('#error_message').html('<div class="alert alert-warning">'+data.message+'</div>');
				}
            },
            error: function (jqXHR, textStatus, errorThrown){
				$('#error_message').html('<div class="alert alert-warning">'+jqXHR.responseText+'</div>');
				
            }
            
        });
	}));

});

function editRecord (user_id){
	$('#info_text').hide();
	$('#form')[0].reset();
	$.ajax({
		url : "<?php echo site_url('user/edit_client/')?>" + user_id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			var rep_users = data.rep_ids;
			rep_arr = rep_users.split(',');
			$("#rep_ids").val( rep_arr ).trigger('change');
			$('[name="parent_client"]').val(data.parent_client);
			$('[name="user_id"]').val(data.user_id);
			$('[name="first_name"]').val(data.first_name);
			$('[name="surname"]').val(data.surname);
			$('[name="email"]').val(data.email);
			$('[name="job_title"]').val(data.job_title);
			
			$('#modal_form').modal('show'); 
			$('.modal-title').text('Edit User');
			$('#invite_user').text('Save Details');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error get data from ajax');
		}
	});
}
function deleteRecord(user_id){
	if(confirm('Are you sure delete this record?')){
	  $.ajax({
		url : "<?php echo site_url('user/delete_client')?>/"+user_id,
		type: "POST",
		dataType: "JSON",
		success: function(data){
			$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
			$('#modal_form').modal('hide');
			reload_table();
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error adding / update data');
		}
	});
	 
  }
}
</script>