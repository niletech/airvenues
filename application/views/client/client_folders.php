	<div class="we-page-title">
	<div class="row">
		<div class="col-md-8 align-self-left">
			<h3 class="we-page-heading">Files and Folders</h3> 
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo base_url('user/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<?php if($root_folder->is_root==1){?>
				<li class="breadcrumb-item active">Files and Folders</li>
				<?php }else{?>
				<li class="breadcrumb-item"><a href="<?php echo base_url('client/files_and_folders/');?>">Files and Folders</a></li>
				<li class="breadcrumb-item active"><?php echo $root_folder->folder_name;?></li>
				<?php }?>
			</ol>
		</div>
		<div class="col-md-4 text-right">
			<?php $this->load->view('includes/practice_logo');?>
		</div>
	</div>
</div>  


<div class="invoice">
	<div class="row">
		<div id="files-toolbar" class="col-md-12">
			<div id="success_message"></div>
			<div class="col-md-12 pull-left">
				<?php if($root_folder->is_root==0){?>
				<a href="<?php echo base_url('client/folders/');?>" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back</a> &nbsp; 
				<button id="openUploadBox" onclick="openUploadBox()" class="btn btn-sm btn-success"><i class="fa fa-upload"></i> Upload File</button> &nbsp; 
				<?php }?>
				<?php if( userViewRestriction( array(1,2,3,4)) ){?>
                <a href="<?php echo base_url('client/users/');?>" id="view-client-users" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View Users</a>
				<?php }?>
            </div>
        </div>
		
        <div class="col-xs-12 col-md-12 table-responsive">
			<table id="folder_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th><th>Description</th><th>Size</th><th style="width:70px;">Status</th><th style="width:20%;">Assigned Users</th><th style="width:200px; text-align:center;">Action</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
        </div>
        <!-- /.col -->
    </div>
</div>
<?php 
if($root_folder->is_root==1)
	$ajax_url = site_url("client/get_client_folders/".$client_data->user_id);
else
	$ajax_url = site_url("client/get_client_folders/".$client_data->user_id).'/'.$root_folder->id;

$data['user_id'] = $client_data->user_id;
$data['folder_id'] = $root_folder->id;
$this->load->view('includes/upload_file',$data);
?>


<div class="modal fade" id="folder_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Assign users to Folder</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
		<form id="form" method="post">
			<input type="hidden" id="folder_id" name="folder_id" />
			<div class="modal-body">
				<div class="form-group row">
					<label for="folder_name" class="col-form-label col-md-3">Folder <b class="text-danger">*</b></label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="folder_name" name="folder_name" disabled />
					</div>	
				</div>
				<div class="form-group row">
					<label for="client_name" class="col-form-label col-md-3">Users</label>
					<div class="col-md-9 rep_select2">
						<select class="form-control multiple-select" id="cl_ids" name="cl_ids[]" multiple="multiple">
						<?php 
						if($users_data->num_rows()>0 ){ 
							foreach($users_data->result() as $user){
								echo '<option value="'.$user->user_id.'">'.$user->first_name.' '.$user->surname.'</option>';
							} 
						}
						?>
						</select>
					</div>
				</div>
				<div id="error_message"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary" id="invite_user">Create Folder</button>
			</div>
		</form>
    </div>
  </div>
</div>
<link href="<?php echo site_url('assets/css/select2.min.css');?>" rel="stylesheet" />
<script src="<?php echo site_url('assets/js/select2.min.js');?>"></script>

<script type="text/javascript">
var table = $('#folder_table').DataTable({
	'ajax': {
	 'url': '<?php echo $ajax_url;?>'
	},
	 'columnDefs': [{
	 'targets': 5,
	 'searchable': false,
	 'orderable': false,
	 'className': 'dt-body-center',
	 /*'render': function (data, type, full, meta){
		return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
	 }*/
  }],
  'order': [[1, 'asc']]
});

function reload_table(){
	table.ajax.reload(null,false);
}

$(document).ready(function (){
	$(".multiple-select").select2({
		placeholder: "Select Users"
	});
	
	$("#form").on('submit',(function(e){
		e.preventDefault();
		$("button[type='submit']").prop('disabled', true);
		$.ajax({
            url : "<?php echo site_url("client/assignUsersToFolder") ?>",
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
			success: function(data){
				if(data.status==true){
					$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
					$("button[type='submit']").prop('disabled', false);
					$('#form')[0].reset();
					$('#folder_form').modal('hide');
					table.ajax.reload( null, false );
				}else{
					$("button[type='submit']").prop('disabled', false);
					$('#error_message').html('<div class="alert alert-warning">'+data.message+'</div>');
				}
            },
            error: function (jqXHR, textStatus, errorThrown){
				$('#error_message').html('<div class="alert alert-warning">'+jqXHR.responseText+'</div>');	
            }
        });
	}));
	
});

function editFolder(folder_id){
	$('#info_text').hide();
	$('#form')[0].reset();
	$.ajax({
		url : "<?php echo site_url('user/edit_folder/')?>" + folder_id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			var rep_users = data.cl_ids;
			rep_arr = rep_users.split(',');
			$("#cl_ids").val( rep_arr ).trigger('change');
			$('[name="folder_id"]').val(data.id);
			$('[name="parent_client"]').val(data.user_id);
			$('[name="parent_folder"]').val(data.parent_id);
			$('[name="folder_name"]').val(data.folder_name);
			$('#folder_form').modal('show'); 
			$('.modal-title').text('Assign User to '+data.folder_name);
			$('#invite_user').text('Save');
			
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Error get data from ajax');
		}
	});
}

function changeStatus(file_id,file_status){
	$.ajax({
		url : "<?php echo site_url('user/changeFileStatus/')?>",
		type: "POST",
		dataType: "JSON",
		data: {file_id:file_id,file_status:file_status},
		success: function(data){
			if(data.status==true){
				$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
			}else{
				$('#error_message').html('<div class="alert alert-warning">'+data.message+'</div>');
			}
			reload_table();
		},
		error: function (jqXHR, textStatus, errorThrown){
			$('#success_message').html('<div class="alert alert-warning">'+jqXHR.responseText+'</div>');
			
		}
	});
}
function deleteRecord(file_id){
	if(confirm('Are you sure you want to permanently delete this file?')){
	  $.ajax({
		url : "<?php echo site_url('user/delete_file')?>/"+file_id,
		type: "POST",
		dataType: "JSON",
		success: function(data){
			$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
			$('#modal_form').modal('hide');
			reload_table();
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error adding / update data');
		}
	});
  }
}
</script>