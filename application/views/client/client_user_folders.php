	<div class="we-page-title">
	<div class="row">
		<div class="col-md-8 align-self-left">
			<h3 class="we-page-heading">Files and Folders</h3> 
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo base_url('user/dashboard');?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<?php if($root_folder->is_root==1){?>
				<li class="breadcrumb-item active">Files and Folders</li>
				<?php }else{?>
				<li class="breadcrumb-item"><a href="<?php echo base_url('client/folders/');?>">Files and Folders</a></li>
				<li class="breadcrumb-item active"><?php echo $root_folder->folder_name;?></li>
				<?php }?>
			</ol>
		</div>
		<div class="col-md-4 text-right">
			<?php $this->load->view('includes/practice_logo');?>
		</div>
	</div>
</div>  


<div class="invoice">
	<div class="row">
		<div id="files-toolbar" class="col-md-12">
			<div id="success_message"></div>
			<div class="col-md-12 pull-left">
				<?php if($root_folder->is_root==0){?>
				<a href="<?php echo base_url('client/folders/');?>" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back</a> &nbsp; 
				<button id="openUploadBox" onclick="openUploadBox()" class="btn btn-sm btn-success"><i class="fa fa-upload"></i> Upload File</button> &nbsp; 
				<?php }?>
				<?php if( userViewRestriction( array(1,2,3,4)) ){?>
                <a href="<?php echo base_url('client/users/');?>" id="view-client-users" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View Users</a>
				<?php }?>
            </div>
        </div>
		
        <div class="col-xs-12 col-md-12 table-responsive">
			<table id="folder_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th><th>Description</th><th style="width:100px; text-align:center;">Action</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
        </div>
        <!-- /.col -->
    </div>
</div>
<?php 
if($root_folder->is_root==1)
	$ajax_url = site_url("client/get_clients_user_folders/".$client_data->user_id);
else
	$ajax_url = site_url("client/get_clients_user_folders/".$client_data->user_id).'/'.$root_folder->id;

$data['user_id'] = $client_data->user_id;
$data['folder_id'] = $root_folder->id;
$this->load->view('includes/upload_file',$data);
?>

<script type="text/javascript">
var table = $('#folder_table').DataTable({
	'ajax': {
	 'url': '<?php echo $ajax_url;?>'
	},
	 'columnDefs': [{
	 'targets': 2,
	 'searchable': false,
	 'orderable': false,
	 'className': 'dt-body-center',
  }],
  'order': [[1, 'asc']]
});

function reload_table(){
	table.ajax.reload(null,false);
}

$(document).ready(function (){
	
	
	
});

function changeStatus(file_id,file_status){
	$.ajax({
		url : "<?php echo site_url('user/changeFileStatus/')?>",
		type: "POST",
		dataType: "JSON",
		data: {file_id:file_id,file_status:file_status},
		success: function(data){
			if(data.status==true){
				$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
			}else{
				$('#error_message').html('<div class="alert alert-warning">'+data.message+'</div>');
			}
			reload_table();
		},
		error: function (jqXHR, textStatus, errorThrown){
			$('#success_message').html('<div class="alert alert-warning">'+jqXHR.responseText+'</div>');
			
		}
	});
}
function deleteRecord(file_id){
	if(confirm('Are you sure you want to permanently delete this file?')){
	  $.ajax({
		url : "<?php echo site_url('user/delete_file')?>/"+file_id,
		type: "POST",
		dataType: "JSON",
		success: function(data){
			$('#success_message').html('<p class="alert alert-success">'+data.message+'</p>');
			$('#modal_form').modal('hide');
			reload_table();
		},
		error: function (jqXHR, textStatus, errorThrown){
			alert('Error adding / update data');
		}
	});
  }
}
</script>