<div class="modal fade" id="file_upload_modal" tabindex="-1" role="dialog" aria-labelledby="file_uploadModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="file_uploadModalLabel">Upload Files</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<form action="<?php echo base_url('user/upload_file')?>" enctype="multipart/form-data" class="dropzone" id="my-dropzone" method="POST">
				<input type="hidden" name="folder_id" value="<?php echo $folder_id;?>" />
				<input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
			</form>
			 <button class="btn btn-primary" id="submit-all">Upload Files</button>
		</div>
    </div>
  </div>
</div>

<link href="<?php echo base_url('assets/css/dropzone.min.css');?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/dropzone.min.js');?>"></script>
<script type="text/javascript">
	Dropzone.options.myDropzone = {
        maxFilesize:10,
		uploadMultiple: true,
		parallelUploads: 15,
		maxFiles: 15,
		addRemoveLinks: true,
		autoProcessQueue: false,
		init: function() {
			  var submitButton = document.querySelector("#submit-all")
				myDropzone = this; // closure
			submitButton.addEventListener("click", function() {
			  myDropzone.processQueue(); // Tell Dropzone to process all queued files.
			});
		// You might want to show the submit button only when 
			// files are dropped here:
			this.on("addedfile", function() {
			  // Show submit button here and/or inform user to click it.
			});
		},
		success: function (file, responseText){
            location.reload();
        },
    };
	
	
</script>