<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?php echo $title;?></title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png');?>" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css');?>">
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
	</script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/owlcarousel/js/owl.carousel.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pricerange.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/function.js');?>"></script>
</head>
<body id="public">
	<div id="main-site">
	
		<?php $this->load->view('includes/header');?>
		
		<div class="main-page">
            	<?php $this->load->view($main_content);?>
		</div>
		
		<?php $this->load->view('includes/footer');?>
		
	</div>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
	<script type="text/javascript">
		$( document ).ready( function () {
			$( "#signupForm" ).validate( {
				rules: {
					first_name: "required",
					last_name: "required",
					email: {
						required: true,
						email: true,
						remote: {
							url: '<?php echo base_url('site/check_user_email')?>',
							type: "POST"
						}
					},
					password:{
					   required: true,
					   minlength: 6
					},
					confirm_password: {
						required: true,
						minlength : 6,
						equalTo : "#password"
					},
					phone: "required",
				},
				messages: {
					first_name: "Please enter your first name",
					last_name: "Please enter your last name",
					email:{
						required: "Please enter a valid email address",
						remote: "Email already in use!",
					},
					password: {
						required: "What is your password?",
						minlength: "Password must contain more than 6 characters"
					},
					confirm_password: {
						required: "You must confirm your password",
						minlength: "Password must contain more than 6 characters",
						passwordMatch: "Your Passwords Must Match"
					},
					phone: "Please enter a valid phone number"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
				},
				
			} );
		} );
	</script>
</body>
</html>