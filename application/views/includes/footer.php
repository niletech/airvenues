

<div class="footer">

	<div class="container">

		<div class="row">

			<div class="col-md-3 col-lg-3 col-sm-3">

				<div class="footer-middle-widget">

					<div class="footer-title">

						<h2>Get in touch</h2>

					</div>						

					<div class="footer-middle-content">

						<ul class="fwidget-list">

						    <li class="f-address"><i class="fa fa-map-marker" aria-hidden="true"></i> Greater Denver Area, Colorado</li>

						    <li><a href="tel:+1 303-968-9560"><i class="fa fa-phone" aria-hidden="true"></i> +1 303-968-9560</a></li>

						    <li><a href="mailto:admin@airvenues.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> admin@airvenues.com</a></li>

						</ul>

					</div>

				    <div class="f-social">

					    <ul>

					        <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>

					        <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>

					        <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>

					    </ul>

					</div>                   

				</div>

			</div>



			<div class="col-md-6 col-lg-6 col-sm-6">

				<div class="footer-widget">

					<div class="footer-title">

						<h2>ACTIVITIES</h2>

					</div>

					<div class="row">

						<div class="col-md-6 col-lg-6 col-sm-6">

							<div class="footer-links">

								<ul id="menu-footer-menu">

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Meetings</a></li>

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Weddings</a></li>

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Birthday</a></li>

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Dinners</a></li>	

								</ul>                    

							</div>

						</div>

						<div class="col-md-6 col-lg-6 col-sm-6">

							<div class="footer-links">

								<ul id="menu-footer-menu">

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Corporate  Event</a></li>

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i>Launch Parties</a></li>

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Trainings</a></li>

									<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> See all activities</a></li>	

								</ul>                    

							</div>

						</div>

					</div>

				</div>

			

			</div>



			<div class="col-md-3 col-lg-3 col-sm-3">

				<div class="footer-widget">

					<div class="footer-title">

						<h2>Quick links</h2>

					</div>

					<div class="footer-links">

						<ul id="menu-footer-menu">

							<li><a href="<?php echo base_url('/about'); ?>"><i class="fa fa-caret-right" aria-hidden="true"></i> About Us</a></li>

							<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Contact Us</a></li>

							<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Privacy</a></li>

							<li><a href="javascript:void(0);"><i class="fa fa-caret-right" aria-hidden="true"></i> Blog</a></li>								

						</ul>                    

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<div class="footer-bottom">

	<div class="container">

		<div class="copyright">

			<div class="textwidget"><p>Copyright &copy; <span><?php echo date('Y');?> AirVenues All Rights Reserved.<span style="margin-left:10px;">Powered By: <a href="http://www.niletechnologies.com/" target="_blank">Nile Technologies</a></span></p></div>

		</div>

	</div>

</div>

</body>

</html>