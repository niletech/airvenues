<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title><?php echo $title;?></title>
	<meta name="description" content="<?php echo $description;?>">
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png');?>" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Libre+Baskerville:400,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/lightbox.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';
	</script>
</head>
<body>
	<div class="loader_bg">
		<div class="loader"></div>
	</div>
	<div class="wrapper">
       	<?php $this->load->view($main_content);?>
	</div>
		
	
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
	<script src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/particles/particles.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/particles/particles_2.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/lightbox.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/script.js');?>"></script>

	<script type="text/javascript">
		$( document ).ready( function () {
			$( "#signupForm" ).validate( {
				rules: {
					firstname: "required",
					lastname: "required",
					email: {
						required: true,
						email: true,
						remote: {
							url: '<?php echo base_url('site/check_user_email')?>',
							data: {
								action: 'check_user'
							},
							type: "post",
							dataFilter: function(data) {
								if(data == 'Exists'){
								  return false;
								}else{
								  return true;
								}
							}
						}
					},
					phone: "required",
				},
				messages: {
					firstname: "Please enter your first name",
					lastname: "Please enter your last name",
					email: "Please enter a valid email address",
					remote: "This Email address is already in use.",
					phone: "Please enter a valid phone number"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
				},
				
			} );

		} );
	</script>
	
</html>