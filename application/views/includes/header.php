<header class="header sticky-header">
	<div class="container">
		<div class="main-menu">
			<nav class="navbar navbar-expand-lg">  
				<div class="navbar-brand logo">
					<a href="<?php echo base_url();?>" class="custom-logo-link"><img src="<?php echo base_url('assets/images/logo-hor.svg');?>"></a>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				     
				<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="javascript:">how it works</a>
						</li>						
						
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('site/step1');?>">List Your Space</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="javascript:">Blog</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="javascript:">Contact Us</a>
						</li>
					</ul>
					<?php if(!logged_in()){?>
					<ul class="navbar-nav">
				      <li class="nav-item">
				        <a class="login-btn" href="<?php echo base_url('login');?>">Log in</a>
				      </li>
				    </ul>
					<?php }else{?>
					<ul class="navbar-nav profile_menu">
				      <li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="javascript:" id="navDropDownLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<!--img class="rounded" id="profile_photo" src="https://img.peerspace.com/image/upload/c_crop,g_custom/v1526048552/kum1a526brbpsozg700p.jpg"-->
						Hi, <?php echo $this->session->userdata('user_name');?></a>
						<div class="dropdown-menu" aria-labelledby="navDropDownLink">
							<a class="dropdown-item" href="<?php echo base_url('user/dashboard');?>">My Profile</a>
							<a class="dropdown-item" href="<?php echo base_url('user/my_venues/');?>">My Venues</a>
							<div class="dropdown-divider"></div>
							<!-- <a class="dropdown-item" href="<?php echo base_url('site/sign_out');?>">List a Space</a> -->
							<a class="dropdown-item" href="<?php echo base_url('site/sign_out');?>">Logout</a>
						</div>
				      </li>
				    </ul>
					<?php }?>
				</div>
			</nav>  
		</div>
	</div>
</header>
