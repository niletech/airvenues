<div class="user-infor-section">
<div class="user-infor-login">
    <div class="container">
        <div class="we-main-login">
            <div class="row">
				<div class="col-md-6 offset-md-3">
                    <div class="we-login-content-box">
                        <div class="we-heading">
							<h3>Forgot your password?</h3>
                            <p>Please enter your email address.<br />
							We will send you an email with instructions to reset your password.</p>
                        </div>

                        <?php //$this->load->view('includes/error');?>
                        <div class="we-form-box">
                            <form action="<?= base_url('site/forgot_password');?>" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" required />
                                    <label class="flot">Email Address</label>
								<?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group submit">
                                    <button type="submit" value="Send" class="Submit-form-button">Reset password</button>
                                </div>
                                <div class="form-group">
                                    <div class="login-form-link1">
                                        <a href="<?php echo base_url('login');?>">< Back to Login</a>
                                    </div>
                                    <div class="login-form-link1">
                                        Don't have an account? <a href="<?php echo base_url('register');?>">Sign Up</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

	<div class="_161wlni"></div>	
</div>
