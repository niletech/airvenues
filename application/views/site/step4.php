<?php 
$session_data = $this->session->userdata('step4');

?>
<section class="slide-content mainexamscreen">
	<div class="header-overlay ">
		<label class="listing-label">STEP 4: YOUR PROFILE</label>
	</div>
	<div class="u-mob-sidebar">
		<div class="tp-left-box">
			<div class="tp-q-box">
				<div class="tu-q-body">
                    <div class="tu-q-body-inner">
						<div class="ar-field">
							<div class="step-infor-box-content">
								<h2>Who will guests be contacting?</h2>
								<form action="<?php echo base_url('step4');?>" method="post" id="step4">
									<div class="we-form-box">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" class="form-control" name="fname" value="<?php if($session_data['first_name']){ echo $session_data['first_name']; } else{ echo $data->first_name; }?>" required >
													<label>First Name *</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" class="form-control" name="lname" value="<?php if($session_data['last_name']){ echo $session_data['last_name']; } else{ echo $data->last_name; }?>"  >
													<label>Last Name *</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<span class="form-control"><?= $this->session->userdata('user_email');?></span>
													
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" class="form-control" name="phone" value="<?php if($session_data['phone']){ echo $session_data['phone']; } else{ echo $data->phone; }?>" >
													<label>Phone *</label>
												</div>
											</div>	
										</div>
									</div>
								</form>
							
								<div class="we-form-box">
									<h2>Add a profile photo with your face</h2>
									
									<div class="profile_image_box">
										<?php if($data->profile_image==''){?>
											<img src="<?php echo base_url('assets/images/default-profile.jpg');?>" class="m-x-auto img-fluid profile_img" alt="avatar" width="160" />
										<?php }else{ ?>
											<img src="<?php echo $data->profile_image;?>" class="m-x-auto img-fluid profile_img" width="160"  alt="avatar" />
										<?php }?>
									</div>
									<div class="upload-btn-wrapper">
										<button class="btn">Upload a file</button>
										<input type="file" name="myfile" id="profile_img">
									</div>
									<input type="hidden" name="old_image_id" id="old_image_id" value="<?php if($data->profile_image!=''){ echo $data->profile_image; } ?>">

								</div>
							</div>
						</div>
					</div>
               
				</div>
				
				 <div class="tu-qp-action-controls">
					<div class="step-btn-box">
						<a class="btn back-btn" href="<?= base_url('step3');?>">Back</a>
						<button type="button" class="btn next-btn" onclick="validate_step4()">Save & Continue</button>
					</div>
					
                </div>
			</div>
		</div>
		<div id="sidebar" class="tp-right-box">
			<div class="tu-question-List">
				<div class="listing-help">
					<div class="listing-help-image">
						<img src="<?php echo base_url();?>/assets/images/step-4.png">
					</div>
					<div class="listing-help-text">
						<p><strong>1. Showcase your space</strong></p>
						<p><strong>2. Take photos when your space is well-lit.</strong></p>
                    	<p><strong>3. Don't forget the details</strong></p>
                    	<p><strong>4. Add photos of your space in action</strong></p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function validate_step4(){
		$( "#step4" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "checkbox" ) {
					//error.insertAfter( element.parent( "em" ) );
					error.insertAfter( '#week_days_error' );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});
		
		if ($('#step4').valid()){
			$('#step4').submit()
		}
	}
</script>
<link href="<?php echo base_url('assets/css/dropzone.min.css');?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/dropzone.min.js');?>"></script>
<script type="text/javascript">
	$("#profile_img").change(function(){
		var im = $('#old_image_id').val();
		var old = im.substring(im.lastIndexOf('/') + 1);
		var url = this.value;
	    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (this.files && this.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
        var reader = new FileReader();
        
        var fd = new FormData();
        var file_name = this.files[0];
        fd.append('file',file_name);
        fd.append('old_image',old);
       
        reader.onload = function (e) {
            $('.profile_img').attr('src', e.target.result);
                        
            $.ajax({
	            url: "<?php echo base_url('site/upload_profile_image');?>", 
	           	data: fd,
	            processData: false,
	            contentType: false,
	            type: 'POST',       
	            success: function(data)   
	            {
	            	
	            }
            });
        }
       
        reader.readAsDataURL(this.files[0]);
    }
    else{
    	alert("Please choose valid image");
    	return false;
    }
});
</script>
<style type="text/css">
	.upload-btn-wrapper{
		padding: 10px 15px !important;
	}
</style>