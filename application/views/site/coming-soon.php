
    <div class="bg_img">
        <img src="<?php echo base_url('assets/images/image2.jpg');?>" alt="Air Venues">
    </div>
    <div class="overlay_gradient"></div>
    <div class="background_dots"></div>
    <div id="particles-js"></div>
    <div class="left color_white hideee">
        <div class="brand"><img src="<?php echo base_url('assets/images/logo.png');?>" class="img-fluid" alt=""></div>
        <div class="content">
            <h1 class="big-title font_serif ssa js-spanize">Coming Soon...</h1>
            <h2 class="font_serif">List your event space and <br>start earning some extra money.</h2>
            <p>Any space can be an event space. So if you've got a backyard with a view, a warehouse, <br>a garage full of tools or any other space that lends itself to fun register it here and share it with people looking for a <br>unique place to throw their next party, wedding, conference, or anything really.</p>
            <div class="space_5 hidden-xs"></div>
            <div class="btns">
                <a class="btn button-book-main shw" href="<?php echo base_url('site/register');?>">Register your venue</a>
                <div class=" btn button-contact-main shw" data-target="right">Contact us</div>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="close-mob"><i class="fa fa-close"></i></div>
        <div id="about">
            <h2 class="title font_bold">About us</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum magni minus nemo numquam quis rem
                repudiandae tempore? Architecto dolor exercitationem, impedit ipsam iste laudantium nesciunt odio
                repudiandae tempore? Architecto dolor exercitationem, impedit ipsam iste laudantium nesciunt odio
                pariatur perferendis quos rem!</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum magni minus nemo numquam quis rem
                repudiandae tempore? Architecto dolor exercitationem, impedit ipsam iste laudantium nesciunt odio
                pariatur perferendis quos rem!</p>
        </div>
        
        <div id="contact">
            <div class="container-fluid">
                <div class="row">
                    <div class="contact">
                        <h2 class="font_bold title">Contact Information</h2>
                        
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consequatur consequuntur eaque explicabo in incidunt ipsum laboriosam, reiciendis</p>
                        
                        <div class="space_10"></div>
                        <div class="item contact-info">
                            <img src="<?php echo base_url('assets/images/icons/map.png');?>" class="img-responsive" alt="">
                            <p class="color_grey font_bold">Greater Denver Area, Colorado</p>
                        </div>
                        <div class="item contact-info">
                            <img src="<?php echo base_url('assets/images/icons/envelope.png');?>" class="img-responsive" alt="">
                            <p class="color_grey font_bold"><a href="mailto:anash@airvenues.com">anash@airvenues.com</a></p>
                        </div>
                        <div class="item contact-info">
                            <img src="<?php echo base_url('assets/images/icons/telephone.png');?>" class="img-responsive" alt="">
                            <p class="color_grey font_bold"><a href="tel:+1 303 968 9560">+1 303-968-9560</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subscribe_section">
        <div class="close-mob"><i class="fa fa-close"></i></div>
        <div class=" overlay_black_75"></div>
        <div class="d_table">
            <div class="d_table_v">
                <h1 class="font_bold">Join AirVenues !</h1>
                <p>Join thousands of hosts who are making extra income by sharing their space for daily activities.</p>
                <div class="space_10"></div>
                <form class="form-register" id="signupForm" action="<?php echo base_url('site/step2');?>" method="post">
                    <div class="form-group">
                        <input type="text" name="firstname" id="firstname" placeholder="First Name *" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <input type="text" name="lastname" placeholder="Last Name *" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" placeholder="Email Address *" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" id="phone" placeholder="Phone *" class="form-control"/>
                    </div>                   
                    <div class="form-group text-center">
                        <button type="submit" value="Send" class="Submit-form-button">Get Started Now</button>
                    </div>
                </form>
                <div class="space_15"></div>
            </div>
        </div>
    </div>
    <div class="social oop">
        <ul class="list-inline   list-inline">
            <li class="list-inline-item"><a href="#"><i class="fa fa-facebook-official"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li class="list-inline-item"><a href="#"><i class="fa fa-youtube"></i></a></li>
        </ul>
    </div>
    <div class="copyright text-right oop">
        Copyright © 2018 AirVenues All Rights Reserved.<span style="margin-left:10px;">Powered By: <a href="http://www.niletechnologies.com/" target="_blank">Nile Technologies</a></span>
    </div>
    <div class="close-ss"></div>
