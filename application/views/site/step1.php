<?php  $session_data = $this->session->userdata('step1');  $type_of_space = json_decode(TYPE_OF_SPACE); ?>
<section class="slide-content mainexamscreen">
	<div class="header-overlay ">
		<label class="listing-label">STEP 1: ABOUT YOUR SPACE</label>
	</div>
	<div class="u-mob-sidebar">
		<div class="tp-left-box">
			<div class="tp-q-box">
				<div class="tu-q-body">
                    <div class="tu-q-body-inner">
						<div class="ar-field">
							<div class="step-infor-box-content">
								<h2>Tell us about your event space so that we can promote it to the world.</h2>
								<h2>What kind of event space do you have?</h2>
								<form  method="post" id="step1">
									<div class="we-form-box">
										<div class="form-group row">
											<div class="col-md-6">
												<select class="ar-select" id="space_type" name="space_type" required>
													<option value="">Select Space Type</option>
													<?php foreach($venue_spaces as $venue_space){ ?>
													<option value="<?php echo $venue_space->id;?>" <?php if($session_data && $session_data['space_id']==$venue_space->id){ echo 'selected'; }?>><?= $venue_space->space_title;?></option>
													
														<?php } ?>
													<option value="Others">Others</option>
												</select>
											</div>
											<div class="col-md-6">
												<div class="admin-contact">
													
												</div>
											</div>
										</div>
									</div>
									<div class="we-form-box">
										<div class="form-group row">
											<div class="col-md-12"><h2>What type of space is it?</h2></div>
											<div class="col-md-6">
												
												<select name="ownership_type" class="ar-select" required id="ownership_type">
													<option value=''>Select Space</option>
													<?php if(!empty($type_of_space)){ foreach($type_of_space as $space_key=>$space_value){ ?>
															<option value="<?php echo $space_value; ?>" <?php if($session_data['ownership_type']==$space_value){ echo "selected"; } ?>><?php echo $space_value; ?></option>
													<?php } } ?>
													<option value="Others">Others</option>
												</select>
											</div>
											<div class="col-md-6 space-message"></div>
										</div>
									</div>
									<h2> Space details</h2>
									<div class="we-form-box">
										<div class="form-group">
											<input type="text" class="form-control" name="title"  value="<?php if($session_data){ echo $session_data['space_title']; }?>" required autocomplete="off">
											<?php if(form_error('title')){ echo '<em id="title-error" class="error" for="title">'.form_error('title').'</em>'; } ?>
											<label>Listing Title *</label>
										</div>
										<div class="form-group">
											<textarea name="description" type="text" class="form-control"  required=""><?php if($session_data){ echo $session_data['space_description']; }?></textarea>
											<label>Listing Details *</label>
										</div>
									</div>
									<div class="we-form-box">
										<div class="row">
											
											<div class="col-md-12">
												<div class="form-group">
													<input type="text" value="<?php if($session_data){ echo $session_data['location']; }?>" class="form-control controls" name="location" required id="pac-input" placeholder=''>

							                        <input type="hidden" name="lattitude" id="lattitude">
							                        <input type="hidden" name="longitude" id="longitude">
							                         <div id="map"></div>
														<label>Location *</label>
												</div>	
											</div>		
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
               
				</div>
				
				 <div class="tu-qp-action-controls">
					<div class="step-btn-box">
						<button type="button" class="btn next-btn" onclick="validate_step1()">Save & Continue</button>
					</div>
					
                </div>
			</div>
		</div>
		<div id="sidebar" class="tp-right-box">
			<div class="tu-question-List">
				<div class="listing-help">
					<div class="listing-help-image">
						<img src="<?php echo base_url();?>/assets/images/step-1.png">
					</div>
					<div class="listing-help-text">
						<p><b>Venue name and description Grab attention and inspire your potential customers with a creative venue title and description Please do not include your name, a businessname or contact details.</b></p>
						<ul class="desc_wrap">
							<li>
								<span class="desc_title_helper">Kind of events - </span>
								<span class="desc_title_helper_examples">loft, studio, dance hall, penthouse</span>
							</li>
							<li>
								<span class="desc_title_helper">Type of space - </span>
								<span class="desc_title_helper_examples">Private, Commercial, Non-profit</span>
							</li>
							<li>
								<span class="desc_title_helper">Space title - </span>
								<span class="desc_title_helper_examples">Give your space or venue title</span>
							</li>
							<li>
								<span class="desc_title_helper">Description - </span>
								<span class="desc_title_helper_examples">Basic information about your venue or space.</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script  src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY; ?>&libraries=places&callback=initAutocomplete"
  async defer></script>
<script>
	function initAutocomplete() {
		var _lat = 39.724049;
		var _lng = -105.001703;
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: _lat , lng: _lng },
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        $('#lattitude').val(_lat);
        $('#longitude').val(_lng);
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
          $('#lattitude').val(places[0].geometry.location.lat());
          $('#longitude').val(places[0].geometry.location.lng());

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];
           
          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            
          });
          map.fitBounds(bounds);
        });
      }
</script>
<script type="text/javascript">
	function validate_step1(){
		$( "#step1" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "radio" ) {
					//error.insertAfter( element.parent( "label" ) );
					error.insertAfter( "#ownership_type_field" );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});
		
		if ($('#step1').valid()){
			$('#step1').submit()
		}

	}

	$(document).on('change','#space_type',function(){
		var select_text = $(this).find("option:selected").text();
		if(select_text=='Not in listing'){
			$('.admin-contact').html('<span>Please contact to admin Email-id : <a href="mailto:admin@airvenues.com">admin@airvenues.com</a><br>Phone :  +1 303-968-9560</span>');
			$('.admin-contact').show();
			$(this).prop('selectedIndex',0);
		}else if($(this).val()=='Others'){
			$('.admin-contact').html('<input type="text" class="form-control custom-add-event"> <button class="btn btn-primary custom-event-save" type="button">Add</button>');
			$('.admin-contact').show();
		}else{
			$('.admin-contact').empty();
		}
	});

	$(document).on('click','.custom-event-save',function(){
		var c_event = $(".custom-add-event").val();
		if(!c_event){
			$(".custom-add-event").attr('placeholder','Enter your event').focus();
			return false;
		}else{
			if(confirm('Are you sure to add this event?.')){
				$.ajax({
					type : 'post',
					url : "<?php echo base_url('site/add_custom_event') ?>",
					data : {'event':c_event},
					dataType : 'json',
					beforeSend : function(){$(".custom-event-save").html('<i class="fa fa-spinner fa-spin"></i>').css('pointer-events','none');},
					success : function(res){
						if(res.status==2){
							$('#space_type').children('option').remove();
							$("<option/>",{"value": '',"text": 'Select Space Type'}).appendTo($("#space_type"));
							$.each(res.lists,function(i,v){
								$("<option/>",{"value": v.id,"text": v.space_title}).appendTo($("#space_type"));
							});
							$("<option/>",{"value": 'Others',"text": 'Others'}).appendTo($("#space_type"));
							$('.admin-contact').html('<p class="text-success">'+res.msg+'</p>');
						}else{
							$(".custom-add-event").val('').attr('placeholder',res.msg).focus();
							$(".custom-event-save").html('Add').removeAttr('style');
						}
					}
				});
			}
		}
	});


	// type of space
	$(document).on('change','#ownership_type',function(){
		var select_text = $(this).find("option:selected").text();
		if($(this).val()=='Others'){
			$('.space-message').html('<input type="text" class="form-control custom-space"> <button class="btn btn-primary custom-space-save" type="button">Add</button>');
			$('.space-message').show();
			$(this).prop('selectedIndex',0);
		}else{
			$('.space-message').empty();
		}
	});

	$(document).on('click','.custom-space-save',function(){
		/*var c_event = $(".custom-space").val();
		if(!c_event){
			$(".custom-space").attr('placeholder','Enter your type of space').focus();
			return false;
		}else{
			if(confirm('Are you sure to add this type of space?.')){
				$.ajax({
					type : 'post',
					url : "<?php echo base_url('site/add_custom_type_of_space') ?>",
					data : {'event':c_event},
					dataType : 'json',
					beforeSend : function(){$(".custom-space-save").html('<i class="fa fa-spinner fa-spin"></i>').css('pointer-events','none');},
					success : function(res){
						if(res.status==2){
							$('#ownership_type').children('option').remove();
							$("<option/>",{"value": '',"text": 'Select Space Type'}).appendTo($("#space_type"));
							$.each(res.lists,function(i,v){
								$("<option/>",{"value": v.id,"text": v.space_title}).appendTo($("#ownership_type"));
							});
							$("<option/>",{"value": 'Others',"text": 'Others'}).appendTo($("#ownership_type"));
							$('.space-message').html('<p class="text-success">'+res.msg+'</p>');
						}else{
							$(".custom-space").val('').attr('placeholder',res.msg).focus();
						}
					}
				});
			}
		}*/
	});

</script>
<style type="text/css">
	.admin-contact{
		display: none;
	}
	.custom-add-event,.custom-space{
	    float: left;
	    width: 75%!important;
	    margin-right: 15px;
	}
	.custom-event-save{

	}
	select.ar-select{
		width: 100%!important;
	}
</style>