<section class="breadcrumb-section login-breadcrum">
	<div class="breadcrumb-media">
	    <img src="<?php echo base_url('assets/images/slide2.jpg');?>" class="main-slider-img-fluid img-fluid">
	</div> 
</section>


<div class="internal-page-section"> 
    <div class="container">
		<h1>AirVenues Privacy Policy</h1>
		<p>
		This Privacy Policy explains how AirVenues, Inc. (“AirVenues” or “we”) collects, uses, and shares personal information. As used in this Privacy Policy, “you” may refer to either a user of AirVenues products or services (“Services”); or a person providing visiting our website, using our apps, or interacting with us. This Privacy Policy describes our use of information that identifies or might reasonably identify you (“personal information”).</p>
		<p>We may change this Privacy Policy from time to time. If we make changes, we will notify you by revising the date at the top of the policy. In some cases, we may provide you with additional notice, such as adding a statement to our homepage or sending you an email notification. We encourage you to review the Privacy Policy whenever you use our Services or apps, or visit our website to understand how we use personal information and the ways you can help protect your privacy.</p>
		<p>If you have questions about this Privacy Policy or our use of personal information, please contact us. Your use of the website or Services constitutes your acceptance of our use of your personal information as described in this Privacy Policy.</p>
		<h2>Collection of Information</h2>
		<h3>Information You Provide to Us</h3>
		<p>We may collect and use personal information that you provide directly to us. For example, we collect personal information when you participate in any interactive features of our Services, fill out a form, connect with us on social media sites, request customer support, or otherwise communicate with us. The types of personal information we may collect when you provide it to us include your name, email or physical address, company information, payment information, pictures, descriptions of properties, and other information you choose to provide.</p>
		<h3>No use for children under the age of 18.</h3>
		<p>The Services are not intended for children under 18 years of age. If you are under 18, do not use or provide any personal information on or through the Services or about yourself to us. If you believe that we might have personal information from or about a child under 18, please contact us.</p>
		<h3>Contact Information</h3>
		<p>Please <a href="maito:help@airvenues.com">email us</a> with any questions or comments about this Privacy Policy.</p>
    </div>
</section>