	<section class="page_content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="card fat">
						<div class="card-body">
							<div class="login-box-inner">
							<?php
							if($user_data->num_rows()>0){
								$user = $user_data->row();
							?>
								<h4 class="text-center text-bold">Account created, e-mail confirmation required.</h4>
								<div class="form-text text-center">
									<p style="font-size:13px;">An activation e-mail has been sent to <b><?php echo $user->email;?></b>, click on the link in the e-mail to complete your registration.</p>
									<p class="text-muted" style="font-size:13px;">If you do not receive the confirmation message within a few minutes of signing up, please check your Spam or Junk folder. <br />
									If so, select the confirmation message and mark it Not Spam, which should allow future messages to get through.</p>
									<p class="text-danger" style="font-size:13px;">Do not forget to click the link in the confirmation message. Otherwise, you will not be able to log in.</p>
									<p class="text-center"><a href="<?php echo base_url('login');?>" class="btn btn-sm back-btn">Continue</a></p>
								</div>
							<?php }?>	
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>