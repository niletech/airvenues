<?php 
$session_data = $this->session->userdata('step2');
?>
<section class="slide-content mainexamscreen">
	<div class="header-overlay ">
		<label class="listing-label">STEP 5: SPACE PHOTOS</label>
	</div>
	<div class="u-mob-sidebar">
		<div class="tp-left-box">
			<div class="tp-q-box">
				<div class="tu-q-body">
                    <div class="tu-q-body-inner">
						<div class="ar-field">
							<div class="step-infor-box-content">
								<h2>Photos can make your listing stand out</h2>
								<ul>
									<li>Add up to 10 images of your space.</li>
									<li>You must submit a minimum of 4 images for your space to be approved.</li>
									<li>Make sure they are at least 1400px width and 1024px height.</li>
								</ul>	
								<p>
									<small>Submitting photos that reveal the identity of your space, or that you don't have the rights to use, is against AirVenues terms of service.</small>
								</p>
								<div class="we-form-box">
									<h2>Please add space photos</h2>
									<?php if($gallary->num_rows() > 0){  ?>
									<div class="space-media-info">
										<div class="row">
										<?php foreach($gallary->result() as $img){ 
										
										$fname = pathinfo($img->thumb_url);
										if(file_exists(FCPATH.'uploads/venue_gallery_thumbnail/'.$fname['basename'])){
									?>
									<div class="col-md-3 col-sm-6 col-lg-3 main-<?php echo $img->id; ?>">
									<div class="space-media-inner">
											<img src="<?php echo $img->thumb_url; ?>" class="img-thumbnail">
											<div class="space-media-content">
												<a class="cz-remove fa fa-trash text-danger" href="javascript:undefined;"  data-id="<?php echo $img->id; ?>" data-name="<?php echo $fname['basename']; ?>"></a>
												<a class="cz-view fa fa-eye text-muted" href="javascript:undefined;"  data-id="<?php echo $img->id; ?>" style="<?php if($img->is_main==1){ echo 'border: 1px solid #1e7e34'; } ?>" ></a>
											</div>
										</div>
									</div>
									<?php 
												} 
											} ?>

									</div>
								</div>
											<?php
										} 
									?>
									<form action="<?php echo base_url('site/upload_venue_image');?>" class="dropzone" id="profile"></form>						
								</div>
									
							</div>
						</div>
					</div>
               
				</div>
				<form action="<?php echo base_url('step5');?>" id="step5" method="post">
					<input type="hidden" name="complete" value="4" />
				</form>	
				
				 <div class="tu-qp-action-controls">
					<div class="step-btn-box">
						<a class="back-btn" href="<?= base_url('step4');?>">Back</a>
						<button type="button" class="btn next-btn" onclick="validate_step5()" style=" <?php if($gallary->num_rows()==0){echo 'pointer-events: none'; }?> ">Save & Continue</button>
					</div>
					
                </div>
			</div>
		</div>
		<div id="sidebar" class="tp-right-box">
			<div class="tu-question-List">
				<div class="listing-help">
					<div class="listing-help-image">
						<img src="<?php echo base_url();?>/assets/images/step-5.png">
					</div>
					<div class="listing-help-text">
						<p><strong>1. Showcase your space</strong></p>
						<p><strong>2. Take photos when your space is well-lit.</strong></p>
                    	<p><strong>3. Don't forget the details</strong></p>
                    	<p><strong>4. Add photos of your space in action</strong></p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<style type="text/css">
	
	.space-media-content {
	    margin-top: 5px;
	    margin-bottom: 5px;
	}
	.space-media-inner {
	    text-align: center;
	}
	.space-media-content a.cz-remove.fa.fa-trash.text-danger {
	    box-shadow: 0 0 30px #eee;
	    width: 30px;
	    height: 30px;
	    line-height: 30px;
	    border-radius: 3px;
	    border: 1px solid #eee;
	}	
	a.dz-view.fa.fa-eye{
		box-shadow: 0 0 30px #eee;
		width: 30px;
		height: 30px;
		line-height: 30px;
		border-radius: 3px;
		border: 1px solid #eee;
		position: relative;
		left: -32px;
		top: -25px;
		cursor: pointer;
    	float: right;
	}
	a.dz-view.fa.fa-eye:before {
	    padding-left: 7px;
	}
	a.dz-remove.fa.fa-trash{
		box-shadow: 0 0 30px #eee;
		width: 30px;
		height: 30px;
		line-height: 30px;
		border-radius: 3px;
		border: 1px solid #eee;
		position: relative;
		left: 24px;
		top: 5px;
	}
	a.cz-view.fa.fa-eye {
	    box-shadow: 0 0 30px #eee;
	    width: 30px;
	    height: 30px;
	    line-height: 30px;
	    border-radius: 3px;
	    border: 1px solid #eee;
	}
	.space-media-info {
	    border: 1px solid #eee;
	    padding: 10px;
	    margin-bottom: 10px;
	}
</style>
<link href="<?php echo base_url('assets/css/dropzone.min.css');?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/dropzone.min.js');?>"></script>
<script type="text/javascript">
	function validate_step5(){
		$('#step5').submit();
	}


	Dropzone.options.profile = {
		
	  init: function() {
	  	
	  	this.id = 0;
	  
	    this.on("success", function(file,resp) { 
	    	var res = $.parseJSON(resp);
	    	$(".next-btn").addClass('disabled').css('pointer-events','none');
	    	if(res.msg=='success'){
	    		this.id = res.id;
	    		
	    		$(file.previewTemplate).children('.dz-remove').attr('image_id',res.id);
	    		$('<a class="dz-view fa fa-eye text-muted" href="javascript:undefined;" data-id="'+this.id+'"></a>').insertAfter($(file.previewTemplate).children('.dz-remove'));
	    	}
	    }),

	    this.on("complete", function(file) {
           $(".dz-remove").text('').addClass('fa fa-trash text-danger');
           $(".next-btn").removeClass('disabled').removeAttr('style');
       	}),
       	this.on('addedfile',function(file){
       		$(".next-btn").addClass('disabled').css('pointer-events','none');
       	})
	  }
	};

	Dropzone.autoDiscover = false; 
	$(".dropzone").dropzone({
	 addRemoveLinks: true,
	
	 removedfile: function(file) { 
	 	var name = file.name;
	 var image_id = $(file.previewTemplate).children('.dz-remove').attr('image_id');
	  $.ajax({
		   type: 'POST',
		   url: '<?php echo base_url(); ?>site/remove_drop_box_file/'+image_id,
		   data: {name: name,'folder_name':'venue_gallery'},
		   success: function(data){
	    
	   	   }
	  });
	  var _ref;
	  return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
	 }
	});

	$(document).on('click','.dz-view, .cz-view',function(){
		var image_id = $(this).data('id');
		var self = $(this);
		if(confirm('Are you sure to make this image is default image?')){
			$.ajax({
				type : 'post',
				url : "<?php echo base_url('site/make_default_image') ?>/"+image_id,
				success : function(res){
					
					if(self.hasClass('text-muted')){
						$('.dz-view, .cz-view').removeAttr('style').removeClass('text-success').addClass('text-muted');
						self.removeClass('text-muted');
						self.addClass('text-success').css('border','1px solid #1e7e34');
					}else{
						$('.dz-view, .cz-view').removeAttr('style').removeClass('text-success').addClass('text-muted');
						self.addClass('text-success').css('border','1px solid #1e7e34');
					}
					
				}
			});
		}
		
	})

	$(document).on('click','.cz-remove',function(){
		var image_id = $(this).data('id');
		var name = $(this).data('name');
		  $.ajax({
			   type: 'POST',
			   url: '<?php echo base_url(); ?>site/remove_drop_box_file/'+image_id,
			   data: {name: name,'folder_name':'venue_gallery'},
			   success: function(data){
			   	console.log('deleted');
		    		$(".main-"+image_id).remove();
		   	   }
		  });
	});
</script>
