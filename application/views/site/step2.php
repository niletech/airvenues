<?php 
$session_data = $this->session->userdata('step2');
$days_arr = array();
if(isset($session_data['available_days'])){
	$days_arr = explode('/', $session_data['available_days']);
}


?>
<section class="slide-content mainexamscreen">
	<div class="header-overlay ">
		<label class="listing-label">STEP 2: AVAILABILITY</label>
	</div>
	<div class="u-mob-sidebar">
		<div class="tp-left-box">
			<div class="tp-q-box">
				<div class="tu-q-body">
                    <div class="tu-q-body-inner">
						<div class="ar-field">
							<div class="step-infor-box-content">
								<h2>When is your space normally open for bookings?</h2>
								<form action="<?php echo base_url('step2');?>" method="post" id="step2">
									<div class="currency-change-list">
										<p>Select when your space is available</p>
										<em id="week_days_error"></em>
										<ul>
											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Sun" name='day[]' id="checkbox1" required <?php if(in_array('Sun', $days_arr)){ ?> checked="checked" <?php } ?>>
													<label for="checkbox1">Sun</label>
												</div>
											</li>

											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Mon" name='day[]' id="checkbox2" required <?php if(in_array('Mon', $days_arr)){ ?> checked="checked" <?php } ?> >
													<label for="checkbox2">Mon</label>
												</div>
											</li>						        

											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Tue"  name='day[]'id="checkbox3" required <?php if(in_array('Tue', $days_arr)){ ?> checked="checked" <?php } ?> >
													<label for="checkbox3">Tue</label>
												</div>
											</li>						        

											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Wed" name='day[]' id="checkbox4" required <?php if(in_array('Wed', $days_arr)){ ?> checked="checked" <?php } ?> >
													<label for="checkbox4">Wed</label>
												</div>
											</li>

											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Thu" name='day[]' id="checkbox5" required <?php if(in_array('Thu', $days_arr)){ ?> checked="checked" <?php } ?> >
													<label for="checkbox5">Thu</label>
												</div>
											</li>


											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Fri" name='day[]' id="checkbox6" required <?php if(in_array('Fri', $days_arr)){ ?> checked="checked" <?php } ?> >
													<label for="checkbox6">Fri</label>
												</div>
											</li>
											<li>
												<div class="checkbox-box">
													<input type="checkbox" value="Sat" name='day[]' id="checkbox7" required <?php if(in_array('Sat', $days_arr)){ ?> checked="checked" <?php } ?> >
													<label for="checkbox7">Sat</label>
												</div>
											</li>
										</ul>
									</div>
									<?php
									$time_slot_array = array(
										"01:00:00" => "1:00am", "01:15:00" => "1:15am", "01:30:00" => "1:30am", "01:45:00" => "1:45am",
										"02:00:00" => "2:00am", "02:15:00" => "2:15am", "02:30:00" => "2:30am", "02:45:00" => "2:45am",
										"03:00:00" => "3:00am", "03:15:00" => "3:15am", "03:30:00" => "3:30am", "03:45:00" => "3:45am",
										"04:00:00" => "4:00am", "04:15:00" => "4:15am", "04:30:00" => "4:30am", "04:45:00" => "4:45am",
										"05:00:00" => "5:00am", "05:15:00" => "5:15am", "05:30:00" => "5:30am", "05:45:00" => "5:45am",
										"06:00:00" => "6:00am", "06:15:00" => "6:15am", "06:30:00" => "6:30am", "06:45:00" => "6:45am",
										"07:00:00" => "7:00am", "07:15:00" => "7:15am", "07:30:00" => "7:30am", "07:45:00" => "7:45am",
										"08:00:00" => "8:00am", "08:15:00" => "8:15am", "08:30:00" => "8:30am", "08:45:00" => "8:45am",
										"09:00:00" => "9:00am", "09:15:00" => "9:15am", "09:30:00" => "9:30am", "09:45:00" => "9:45am",
										"10:00:00" => "10:00am", "10:15:00" => "10:15am", "10:30:00" => "10:30am", "10:45:00" => "10:45am",
										"11:00:00" => "11:00am", "11:15:00" => "11:15am", "11:30:00" => "11:30am", "11:45:00" => "11:45am",
										"12:00:00" => "12:00pm", "12:15:00" => "12:15pm", "12:30:00" => "12:30pm", "12:45:00" => "12:45pm",
										"13:00:00" => "1:00pm", "13:15:00" => "1:15pm", "13:30:00" => "1:30pm", "13:45:00" => "1:45pm",
										"14:00:00" => "2:00pm", "14:15:00" => "2:15pm", "14:30:00" => "2:30pm", "14:45:00" => "2:45pm",
										"15:00:00" => "3:00pm", "15:15:00" => "3:15pm", "15:30:00" => "3:30pm", "15:45:00" => "3:45pm",
										"16:00:00" => "4:00pm", "16:15:00" => "4:15pm", "16:30:00" => "4:30pm", "16:45:00" => "4:45pm",
										"17:00:00" => "5:00pm", "17:15:00" => "5:15pm", "17:30:00" => "5:30pm", "17:45:00" => "5:45pm",
										"18:00:00" => "6:00pm", "18:15:00" => "6:15pm", "18:30:00" => "6:30pm", "18:45:00" => "6:45pm",
										"19:00:00" => "7:00pm", "19:15:00" => "7:15pm", "19:30:00" => "7:30pm", "19:45:00" => "7:45pm",
										"20:00:00" => "8:00pm", "20:15:00" => "8:15pm", "20:30:00" => "8:30pm", "20:45:00" => "8:45pm",
										"21:00:00" => "9:00pm", "21:15:00" => "9:15pm", "21:30:00" => "9:30pm", "21:45:00" => "9:45pm",
										"22:00:00" => "10:00pm", "22:15:00" => "10:15pm", "22:30:00" => "10:30pm", "22:45:00" => "10:45pm",
										"23:00:00" => "11:00pm", "23:15:00" => "11:15pm", "23:30:00" => "11:30pm", "23:45:00" => "11:45pm",
									);
									?>
									<div class="we-form-box">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<select name="start_time" id="start_time" class="form-control select2" required>
														<option value=""></option>
														<?php foreach ($time_slot_array as $key => $val) { ?>
														<option value="<?php echo $key; ?>" <?php if($session_data && $session_data['available_from']==$key){ echo "selected"; } ?> ><?php echo $val; ?></option>
														<?php } ?>										
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<select name="end_time" id="end_time" class="form-control" required>
														<option value=""></option>
														<?php foreach ($time_slot_array as $key => $val) { ?>
														<option value="<?php echo $key; ?>" <?php if($session_data && $session_data['available_to']==$key){ echo "selected"; } ?> ><?php echo $val; ?></option>
														<?php } ?>										
													</select>
												</div>
											</div>
										</div>
										
									</div>
								</form>
							</div>
						</div>
					</div>
               
				</div>
				
				 <div class="tu-qp-action-controls">
					<div class="step-btn-box">
						<a class="btn back-btn" href="<?= base_url('step1');?>">Back</a>
						<button type="button" class="btn next-btn" onclick="validate_step2()">Save & Continue</button>
					</div>
					
                </div>
			</div>
		</div>
		<div id="sidebar" class="tp-right-box">
			<div class="tu-question-List">
				<div class="listing-help">
					<div class="listing-help-image">
						<img src="<?php echo base_url();?>/assets/images/step-2.png">
					</div>
					<div class="listing-help-text">
						<p><strong>Keep your availability up to date</strong></p>
						<p>Make sure this is accurate to avoid miscommunications and cancellations due to scheduling errors.</p>
                    	<p>Set your space's bookable hours for certain days of the week.<br />
                    	Ex. Mon-Fri 9am-5pm, Sat & Sun 12pm-4pm</p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<link href="<?php echo base_url('assets/css/select2.min.css');?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/select2.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#start_time").select2({
		placeholder: "Select start time",
		allowClear: true
	});
	$("#end_time").select2({
		placeholder: "Select end time",
		allowClear: true
	});
	
});

	function validate_step2(){
		$( "#step2" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "checkbox" ) {
					//error.insertAfter( element.parent( "em" ) );
					error.insertAfter( '#week_days_error' );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});
		
		if ($('#step2').valid()){
			$('#step2').submit()
		}
	}

</script>
	