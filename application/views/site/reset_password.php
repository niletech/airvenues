<div class="user-infor-section">
	<div class="user-infor-login">
		<div class="container">
			<div class="we-main-login">
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="we-login-content-box">
							<div class="we-form-box">
							<?php 
							if($success){
								echo '<div class="we-heading text-center"><h3>Password Changed</h3></div>';
								echo '<p class="text-center">You have successfully reset your password.<br />Please login to '.DEFAULT_SITE_TITLE.' by clicking the login button below.</p>';
								echo '<p class="text-center"><a href="'.base_url('login').'" class="button-book-main">Login</a></p>';
							} else {
							echo form_open(); 
							?>
								<div class="we-heading">
									<h3>Set New Password</h3>
								</div>
								<div class="form-group">
									<input id="password" type="password" class="form-control" name="password" value="<?php echo set_value('password'); ?>" required autofocus />
									<label class="flot" for="password">Password *</label>
									<?php if(form_error('password')){ echo '<label id="password-error" class="error" for="password">'.form_error('password').'</label>'; } ?>
								</div>
								<div class="form-group">
									<input id="password_conf" type="password" class="form-control" name="password_conf" value="<?php echo set_value('password_conf'); ?>" required />
									<label class="flot" for="password_conf">Confirm Password *</label>
									<?php if(form_error('password_conf')){ echo '<label id="password_conf-error" class="error" for="password_conf">'.form_error('password_conf').'</label>'; } ?>
								</div>
								<div class="form-group no-margin">
									<button type="submit" class="Submit-form-button">Set Password</button>
								</div>
							</form>
							<?php }?>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="_161wlni"></div>
</div>