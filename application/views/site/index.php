
<link rel="stylesheet" href="<?php echo base_url('assets/css/select2.min.css'); ?>">
<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>
<script>
	$(document).ready(function() {
	    $('.planning').select2({
			placeholder: 'What are you planning?',
			allowClear: true
		});
		$('.datepicker1').datepicker({
		     startDate: "+",
		     forceParse: true,
		});
	});
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY;?>&libraries=places&callback=initAutocomplete"
  async defer></script>
<script>
	function initAutocomplete() {
		var _lat = 39.724049;
		var _lng = -105.001703;
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: _lat , lng: _lng },
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        $('#lattitude').val(_lat);
        $('#longitude').val(_lng);
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
          $('#lattitude').val(places[0].geometry.location.lat());
          $('#longitude').val(places[0].geometry.location.lng());

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];
           
          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            
          });
          map.fitBounds(bounds);
        });
      }
</script>
<section class="main-slider-section main-slider-bg">	
    <div class="main-slider-content">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-5">
			    	 <div class="av-search-content-box">
					    <div class="we-heading">
					        <h2>Find a venue</h2>
					    </div>

					    <div class="av-search-form-box">
					        <form action="<?php echo base_url('site/venues/'); ?>" method="get">
					            <div class="row">
					                 <div class="col-md-12 col-sm-12 col-lg-12">
					                    <div class="form-group">
					                        <select class="planning form-control" name="p">
					                        	<option value=""></option>
					                        	<?php foreach(json_decode(TYPE_OF_SPACE) as $space_key=>$space_value){ ?>
					                        	<option value="<?php echo $space_key; ?>"><?php echo $space_value; ?></option>
					                        	<?php } ?>
					                        </select>
					                    </div>
					                </div>
					                 <div class="col-md-12 col-sm-12 col-lg-12">
					                    <div class="form-group">
					                        <input type="text" name="l" class="form-control controls" required="" id="pac-input" placeholder=''>
					                        <input type="hidden" name="lattitude" id="lattitude">
					                        <input type="hidden" name="longitude" id="longitude">
					                         <div id="map"></div>
					                        <label class="flot">Where are you planning it?</label>
					                    </div>
					                </div>

					                 <div class="col-md-6 col-sm-6 col-lg-6">
					                    <div class="form-group datepicker-box">
					                       <input type="text" class="span2 form-control datepicker1" name="f" autocomplete="off">	
					                        <label class="flot">Start Date</label>							 
					                    </div>
					                </div>

					                 <div class="col-md-6 col-sm-6 col-lg-6">
					                    <div class="form-group  datepicker-box">
					                       <input type="text" class="span2 form-control datepicker" data-date-format="mm/dd/yy" name="e" autocomplete="off">	
					                        <label class="flot">End Date</label>							 
					                    </div>
					                </div>

					                 <div class="col-md-12 col-sm-12 col-lg-12">
					                    <div class="form-group">
					                       <input type="number" name="n" class="form-control" >
					                        <label class="flot"># of people</label>							 
					                    </div>
					                </div>

					                <div class="col-md-6 col-sm-6 col-lg-6">
					                    <div class="form-group submit">
					                        <button type="submit" value="Send" class="Submit-form-button">Search</button>
					                    </div>
					                </div>

					            </div>
					        </form>
					    </div>
					</div>
				</div>
				<div class="col-md-7">
			        <div class="main-slider-content-inner">
			            <div class="slider-content-aera">
			              <h1>Find the perfect place to get together</h1>
			              <p>Generate Lorem Ipsum placeholder text for use in your graphic, print and web layouts, and discover plugins for your favorite writing, design and blogging tools.</p>
			              <a href="<?php echo base_url('site/step1');?>" class="button-book-main">Get Started</a>

			            </div>
			        </div>
			    </div>

     		</div>
    	</div>
    </div>
</section>


<section class="how-it-work">
	<div class="container">
		<div class="heading-aera">
			<h1>How It Works</h1>
			<p>Lorem Ipsum copy in various charsets and languages for layouts.</p>			
		</div>
		<div class="row">
			<div class="col-md-4">
			    <div class="how-it-work-box">
			        <div class="how-it-work-box-inner">			          
			            <div class="how-it-work-box-icon">
			                 <img src="<?php echo base_url('assets/images/explore-space-icon.png');?>">
			            </div> 
			            <h4>Explore Venues</h4> 
			            <p>Tell us what you’re looking for using the filters above. Then, search through your customized results until you find the perfect place for your event.</p> 
			        </div>
			    </div>
			</div>

			<div class="col-md-4">
			    <div class="how-it-work-box">
			        <div class="how-it-work-box-inner">			          
			            <div class="how-it-work-box-icon">
			                 <img src="<?php echo base_url('assets/images/connectwith-icon.png');?>" alt="Connect venue" />
			            </div> 
			            <h4>Connect with the venue</h4> 
			            <p>Once you find a venue that fits your requirements you can connect with the venue owner. </p> 
			        </div>
			    </div>
			</div>

			<div class="col-md-4">
			    <div class="how-it-work-box">
			        <div class="how-it-work-box-inner">			          
			            <div class="how-it-work-box-icon">
			                 <img src="<?php echo base_url('assets/images/book-icon.png');?>">
			            </div> 
			            <h4>Book & Enjoy your event</h4> 
			            <p>Once you’ve found the perfect venue simply book it through our 100% secure checkout and then get ready for the fun.</p> 
			        </div>
			    </div>
			</div>
		</div>
	</div>
</section>


<section class="av-activities-section">
	<div class="container">
		<div class="row">

			<!-- section-title - start -->
			<div class="col-lg-4 col-md-12 col-sm-12">
				<div class="activities-section-title">
					<h3>Popular Activities</h3>
					<h2>There’s an <strong>event</strong> for every space.</h2>
					<p>It’s not just big events like weddings and parties that require a great venue. So we offer a little bit of everything. Here are some activities that other people have needed some space for. </p>
					<p>And if you’re not seeing what you’re looking for, we’re open to suggestion. So if you’ve got a perfect venue for something you don’t see here just let us know and we’d be happy to add it to the list.</p>
					<a href="#!" class="custom-btn">Suggest a venue</a>
				</div>
			</div>
			<!-- section-title - end -->

			<!-- about-item-wrapper - start -->
			<div class="col-lg-8 col-md-12 col-sm-12">
				<div class="activities-item-wrapper ul-li">
					<ul>

						<li>
							<a href="#!" class="activities-item">
								<div class="activities-item-icon">
									<i class="flaticon-music-and-multimedia"></i>
								</div>
								<div class="activities-item-content">
									<h1>Music venues</h1>
								</div>
							</a>
						</li>
						
						<li>
							<a href="#!" class="activities-item">
								<div class="activities-item-icon">
									<i class="flaticon-running-man"></i>
								</div>
								<div class="activities-item-content">
									<h1>Sports Venues</h1>
								</div>
							</a>
						</li>

						<li>
							<a href="#!" class="activities-item">
								<div class="activities-item-icon">
									<i class="flaticon-lesson"></i>
								</div>
								<div class="activities-item-content">
									<h1>Lessons</h1>
								</div>
							</a>
						</li>

						<li>
							<a href="#!" class="activities-item">
								<div class="activities-item-icon">
									<i class="flaticon-public-speech"></i>
								</div>
								<div class="activities-item-content">
									<h1>Corporate Event</h1>
								</div>
							</a>
						</li>

						<li>
							<a href="#!" class="activities-item">
								<div class="activities-item-icon">
									<i class="flaticon-meeting"></i>
								</div>
								<div class="activities-item-content">
									<h1>Meeting</h1>
								</div>
							</a>
						</li>

						<li>
							<a href="#!" class="activities-item">
								<div class="activities-item-icon">
									<i class="flaticon-wedding-couple"></i>										
								</div>
								<div class="activities-item-content">
									<h1>Wedding</h1>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- about-item-wrapper - end -->
			
		</div>
	</div>
</section>



<section class="av-hotdeals-section">
	<div class="container">
		<div class="heading-aera">
			<h1>Discover Hot Deals</h1>
			<p>Lorem Ipsum copy in various charsets and languages for layouts.</p>			
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-6">
			    <div class="hotdeals-wrap">
			        <div class="hotdeals-wrap-media">
			            <img src="<?php echo base_url();?>assets/images/deal1.png">
			            <div class="hotdeals-wrap-content">
			            	<div class="rating-box1 hook-reviews">
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star-o black" aria-hidden="true"></i>
							</div>   
			            </div>
			        </div>
			        <div class="hotdeals-wrap-content-body">
			            <h2>Corporate</h2>
			            <h1>Pike Place Market Studio</h1>
			            <div class="hotdeals-wrap-content-info">  
			            	<ul class="hotdeals-info-list">
		                    	<li>
		                        	<div class="hotdeals-box-widget">
		                                <div class="hotdeals-box-widget-icon">
		                                    <i class="flaticon-team" ></i>
		                                </div>
		                                <div class="hotdeals-box-widget-content">
		                                	<h3 class="hotdeals-box-widget-title">Attendees</h3> 
		                                	<p class="hotdeals-box-widget-sub-title">10</p>
		                                </div>
		                            </div>
		                        </li>
		                        <li>
		                        	<div class="hotdeals-box-widget">
		                                <div class="hotdeals-box-widget-icon">
		                                    <i class="flaticon-placeholder"></i>
		                                </div>
		                                <div class="hotdeals-box-widget-content">
		                                	<h3 class="hotdeals-box-widget-title">Location</h3> 
		                                	<p class="hotdeals-box-widget-sub-title">Downtown Seattle WA</p>
		                                </div>
		                            </div>
		                        </li>
                    		</ul>
			            </div>
			            <div class="hotdeals-wrap-content-footer">
			            	<div class="row">
			            		<div class="col-md-5">
			            			<div class="price"> 
			            				<div> 
				            				<ins> $275 </ins> 
				            				<span> PER NIGHT </span> 
			            				</div> 
			            			</div>
			            		</div>
			            		<div class="col-md-7">
			            			<div class="btn-group">
			            				<a class="btn-view" href="#">View Details</a>
			            			</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			    </div>
			</div>


			<div class="col-md-4 col-sm-6">
			    <div class="hotdeals-wrap">
			        <div class="hotdeals-wrap-media">
			            <img src="<?php echo base_url();?>assets/images/deal2.png">
			            <div class="hotdeals-wrap-content">
			            	<div class="rating-box1 hook-reviews">
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star-o black" aria-hidden="true"></i>
							</div>   
			            </div>
			        </div>
			        <div class="hotdeals-wrap-content-body">
			            <h2>Meeting</h2>
			            <h1>Large Focus Room Located </h1>
			            <div class="hotdeals-wrap-content-info">  
			            	<ul class="hotdeals-info-list">
		                    	<li>
		                        	<div class="hotdeals-box-widget">
		                                <div class="hotdeals-box-widget-icon">
		                                    <i class="flaticon-team" ></i>
		                                </div>
		                                <div class="hotdeals-box-widget-content">
		                                	<h3 class="hotdeals-box-widget-title">Attendees</h3> 
		                                	<p class="hotdeals-box-widget-sub-title">15-20</p>
		                                </div>
		                            </div>
		                        </li>
		                        <li>
		                        	<div class="hotdeals-box-widget">
		                                <div class="hotdeals-box-widget-icon">
		                                    <i class="flaticon-placeholder"></i>
		                                </div>
		                                <div class="hotdeals-box-widget-content">
		                                	<h3 class="hotdeals-box-widget-title">Location</h3> 
		                                	<p class="hotdeals-box-widget-sub-title">Downtown Seattle WA</p>
		                                </div>
		                            </div>
		                        </li>
                    		</ul>
			            </div>
			            <div class="hotdeals-wrap-content-footer">
			            	<div class="row">
			            		<div class="col-md-5">
			            			<div class="price"> 
			            				<div> 
				            				<ins> $100 </ins> 
				            				<span> PER NIGHT </span> 
			            				</div> 
			            			</div>
			            		</div>
			            		<div class="col-md-7">
			            			<div class="btn-group">
			            				<a class="btn-view" href="#">View Details</a>
			            			</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="col-md-4 col-sm-6">
			    <div class="hotdeals-wrap">
			        <div class="hotdeals-wrap-media">
			            <img src="<?php echo base_url();?>assets/images/deal3.png">
			            <div class="hotdeals-wrap-content">
			            	<div class="rating-box1 hook-reviews">
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star" aria-hidden="true"></i>
							  <i class="fa fa-star-o black" aria-hidden="true"></i>
							</div>   
			            </div>
			        </div>
			        <div class="hotdeals-wrap-content-body">
			            <h2>Corporate</h2>
			            <h1>Charming Industrial Loft Close</h1>
			            <div class="hotdeals-wrap-content-info">  
			            	<ul class="hotdeals-info-list">
		                    	<li>
		                        	<div class="hotdeals-box-widget">
		                                <div class="hotdeals-box-widget-icon">
		                                    <i class="flaticon-team" ></i>
		                                </div>
		                                <div class="hotdeals-box-widget-content">
		                                	<h3 class="hotdeals-box-widget-title">Attendees</h3> 
		                                	<p class="hotdeals-box-widget-sub-title">500</p>
		                                </div>
		                            </div>
		                        </li>
		                        <li>
		                        	<div class="hotdeals-box-widget">
		                                <div class="hotdeals-box-widget-icon">
		                                    <i class="flaticon-placeholder"></i>
		                                </div>
		                                <div class="hotdeals-box-widget-content">
		                                	<h3 class="hotdeals-box-widget-title">Location</h3> 
		                                	<p class="hotdeals-box-widget-sub-title">West Town, Chicago</p>
		                                </div>
		                            </div>
		                        </li>
                    		</ul>
			            </div>
			            <div class="hotdeals-wrap-content-footer">
			            	<div class="row">
			            		<div class="col-md-5">
			            			<div class="price"> 
			            				<div> 
				            				<ins> $150 </ins> 
				            				<span> PER NIGHT </span> 
			            				</div> 
			            			</div>
			            		</div>
			            		<div class="col-md-7">
			            			<div class="btn-group">
			            				<a class="btn-view" href="#">View Details</a>
			            			</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			    </div>
			</div>

		</div>
	</div>
</section>


<section class="av-about-section">
	<div class="container">
		<div class="av-about-content">
			<div class="row">
				<div class="col-md-4 col-sm-6 padd-5">
				    <div class="about-content-box">
				    	<h1>About Us</h1>
				    	<p>At AirVenues, we believe the more people get together, the more fun people have. And the more fun people have, the better the world is. That’s why we’ve created a platform where there are more places for people to get together to do more stuff. Simple as that. So whether you’re looking for a unique place to hold a pool party, a wedding, woodworking classes or even some band practice, AirVenues can help you find that space.</p>
				    </div> 
				</div>
				<div class="col-md-4 col-sm-6 padd-5">
				    <div class="about-content-box bg-about">
				    	<h1>Booking venues With AirVenues</h1>
				    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.scrambled it to make a type specimen book.</p>
				    </div> 
				</div>
				<div class="col-md-4 col-sm-6 padd-5">
				    <div class="about-content-box">
				    	<h1>Domestic space with AirVenues</h1>
				    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.scrambled it to make a type specimen book.</p>
				    </div> 
				</div>
			</div>
		</div>
	</div>
</section>




<section id="special-offer-section" class="special-offer-section clearfix" style="background-image: url(<?php echo base_url('assets/images/list-space-bg.jpg');?>);">
	<div class="container">
		<div class="row">

			<!-- special-offer-content - start -->
			<div class="col-lg-8 col-md-12 col-sm-12">
				<div class="special-offer-content">
					<h2>List your event space and start earning some extra money.</h2>
					<p>Any space can be an event space. So if you've got a backyard with a view, a warehouse, a garage full of tools or any other space that lends itself to fun register it here and share it with people looking for a unique place to throw their next party, wedding, conference, or anything really.</p>
				</div>
			</div>
			<!-- special-offer-content - end -->

			<!-- event-makeing-btn - start -->
			<div class="col-lg-4 col-md-12 col-sm-12">
				<div class="event-makeing-btn">
					<a href="<?php echo base_url('site/register');?>">Register your venue</a>
					<a href="javascript:">Contact us</a>
				</div>
			</div>
			<!-- event-makeing-btn - end -->

		</div>
	</div>
</section>

<section class="av-sponsors-section">
	<div class="container">
		<div class="heading-aera">
			<h1>Sponsors</h1>
			<p>We know a lot goes into planning the perfect event. So we’ve partnered<br> with these great businesses to make planning your next event a little bit easier.</p>			
		</div>
		<div class="av-sponsors-content">
			<div id="sponsors-owl-carousel" class="owl-carousel owl-theme">
    			<div class="item">
    				<div class="sponsor-logo-content">
    					<a href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/sp-logo1.png"></a>
    				</div>
    			</div>
    			<div class="item">
    				<div class="sponsor-logo-content">
    					<a href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/sp-logo2.png"></a>
    				</div>
    			</div>
    			<div class="item">
    				<div class="sponsor-logo-content">
    					<a href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/sp-logo3.png"></a>
    				</div>
    			</div>
    			<div class="item">
    				<div class="sponsor-logo-content">
    					<a href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/sp-logo4.png"></a>
    				</div>
    			</div>
    			<div class="item">
    				<div class="sponsor-logo-content">
    					<a href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/sp-logo1.png"></a>
    				</div>
    			</div>
    			
    		</div>	
		</div>
	</div>
</section>
