<?php 
$location = '';
$planing = '';
	if(isset($_GET['l']) && $_GET['l']  ){
		$location = $_GET['l'];
	}
	
$type_of_space = json_decode(TYPE_OF_SPACE);
$arr_space_type = array();
if(isset($_GET['space'])){
	$arr_space_type = $_GET['space'];
}

$emenity = json_decode(EMENITIES);
$arr_em = array();
if(isset($_GET['em'])){
	$arr_em = $_GET['em'];
}
$map_data = json_encode($data);
?>
<section class="list-venue-content mainexamscreen">	
	<div class="list-venue-sidebar">
		<div class="list-venue-left-box">
			<div class="list-venue-box">
				<form name="filter_form" id="filter_form">
				<div class="list-venue-header-overlay ">
					<div class="search-filters-box">
						<div class="row">
							<div class="col-sm-6 search-filters-group nopadding">
								<div class="form-group">
									<select class="planning form-control" name="p">
			                        	<option value=""></option>
			                        	<?php foreach($type_of_space as $space_key=>$space_value){ ?>
			                        	<option value="<?php echo $space_key; ?>" <?php if(isset($_GET['p']) && $_GET['p']==$space_key){ echo "selected"; } ?> ><?php echo $space_value; ?></option>
			                        	<?php } ?>
			                        </select>
								</div>
							</div>
							<div class="col-sm-6 search-filters-group nopadding">
								<div class="form-group">
									<input type="text"  class="form-control" id="pac-input" placeholder="Where are you planning?" autocomplete="off" value="<?php echo $location; ?>" name="l" />
									<input type="hidden" name="lattitude" id="lattitude" value="<?php if(isset($_GET['lattitude']) && !empty($_GET['lattitude'])){ echo $_GET['lattitude'];} ?>">
									<input type="hidden" name="longitude" id="longitude" value="<?php if(isset($_GET['longitude']) && !empty($_GET['longitude'])){ echo $_GET['longitude'];} ?>" >
									 <div id="map1"></div>
									 <input type="hidden" name="s" value="<?php if(isset($_GET['s']) && !empty($_GET['s'])){ echo $_GET['s'];} ?>" >
									 <input type="hidden" name="e" value="<?php if(isset($_GET['e']) && !empty($_GET['e'])){ echo $_GET['e'];} ?>" >
								</div>
							</div>					
						</div>
					</div>
				</div>
				<section class="search-filters">
					<div class="filter-buttons">
						<ul class="filter-buttons-list">
							<li class="filter-tab"><a href="#">Space Type</a>
								<div class="filter-wrp-dropdown">
								    <div class="filter-wrp-dropdown-inner">
								    	<span class="triangle" style="left: 39px;"></span>
								        <h2>Space Type</h2>
								        <div class="filter-field-container filter-field-list">
								            <ul class="filter-field-list space-type">
								            	<?php if($space_lists){ foreach($space_lists as $space_list){ ?>
								                <li>
								                    <label>
								                        <input type="checkbox" value="<?php echo $space_list->id; ?>" name="space[]" <?php if(in_array($space_list->id, $arr_space_type)){ echo "checked";} ?>>
								                        <div class="ar-field-list-option"><?php echo $space_list->space_title; ?></div>
								                    </label>
								                </li>
								            	<?php }} ?>
								        	</ul>
								    	</div>
								    </div>

								    <div class="fiiter-footer-action-controls">
								    	<div class="row">
								    		<div class="col-md-6">
								    			<div class="text-left">
												    <button type="button" value="Reset" class="reset-btn space-type-clear">Clear</button>
												</div>
								    		</div>
								    		<div class="col-md-6">
								    			<div class="text-right">
												    <button type="button" class="Apply-btn space-type-apply">Apply</button>
												</div>
								    		</div>
								    	</div>
                					</div>
								</div>
							</li>


							<li class="filter-tab"><a href="#">Guests</a>

								<div class="wrp-dropdown-width filter-wrp-dropdown ">
									<span class="triangle" style="left: 30%;"></span>

									 <div class="filter-wrp-dropdown-inner no_of_guest">
								        <h2>Number of guests</h2>
										<div class="input-group plus-minus-input">
										  <div class="input-group-button">
										    <button type="button" class="button hollow circle" data-quantity="minus" data-field="n">
										      <i class="fa fa-minus" aria-hidden="true"></i>
										    </button>
										  </div>
										  <input class="input-group-field" type="number" name="n" value="<?php if(isset($_GET['n']) && $_GET['n']){ echo $_GET['n'];} ?>" >
										  <div class="input-group-button">
										    <button type="button" class="button hollow circle" data-quantity="plus" data-field="n">
										      <i class="fa fa-plus" aria-hidden="true"></i>
										    </button>
										  </div>
										</div>
									</div>
									 <div class="fiiter-footer-action-controls">
								    	<div class="row">
								    		<div class="col-md-6">
								    			<div class="text-left">
												    <button type="button" value="Reset" class="reset-btn no_of_guest_reset">Clear</button>
												</div>
								    		</div>
								    		<div class="col-md-6">
								    			<div class="text-right">
												    <button type="button" class="Apply-btn no_of_guest_apply">Apply</button>
												</div>
								    		</div>
								    	</div>
                					</div>
								</div>

							</li>


							<li class="filter-tab"><a href="#">Size</a>

								<div class="wrp-dropdown-width filter-wrp-dropdown ">
									<span class="triangle" style="left: 42%;"></span>

									 <div class="filter-wrp-dropdown-inner">
								        <h2>Size in sq. feet</h2>
										<div class="input-group plus-minus-input size-sqr">
										  <div class="input-group-button">
										    <button type="button" class="button hollow circle" data-quantity="minus" data-field="sqr">
										      <i class="fa fa-minus" aria-hidden="true"></i>
										    </button>
										  </div>
										  <input class="input-group-field" type="number" name="sqr" value="<?php if(isset($_GET['sqr']) && !empty($_GET['sqr'])){ echo $_GET['sqr'];} ?>">
										  <div class="input-group-button">
										    <button type="button" class="button hollow circle" data-quantity="plus" data-field="sqr">
										      <i class="fa fa-plus" aria-hidden="true"></i>
										    </button>
										  </div>
										</div>
									</div>
									 <div class="fiiter-footer-action-controls">
								    	<div class="row">
								    		<div class="col-md-6">
								    			<div class="text-left">
												    <button type="button" value="Reset" class="reset-btn size-sqr-clear">Clear</button>
												</div>
								    		</div>
								    		<div class="col-md-6">
								    			<div class="text-right">
												    <button type="button" class="Apply-btn size-sqr-apply">Apply</button>
												</div>
								    		</div>
								    	</div>
                					</div>
								</div>

							</li>


							<li class="filter-tab"><a href="#">Price</a>
								<div class="wrp-dropdown-width filter-wrp-dropdown ">
									<span class="triangle" style="left: 55%;"></span>

									 <div class="filter-wrp-dropdown-inner price-inner">
								        <h2>Price</h2>
										<div class="filter-wrp-price">
											<div id="ranged-value"></div>
										</div>
										<input type="hidden" name="low" value="0" id="low-price"> 
										<input type="hidden" name="high" value="0" id="high-price"> 
									</div>
									 <div class="fiiter-footer-action-controls">
								    	<div class="row">
								    		<div class="col-md-6">
								    			<div class="text-left">
												    <button type="button" value="Reset" class="reset-btn price-clear">Clear</button>
												</div>
								    		</div>
								    		<div class="col-md-6">
								    			<div class="text-right">
												    <button type="button" class="Apply-btn price-apply">Apply</button>
												</div>
								    		</div>
								    	</div>
                					</div>
								</div>

							</li>

							<li class="filter-tab"><a href="#">More filters</a>

								<div class="wrp-dropdown-full-width filter-wrp-dropdown ">
										<span class="triangle" style="left: 38%;"></span>

									 <div class="filter-wrp-dropdown-full-inner">
									 	
										<div class="filter-wrp-dropdown-full-item amenities-inner">
								        <h2>Amenities</h2>
											
											<ul class="amens-checkboxe-list" id="amt_lists">
												<?php if($emenity){ foreach($emenity as $em_key=>$em_value){ ?>
												<li>
													<div class="checkbox-box-amenities">
														<input type="checkbox" name="em[]" value="<?php echo $em_key; ?>" <?php if(isset($_GET['em']) && in_array($em_key, $_GET['em'])){ echo "checked"; } ?>>
														<label for="<?php echo $em_key; ?>"></label>
														<?php echo $em_value; ?>														
													</div>
												</li>
												<?php } } ?>												
											</ul>
										</div>
									</div>
									 <div class="fiiter-footer-action-controls">
								    	<div class="row">
								    		<div class="col-md-6">
								    			<div class="text-left">
												    <button type="button" value="Reset" class="reset-btn amenities-clear">Clear</button>
												</div>
								    		</div>
								    		<div class="col-md-6">
								    			<div class="text-right">
												    <button type="button" class="Apply-btn amenities-apply">Apply</button>
												</div>
								    		</div>
								    	</div>
                					</div>
								</div>

							</li>
							
						</ul>
					</div>
				</section>	
				</form>
				<div class="list-venue-body">
                	<div class="list-venue-body-inner">
                		<div class="row">
                			<?php if($data){ foreach($data as $row){ ?>
							<div class="col-md-6 col-sm-6">
							    <div class="hotdeals-wrap">
							        <div class="hotdeals-wrap-media">
							            <img src="<?php echo $row->image; ?>">
							            <div class="hotdeals-wrap-content">
							            	<div class="rating-box1 hook-reviews">
											  <i class="fa fa-star" aria-hidden="true"></i>
											  <i class="fa fa-star" aria-hidden="true"></i>
											  <i class="fa fa-star" aria-hidden="true"></i>
											  <i class="fa fa-star" aria-hidden="true"></i>
											  <i class="fa fa-star-o black" aria-hidden="true"></i>
											</div>   
							            </div>
							        </div>
							        <div class="hotdeals-wrap-content-body">
							            <h2><?php echo $row->plan_title; ?></h2>
							            <h1><?php echo $row->space_title; ?></h1>
							            <div class="hotdeals-wrap-content-info">  
							            	<ul class="hotdeals-info-list">
						                    	<li>
						                        	<div class="hotdeals-box-widget">
						                                <div class="hotdeals-box-widget-icon">
						                                    <i class="flaticon-team" ></i>
						                                </div>
						                                <div class="hotdeals-box-widget-content">
						                                	<h3 class="hotdeals-box-widget-title">Attendees</h3> 
						                                	<p class="hotdeals-box-widget-sub-title"><?php echo $row->capacity; ?></p>
						                                </div>
						                            </div>
						                        </li>
						                        <li>
						                        	<div class="hotdeals-box-widget">
						                                <div class="hotdeals-box-widget-icon">
						                                    <i class="flaticon-placeholder"></i>
						                                </div>
						                                <div class="hotdeals-box-widget-content">
						                                	<h3 class="hotdeals-box-widget-title">Location</h3> 
						                                	<p class="hotdeals-box-widget-sub-title"><?php echo $row->location; ?></p>
						                                </div>
						                            </div>
						                        </li>
				                    		</ul>
							            </div>
							            <div class="hotdeals-wrap-content-footer">
							            	<div class="row">
							            		<div class="col-md-5">
							            			<div class="price"> 
							            				<div> 
								            				<ins> $<?php echo $row->hourly_rate; ?> </ins> 
								            				<span> PER HOUR	 </span> 
							            				</div> 
							            			</div>
							            		</div>
							            		<div class="col-md-7">
							            			<div class="btn-group">
							            				<a class="btn-view" href="#">View Details</a>
							            			</div>
							            		</div>
							            	</div>
							            </div>
							        </div>
							    </div>
							</div>
							<?php }}else{ ?>
								<div class="col-md-12 col-sm-12">
									<h2>Sorry, record couldn't match that search.</h2>
								</div>
							<?php } ?>
						</div>
                	</div>    
				</div>
				
				<div class="list-venue-qp-action-controls"></div>
			</div>
		</div>
		<div id="sidebar" class="list-venue-right-box">
			<div class="list-venue-right-box-body">
				<div id="map-canvas" class="map"> </div><!-- map -->
			</div>
		</div>
	</div>
</section>

<?php
$center_lat = (isset($post_data['lattitude']) ? $post_data['lattitude'] : '39.724049' );
$center_lon = (isset($post_data['longitude']) ? $post_data['longitude'] : '-105.001703' );
?>

<link rel="stylesheet" href="<?php echo base_url('assets/css/select2.min.css'); ?>"> <!-- select2 css -->
<script src="<?php echo base_url('assets/js/select2.min.js'); ?>"></script>

<!-- map css start -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/map/') ?>style.css" media="screen" type="text/css" /><link rel="stylesheet" href="<?php echo base_url('assets/css/map/') ?>_legacy/spritesheet.css" media="screen" type="text/css" />

<script src="<?php echo base_url('assets/js/map/') ?>jquery.cycle2.min.js" type="text/javascript"></script>

<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAp0lAQ9iG91OpsDDDbkACSbmShbV0aKqM&libraries=places"
    ></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/map/') ?>richmarker.js"></script>
<script>
	var current_url = window.location.href;
	var pattern = /[?]/;
	$(document).ready(function() {
		
	    $('.planning').select2({
			placeholder: 'What are you planning?',
		});

		$('.planning').on('change',function(){
			$('#filter_form').submit();
		});
		
	});
		

		var autocomplete = new google.maps.places.Autocomplete($("#pac-input")[0], {});

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var places = autocomplete.getPlace();
            console.log(places);
            $('#lattitude').val(places.geometry.location.lat());
	        $('#longitude').val(places.geometry.location.lng());
	        $('#filter_form').submit();
        });

	      $('.space-type-clear').on('click',function(){
	      	$(".space-type input").prop('checked',false);
	      });

	      $(".space-type-apply, .no_of_guest_apply, .size-sqr-apply, .price-apply, .amenities-apply").on('click',function(){
	      	$('#filter_form').submit();
	      });

	      $(".no_of_guest_reset").on('click',function(){
	      	$('.no_of_guest input').val(0);
	      });

	      $(".size-sqr-clear").on('click',function(){
	      	$('.size-sqr input').val(0);
	      });

	      $(".price-clear").on('click',function(){
	      	$('.price-inner input').val(0);
	      });

	      $('.amenities-clear').on('click',function(){
	      	$(".amenities-inner input").prop('checked',false);
	      });
</script>

<script type="text/javascript">
	var map_data = JSON.parse(JSON.stringify(<?php echo $map_data; ?>));
	
	if(map_data!=''){
		console.log(map_data);
		function initialize1() {
		var mapOptions = {
			center: new google.maps.LatLng(<?php echo $center_lat;?>, <?php echo $center_lon;?>),
			zoom: 5,
			scrollwheel: false,
			mapTypeControl: false,
			streetViewControl: false
		};

		// Create map
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var k = 0;
		 var accounts = [];
		$.each(map_data, function(key,v){ k++;
			
			var images = '<div class="image"><img src="'+v.image+'" alt="" class="img-fluid" /></div>';				var marker_html = '<div class="pin public marker_'+k+'" data-lat="'+v.lattitude+'" data-lng="'+v.longitude+'" data-categories="public">' +
				'<div class="wrapper">' +
					'<div class="small">' +
						'<img src="<?php echo base_url('assets/images/favicon.png');?>" alt="" width="32" />' +
					'</div>' +
					'<div class="large">' +
						images +
						'<div class="text">' +
							'<p class="artist monteserrat"><strong>'+v.space_title+'</strong></p>' +
							'<p class="description">'+v.location+'</p>' +
						'</div>' +
						'<a class="icn close" href="#" title="Close">Close</a>' + 
					'</div>' +
				'</div>' +
				'<span></span>' +
				'</div>';
				var marker_1 = new RichMarker({
					position: new google.maps.LatLng(v.lattitude, v.longitude),
					draggable: false,
					flat: true,
					anchor: RichMarkerPosition.BOTTOM,
					content: marker_html
				});			
				marker_1.setMap(map);
				
				setCustomMarker(k,marker_1);
				

			
		});
		
		
		function setCustomMarker(k,marker_1){
			google.maps.event.addListener(marker_1, 'click', function() {
					
					var modifier = 0.0178;
					if ($(window).width() < 768) {
						modifier = 0;
					}
					
					if (!$('.marker_'+k).hasClass('active')) {
						// map.setZoom(14);
						// var temp_lat = -31.955945 + modifier;
						// map.panTo(new google.maps.LatLng(temp_lat, 115.856339));
					}
					
					$('.pin').removeClass('active').css('z-index', 10);
					$('.marker_'+k).addClass('active').css('z-index', 200);
					console.log('.marker_'+k)
					$('.marker_'+k+' .large .image').cycle({
						fx: 'scrollHorz',
						slides: '> img',
						prev: '> .left-black-arrow',
						next: '> .right-black-arrow'
					});
				
					$('.marker_'+k+' .large .close').click(function(){
						$('.pin').removeClass('active');
						return false;
					});
					
				});
		}					
		positionMapButton();
		resizeMapForSmallRes();
	
		$(window).resize(function(){
			
			if ($('.pin.active').length > 0) {
				var latitude = $('.pin.active:first').attr('data-lat');
				var longitude = $('.pin.active:first').attr('data-lng');
				var modifier = 0.0178;
				if ($(window).width() < 768) {
					modifier = 0;
				}
				latitude = parseFloat(latitude) + modifier;
			}
			else {
				var latitude = $('.pin:first').attr('data-lat');
				var longitude = $('.pin:first').attr('data-lng');
			}
			map.setZoom(15);
			map.panTo(new google.maps.LatLng(latitude, longitude));
			
			positionMapButton();
			resizeMapForSmallRes();
			
		});
		
		if ($('.map-nav').length > 0) {
			$('.map-nav ul a').click(function(){
				$('.map-nav ul a').removeClass('active');
				$(this).addClass('active');
				$('.pin').hide();
				$('.map-nav ul a.active').each(function(){
					var filter = $(this).attr('data-filter');
					$('.pin[data-categories*="' + filter + '"]').show();
				});
				return false;
			});
		}
		
					
	}
	
	function resizeMapForSmallRes() {
	
		if ($(window).width() < 768) {
			var map_height = $(window).height() - 80;
			if (map_height > 749) { map_height = 749; }
			$('#map-canvas').css({height: map_height});
		}
		
		var filter_height = 0;
		$('.map-nav ul a').each(function(){
			if ($(this).height() > filter_height) {
				filter_height = $(this).height();
			}
		});
		$('.map-nav ul a').css({height: filter_height});
		
	}
	
	function positionMapButton() {
		if ($('.map-btn').length > 0) {
			var left_pos = ($(window).width() - $('.map-btn').width()) / 2;
			$('.map-btn').css({'left': left_pos});
		}
	}
	
	google.maps.event.addDomListener(window, 'load', initialize1);
}	
</script>
<style type="text/css">
	.select2-container--default .select2-selection--single {
	    border:unset!important;
	    
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	    display: none;
	}
	.select2-container--default .select2-selection--single .select2-selection__placeholder {
	    color: #495057;
	    padding: 15px 15px;
	    font-size: 1rem;
	}
</style>