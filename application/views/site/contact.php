<div class="user-infor-section">
<div class="user-infor-login">
    <div class="container">
        <div class="we-main-login">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="we-login-content-box">
                        <div class="we-heading">
							<h3>Contact Us</h3>
                            <p>Please send your request.</p>
                        </div>

						<?php if($this->session->flashdata('erreor_login')){?>
						<div class="alert alert-danger"><?= $this->session->flashdata('erreor_login');?> </div>
						<?php }?>
                        <div class="we-form-box">
                            <form action="#" method="post" autocomplete="off">
                            	
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" required />
                                    <label class="flot">Email Address</label>
								<?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" required />
                                    <label class="flot">Name</label>
									<?php echo form_error('name', '<div class="error">', '</div>'); ?>
                                
                                </div>

                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control" required />
                                    <label class="flot">Phone</label>
									<?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                                
                                </div>
                                <div class="form-group">
                                    <textarea  name="message" class="form-control" required ></textarea>
                                    <label class="flot">Message</label>
									<?php echo form_error('message', '<div class="error">', '</div>'); ?>
                                
                                </div>
                                <div class="form-group submit">
                                    <button type="submit" value="Send" class="Submit-form-button">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-8 col-lg-8 col-sm-8">
                    <div class="we-login-content-section">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="_161wlni"></div>

</div>
