<div class="user-infor-section">
<div class="user-infor-login">
    <div class="container">
        <div class="we-main-login">
            <div class="row">
                <div class="col-md-7 col-lg-7 col-sm-7">
                    <div class="we-login-content-section">
                        <div class="we-login-content-top">
							<h1>Offer your place as an event space now</h1>
                            <p>AirVenues is a unique way to earn extra income. Offer yourbackyard, swimming pool, office, shop, showroom or anywhere else as an event space where people can get together for parties, meetings, weddings…any kinds of events. Get paid for hosting events and you’ll find that new car, once in a life time family trip, kid’s college tuition or luxury treat become so much more affordable.</p>
                        </div>
                        <div class="Marketplace-section">
                         <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="Marketplace-step-box">
                                    <h4>Maximize your space</h4>
                                    <p>Open your doors to thousands of organizers looking for creative venues</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="Marketplace-step-box">
                                    <h4>Inspire creativity</h4>
                                    <p>Join a community of curated spaces and help create meaningful experiences</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4">
                                <div class="Marketplace-step-box">
                                    <h4>Make money</h4>
                                    <p>Create additional income by booking your residential or commercial space</p>
                                </div>
                            </div>
                        </div>
                        
                        <ul class="Marketplace-page-list ">
                            <h5 style="color: white;">Register your space now and join the AirVenues community. </h5>
                            <h5 style="color: white;">We’re here to help the world get together. Anywhere.</h5>
                            <li><span><img src="<?php echo base_url('assets/images/check.png');?>"></span>Free listing</li>
                            <li><span><img src="<?php echo base_url('assets/images/check.png');?>"></span>Secure payments</li>
                            <li><span><img src="<?php echo base_url('assets/images/check.png');?>"></span>Expert support & guidance</li>
                            <li><span><img src="<?php echo base_url('assets/images/check.png');?>"></span>Liability coverage of up to $1,000,000 per event</li>
                        </ul>
                        </div>
                    </div>


                </div>
                <div class="col-md-5 col-lg-5 col-sm-5">
                    <div class="we-login-content-box">
                        <div class="we-heading">
							<h3>Welcome Back</h3>
                            <p>Please log in to continue.</p>
                        </div>
						<?php //$this->load->view('includes/error');?>
                        <div class="social-login-box">
							<a class="facebook-btn" href="<?php echo base_url('hauth/login/Facebook');?>">
							<span><img src="<?php echo base_url('assets/images/fb.png');?>"></span> Connect with Facebook
							</a>
							<a class="google-btn" href="<?php echo base_url('hauth/login/Google');?>">
							<span><img src="<?php echo base_url('assets/images/google.png');?>"></span> Connect with google
							</a>
                        </div>

                        <div class="or">
                        	<div class="panel__divider">OR</div>
                        </div>
						<?php if($this->session->flashdata('erreor_login')){?>
						<div class="alert alert-danger"><?= $this->session->flashdata('erreor_login');?> </div>
						<?php }?>
                        <div class="we-form-box">
						
                            <form action="<?= base_url('site/login');?>" method="post">
                            	
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" required />
                                    <label class="flot">Email Address</label>
								<?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" required />
                                    <label class="flot">Password</label>
									<?php echo form_error('password', '<div class="error">', '</div>'); ?>
                                
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" value="Send" class="Submit-form-button">Sign In</button>
                                </div>
                                <div class="form-group already_account">
                                    <div class="login-form-link1">
                                        <a href="<?php echo base_url('forgot_password');?>">Forgot password?</a>
                                    </div>
                                    <div class="login-form-link1">
                                        Don't have an account? <a href="<?php echo base_url('register');?>" class="login-btn">Sign Up</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="_161wlni"></div>
<div class="av-space-box-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="space-box-info">
                    <h2>Why join AirVenues?</h2>
                    <p>If you have a space where people can get together, then you have a way to make more money through AirVenues. Unique event spaces are in demand and every time yours is booked, you’ll get paid.</p>
                </div>
            </div>
             <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="space-box-info">
                    <h2>You’re in control</h2>
                    <p>AirVenues ensure you have full control of your space’s availability, prices and rules. You can select availability dates and times, hourly rates and the kind of events your space is used for.</p>
                </div>
            </div>
             <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="space-box-info">
                    <h2>We’re here to help</h2>
                    <p>Ourhelpful guides, tips and expert support, are at your disposal 24/7 to help you turn your space into an amazing AirVenue that delivers magical moments and creates lasting memories for your guests.</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
