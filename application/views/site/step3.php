<?php 
	$session_data3 = $this->session->userdata('step3');
	$amenities = explode('/',$session_data3['amenities']);
	
?>

<section class="slide-content mainexamscreen">
	<div class="header-overlay ">
		<label class="listing-label">STEP 3: PRICING & ACCESSIBILITY</label>
	</div>
	<div class="u-mob-sidebar">
		<div class="tp-left-box">
			<div class="tp-q-box">
				<div class="tu-q-body">
                    <div class="tu-q-body-inner">
						<div class="ar-field">
							<div class="step-infor-box-content">
								<form action="<?php echo base_url('step3');?>" method="post" id="step3">
									<div class="we-form-box">
										<div class="row">
											<div class="col-md-6">
												<p>Price</p>
												<div class="form-group">
													<div class="pricing_area">
														<div class="pricing_area-icon"><span class="icon">$</span></div>
														<input type="number" class="form-control pricing_area-input" value="<?php if($session_data3){ echo $session_data3['hourly_rate']; }?>" name="hourly_rate" required>
														<!-- <label>Price *</label> -->
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<p>Price Based on</p>
												<div class="form-group">
													<select name="price_type" id="price_type" required class="form-control">
														<option>Hourly</option>
														<option>Weekly</option>
														<option>Monthly</option>
														<option>Yearly</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<h2>Capacity</h2>
											</div>
											<div class="col-md-6">
												<p>Maximum number of guests</p>
												<div class="form-group">
													<div class="pricing_area">
														<div class="pricing_area-icon"><span class="icon"><i class="fa fa-user" aria-hidden="true"></i></span></div>
														<input type="number" class="form-control pricing_area-input" value="<?php if($session_data3){ echo $session_data3['capacity']; }?>" name="guests" required="">
														<!-- <label>guests</label> -->
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<p>Minimum number of hours</p>
													<div class="pricing_area">
														<div class="pricing_area-icon"><span class="icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span></div>
														<input type="number" class="form-control pricing_area-input" value="<?php if($session_data3){ echo $session_data3['hours']; }?>" name="hours">
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<p>Maximum space</p>
													<div class="sqft_area">
														<input type="number" value="<?php if($session_data3){ echo $session_data3['space_area']; }?>" class="form-control sqft_input" name="sqft" required>
														<!-- <label>Available space *</label> -->
														<div class="sqft_icon"><span class="icon">Sq Meters</span></div>
													</div>
												</div>	
											</div>	
											<div class="col-md-6">
													<p>Space Based on</p>
												<div class="form-group">
													<select name="space_type" id="space_type" required class="form-control">
														<option>Square Feet</option>
														<option>Square Meter</option>
														<option>Square Yards</option>
													</select>
												</div>
											</div>
											
										</div>
										
										<div class="row">
											<div class="col-md-12 col-lg-12 col-sm-12">
												<div class="step-infor-box-content">
												
													<h2>Off-Site Amenities</h2>
													<p><b>At least one amenity must be selected</b></p>
													<div class="clearfix"></div>
													<?php
													$amt_ar = array(
														'WiFi','Tables','Chairs','Whiteboard','Chalkboard','Projector','Screen','Flip Charts','Monitor','Conference Phone','Public Transportation','Restrooms','Wheelchair Accessible','Breakout Space','Kitchen','Rooftop','Outdoor Area','Dressing Room','Parking Space(s)'
													);
													?>
													<ul class="amens-checkboxe-list" id="amt_lists">
														<?php foreach($amt_ar as $amt){?>
														<li>
															<div class="checkbox-box-amenities">
																<input type="checkbox" name="amenities[]" value="<?php echo $amt;?>" <?php if(in_array($amt,$amenities)){echo 'checked'; }?>>
																<label for="<?php echo $amt;?>"></label>
																<?php echo $amt;?>
															</div>
														</li>
														<?php }?>
													</ul>	
													<div class="step-infor-form-box we-form-box">
														<p><b>Add your own amenities</b></p>
														<div class="form-group">
															<input type="text" class="form-control" name="own_amenities" id="own_amt" />
														</div>
														<div class="form-group">
															<input type="button" class="btn-button" onclick="add_own_amt()" value="+ Add" />
														</div>

													</div>
												</div>
											</div>
										</div>
										
									</div>
								</form>
							</div>
						</div>
					</div>
               
				</div>
				
				 <div class="tu-qp-action-controls">
					<div class="step-btn-box">
						<a class="btn back-btn" href="<?= base_url('step2');?>">Back</a>
						<button type="button" class="btn next-btn" onclick="validate_step3()">Save & Continue</button>
					</div>
					
                </div>
			</div>
		</div>
		<div id="sidebar" class="tp-right-box">
			<div class="tu-question-List">
				<div class="listing-help">
					<div class="listing-help-image">
						<img src="<?php echo base_url();?>/assets/images/step-3.png">
					</div>
					<div class="listing-help-text">
						<p><strong>Accessibility is important</strong> for events that require loading in equipment, furniture, and props. Including these details helps event organizers come prepared.</p>
						<p><strong>Pricing:</strong> Rate per hour</p>
						<p><strong>Discount:</strong> Set discount price if booking hours is greater than 8.</p>
						<p><strong>Capacity:</strong> Maximum no. of guests and minimun no. of hours.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	function add_own_amt(){
		var custom_amt = $('#own_amt').val();
		if(custom_amt!=''){
			$('#amt_lists').append(
				$('<li>').append(
				'<div class="checkbox-box-amenities"><input type="checkbox" name="amenities[]" value="'+custom_amt+'"><label for="'+custom_amt+'"></label>'+custom_amt+'</div>'
				)); 
			$('#own_amt').val('');
		}else{
			alert('Please enter own amenities first.');
			$('#own_amt').focus();
		}
	}
	function validate_step3(){
		$( "#step3" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "checkbox" ) {
					//error.insertAfter( element.parent( "em" ) );
					error.insertAfter( '#week_days_error' );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});
		
		if ($('#step3').valid()){
			$('#step3').submit()
		}
	}

</script>
<style type="text/css">
	select.form-control:not([size]):not([multiple]) {
	    height: inherit!important;
	}
</style>
