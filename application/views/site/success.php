<div class="step-infor-section">
    <div class="container">
        <div class="step-success-box">
        	<div class="step-success-box-inner">
			    <h2><i class="fa fa-check-circle-o" aria-hidden="true"></i> Venue Submitted</h2>
				<p>Thank You for listing your venue. <br />It will be live once approved by site administrator.</p>
			</div>
			</div>
		</div>
	</div>
</div>