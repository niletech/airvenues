


<section class="event-details-section">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-8 col-lg-8">
				<section class="event-media-section">
					<div id="owl-event-media" class="owl-carousel owl-theme">
					    <div class="item">
					    	<div class="event-media-content">
					    		<img src="<?php echo base_url();?>assets/images/deal1.png">
					    	</div>
					    </div>

					    <div class="item">
					    	<div class="event-media-content">
					    		<img src="<?php echo base_url();?>assets/images/deal3.png">
					    	</div>
					    </div>

					    <div class="item">
					    	<div class="event-media-content">
					    		<img src="<?php echo base_url();?>assets/images/deal2.png">
					    	</div>
					    </div>					   
					</div>
				</section>

				<section class="event-content-section">
					<div class="event-content-tab-box">
						<div class="link-tabs">
							<a href="#overview">Overview</a>
							<a href="#amenities">Amenities</a>
							<a href="#review">Review</a>
							<a href="#map">Map</a>
							<a href="#rate">Rate & Availability</a>
						</div>

						<div class="link-tabs-content">
							<section id="overview" class="tabs-content">
								<div class="event-content-tab-item">
									<div class="event-content-box">
										<h2>BALIAN TREEHOUSE w beautiful pool</h2>
										<h4>Malibu, California, United States</h4>
									</div>

									<div class="event-hosting-by-section">
									<div class="event-hosting-user-media">
										<img src="<?php echo base_url();?>assets/images/user-profile.jpg" ">
									</div>
									<div class="event-hosting-user-content">
										<h2>Hosted by Bibi</h2>
										<p>Malibu, California, United States· Joined in January 2013</p>										
										<div class="hosting-link-list">
											<ul>
												<li>
													<i class="fa fa-star" aria-hidden="true"></i><span>1691 Reviews</span>
												</li>
												<li>
													<i class="fa fa-star" aria-hidden="true"></i><span>1 Reference</span>
												</li>
												<li>
													<i class="fa fa-star" aria-hidden="true"></i><span>Verified</span>
												</li>
											</ul>
										</div>
										
									</div>
								</div>

									<div class="link_tabs_links">
								        <ul>
								        	<li>
								        		<div class="link-tabs-design">
								        			<img src="<?php echo base_url();?>assets/images/space-plans.svg" >
								        			<h3>Apartment</h3>
								        			<h2>1400 Sqft</h2>
								        		</div>
								        	</li>
								        	<li>
								        		<div class="link-tabs-design">
								        			<img src="<?php echo base_url();?>assets/images/family.svg">
								        			<h3>Guests</h3>
								        			<h2>400</h2>
								        		</div>
								        	</li>
								        	
								        </ul>
								    </div>
								</div>



							</section>
							<section id="amenities" class="tabs-content"></section>
							<section id="review" class="tabs-content"></section>
							<section id="map" class="tabs-content"></section>
							<section id="rate" class="tabs-content"></section>
						</div>

					</div>	
				</section>

			</div>
			<div class="col-sm-6 col-md-4 col-lg-4">
			</div>
		</div>
	</div>
</section>

