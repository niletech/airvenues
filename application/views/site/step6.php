<?php 
$session_data = $this->session->userdata('step2');
?>
<section class="slide-content mainexamscreen">
	<div class="header-overlay ">
		<label class="listing-label">STEP 6: SPACE PHOTOS</label>
	</div>
	<div class="u-mob-sidebar">
		<div class="tp-left-box">
			<div class="tp-q-box">
				<div class="tu-q-body">
                    <div class="tu-q-body-inner">
						<div class="ar-field">
							<div class="step-infor-box-content">
								<h2>Photos can make your listing stand out</h2>
								<ul>
									<li>Add up to 10 images of your space.</li>
									<li>You must submit a minimum of 4 images for your space to be approved.</li>
									<li>Make sure they are at least 1400px width and 1024px height.</li>
								</ul>	
								<p>
									<small>Submitting photos that reveal the identity of your space, or that you don't have the rights to use, is against AirVenues terms of service.</small>
								</p>
								<div class="we-form-box">
									<h2>Please add space photos</h2>
									<form action="<?php echo base_url('site/upload_venue_image');?>" class="dropzone" id="profile"></form>						
								</div>
									
							</div>
						</div>
					</div>
               
				</div>
				<form action="<?php echo base_url('site/step6');?>" id="step6" method="post">
					<input type="hidden" name="complete" value="4" />
				</form>	
				
				 <div class="tu-qp-action-controls">
					<div class="step-btn-box">
						<a class="back-btn" href="<?= base_url('site/step5');?>">Back</a>
						<button type="button" class="btn next-btn" onclick="validate_step6()" style="pointer-events: none;">Next</button>
					</div>
					
                </div>
			</div>
		</div>
		<div id="sidebar" class="tp-right-box">
			<div class="tu-question-List">
				<div class="listing-help">
					<div class="listing-help-image">
						<img src="<?php echo base_url();?>/assets/images/step-5.png">
					</div>
					<div class="listing-help-text">
						<p><strong>1. Showcase your space</strong></p>
						<p><strong>2. Take photos when your space is well-lit.</strong></p>
                    	<p><strong>3. Don't forget the details</strong></p>
                    	<p><strong>4. Add photos of your space in action</strong></p>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<link href="<?php echo base_url('assets/css/dropzone.min.css');?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/dropzone.min.js');?>"></script>
<script type="text/javascript">
	function validate_step6(){
		$('#step6').submit();
		//window.location = '<?php echo base_url('site/complete_venue_registration/');?>';
	}


	Dropzone.options.profile = {
	  init: function() {
	  	
	    this.on("success", function(file,resp) { 
	    	var res = $.parseJSON(resp);
	    	if(res.msg=='success'){
	    		$(file.previewTemplate).children('.dz-remove').attr('image_id',res.id);
	    	}
	    }),

	    this.on("complete", function(file) {
           $(".dz-remove").text('').addClass('fa fa-trash text-danger');
           $(".next-btn").removeClass('disabled').removeAttr('style');
       	}),
       	this.on('addedfile',function(file){
       		$(".next-btn").addClass('disabled').css('pointer-events','none');
       	})
	  }
	};

	Dropzone.autoDiscover = false; 
	$(".dropzone").dropzone({
	 addRemoveLinks: true,
	 removedfile: function(file) { 
	 	var name = file.name;
	 var image_id = $(file.previewTemplate).children('.dz-remove').attr('image_id');
	  $.ajax({
		   type: 'POST',
		   url: '<?php echo base_url(); ?>site/remove_drop_box_file/'+image_id,
		   data: {name: name,'folder_name':'venue_gallery'},
		   sucess: function(data){
	    
	   	   }
	  });
	  var _ref;
	  return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
	 }
	});
	/*Dropzone.options.myDropzone = {
        maxFilesize:4,
		uploadMultiple: true,
		parallelUploads: 1,
		maxFiles: 1,
		addRemoveLinks: true,
		autoProcessQueue: false,
		init: function() {
			  var submitButton = document.querySelector("#submit-all")
				myDropzone = this; // closure
			submitButton.addEventListener("click", function() {
			  myDropzone.processQueue(); // Tell Dropzone to process all queued files.
			});
		// You might want to show the submit button only when 
			// files are dropped here:
			this.on("addedfile", function() {
			  // Show submit button here and/or inform user to click it.
			});
		},
		success: function (file, responseText){
            //location.reload();
        },
    };
	*/
	
</script>
