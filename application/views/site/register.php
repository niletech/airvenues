<div class="user-infor-section">
    <div class="container">
        <div class="we-main-login">
            <div class="row">
                <div class="col-md-7 col-lg-7 col-sm-7">
                    <div class="we-login-content-section">
                        <div class="we-login-content-top">
                            <h1>You need an account to add a space</h1>
                            <p>AirVenues is a unique way to earn extra income. Here, you may offer your backyard, swimming pool, office, shop, gourmet kitchen, showroom or anywhere else you see as an event space. Your space allows people, globally, to create lasting moments where they can gather for parties, study sessions, meetings, retreats, reunions, weddings…or any creative event they imagine.  Get paid for hosting events and you’ll find that new car, once in a lifetime family trip, kid’s college tuition or luxury treat become so much more affordable.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-lg-5 col-sm-5">
                    <div class="we-login-content-box">
                        <div class="we-heading">
                            <h3>Get Started,It's easy</h3>
                            <p>Anyone can earn more money with AirVenues - start today</p>
                        </div>

                        <div class="social-login-box">
							<a class="facebook-btn" href="<?php echo base_url('hauth/login/Facebook');?>">
							<span><img src="<?php echo base_url('assets/images/fb.png');?>"></span> Connect with Facebook
							</a>
							<a class="google-btn" href="<?php echo base_url('hauth/login/Google');?>">
							<span><img src="<?php echo base_url('assets/images/google.png');?>"></span> Connect with google
							</a>
                        </div>

                        <div class="or">
                        	<div class="panel__divider">OR</div>
                        </div>

                        <div class="we-form-box">
							<form class="form-register" id="signupForm" action="<?php echo base_url('register');?>" method="post">
								<div class="form-group">
									<input type="text" name="first_name" id="firstname" class="form-control" value="<?php echo set_value('first_name'); ?>"  required />
									<?php if(form_error('first_name')){ echo '<em id="first_name-error" class="error" for="first_name">'.form_error('first_name').'</em>'; } ?>
									<label class="flot">First Name *</label>
								</div>
								<div class="form-group">
									<input type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" required />
									<?php if(form_error('last_name')){ echo '<em id="last_name-error" class="error" for="last_name">'.form_error('last_name').'</em>'; } ?>
									<label class="flot">Last Name *</label>
								</div>
								<div class="form-group">
									<input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control" required />
									<?php if(form_error('email')){ echo '<em id="email-error" class="error" for="email">'.form_error('email').'</em>'; } ?>
									<label class="flot">Email Address *</label>
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" class="form-control" required>
									<?php if(form_error('password')){ echo '<em id="password-error" class="error" for="password">'.form_error('password').'</em>'; } ?>
									<label class="flot">Password *</label>
								</div>
								<div class="form-group">
									<input type="password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>" class="form-control" required>
									<?php if(form_error('confirm_password')){ echo '<em id="confirm_password-error" class="error" for="confirm_password">'.form_error('confirm_password').'</em>'; } ?>
									<label class="flot">Confirm Password *</label>
								</div>
								<div class="av-captcha">
									<?php echo $recaptcha;?>
									<?php if(form_error('recaptcha')){ echo '<em id="recaptcha-error" class="error" for="recaptcha">'.form_error('recaptcha').'</em>'; } ?>
								</div>
								<p class="terms-of-service">By signing up I agree to AirVenues <a href="<?php echo base_url('terms-of-service');?>" target="_blank">Terms of Service</a> & <a href="<?php echo base_url('terms-of-service');?>" target="_blank">Privacy Policy</a></p>
								<div class="form-group text-center">
									<button type="submit" value="Send" class="Submit-form-button">Sign up</button>
								</div>
								<div class="form-group already_account">
                                    <div class="login-form-link1">
                                        Already have an AirVenues account? <a href="<?php echo base_url('login');?>" class="login-btn">Log In</a>
                                    </div>
                                </div>
							</form>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
