	<section class="page_content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="card fat">
						<div class="card-body">
							<div class="login-box-inner">
							<?php
							if($user_data->num_rows()>0){
								$user = $user_data->row();
							?>
								<h4 class="text-center text-bold">Congrtulations! <br />Your AirVenues account activated successfully</h4>
								<div class="form-text">
									<p class="text-center" style="font-size:13px;">Thank you for choosing <?php echo DEFAULT_SITE_TITLE;?>.</p>
									<p class="text-center" style="font-size:13px;">Click on login and enjoy AirVenues services.</p>
									<p class="text-center"><a href="<?php echo base_url('login');?>" class="btn btn-sm back-btn">Login</a></p>
								</div>
							<?php }else{?>
								<div class="form-text text-center">
									<p class="text-danger card-title">Email Verification Failed!</p>
									<p class="text-left" style="font-size:13px;">Sorry, there was a problem confirming your account, please verify that this account has not previously been confirmed.</p>
									<ul class="text-left" style="margin-left:-22px; font-size:13px;">
										<li>Please try again, following the link you received in your email</li>
										<li>If you continue to encounter this error please contact support</li>
									</ul>
									<p class="text-center"><a href="<?php echo base_url('login');?>" class="btn btn-sm back-btn">Continue</a></p>
								</div>
							<?php }?>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</section>