<div id="x-section-1" class="x-section bg-image parallax header_bg" style="background-image: url('<?php echo base_url('assets/images/bg_slider1-min.jpg');?>');">
	<div class="container max width" style="margin: 0px auto;padding: 0px;">
		<div class="x-column x-sm x-1-1" style="padding: 0px;">
			<div class="bearsthemes-element bearsthemes-heading-element cs-ta-center" style="margin: 0px;padding: 0px;">
				<h2 class="page_heading text-center">About Us</h2>
			</div>
			<div class="bearsthemes-element bearsthemes-breadcrumbs-element text-center">
				<div class="breadcrumbs">
					<span class="first-item">
					<a href="<?php echo base_url();?>">Home</a></span>
					<span class="separator"><span class="fa fa-angle-right"></span></span>
					<span class="last-item">About Us</span>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="about-content">
	<div class="container">
		<div class="row">
			<div class="col-md-8 text-content">
				<h2>Our Story</h2>
				<p>Just imagine if any garden, building, room or house could be hired for parties, weddings, meetings, classes and get togethers of all kinds…it would be pretty cool right? Well,now they can. AirVenues is here to help the world get together, anywhere, in venues with meaning - where real life happens.From your neighbors’ backyard to the office at your work, the local car showroom to the roof of a Manhattan skyscraper– anywhere can be an AirVenue.</p>
				<p>Want to have a pool party, but don’t have a pool? Search for a backyard pool to hire in your neighborhood on AirVenues. Looking for a wedding venue that’s so unashamedly different it will make your big day totally unforgettable? Start looking now. Need a unique location to shoot your next video – look no further. Think a toy shop is a great place for a child’s birthday party? We do too,so we’re growing the AirVenues community day by day, to sharemore and more amazing venues with you.</p>
				<p>So, that’s us, AirVenues, we’re helping you, your friends, your colleagues and everyone, get together to make magical moments and lasting memories…any space, anytime, anywhere.</p>
				<p>“One love, one heart…</p>
				<p>Let’s get together and feel all right”</p>
				<p>―Bob Marley </p>

			</div>
			<div class="col-md-4 our_story">
				<img class="img-fluid" src="<?php echo base_url('assets/images/our_story.jpg');?>" />
			</div>
		</div>
	</div>
</section>	
<section class="testimonial-section">
	<div class="container">
		<div class="heading-aera">
			<h1>Testimonials</h1>
			<p>What our customers are saying!</p>
		</div>	
		<div class="row">
			<div class="owl-carousel" id="testimonial-carousel">
				<div class="item">
					<div  class="testimonal-item-element "   >
					<img class='avatar' src="<?php echo base_url('assets/images/avatar3-min.jpg');?>" alt='avatar'><div class='entry-content' style="">“Duis autem vel eum iriure dolor in hen- drerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla.”</div>
					<h4 class='title'>Samuel Palmer</h4>
					<div class='sub-title' >Founder &amp; CEO , Realty Properties Inc</div>
					</div>
				</div>
				<div class="item">
					<div  class="testimonal-item-element "   >
					<img class='avatar' src="<?php echo base_url('assets/images/avatar2-min.jpg');?>"  alt='avatar'>
					<div class='entry-content' style="">“Duis autem vel eum iriure dolor in hen- drerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla.”</div>
					<h4 class='title' style="">Vincent Fuller</h4>
					<div class='sub-title' style="">Company Agent , Cool Houses Inc.</div>
					</div>
				</div>
				<div class="item">
					<div  class="testimonal-item-element "   >
					<img class='avatar' src="<?php echo base_url('assets/images/avatar1-min.jpg');?>"  alt='avatar'><div class='entry-content' style="">“Duis autem vel eum iriure dolor in hen- drerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla.”</div>
					<h4 class='title'>Chris Richards</h4>
					<div class='sub-title'>Marketing Envato Pty Ltd.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>