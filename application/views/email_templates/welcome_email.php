<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<head>
</head>
 
<body bgcolor="#ffffff">
<table class="body-wrap" align="center" border="0" cellpadding="0" cellspacing="0" width="620" bgcolor="#f1f4f5" style="border:solid 1px #f1f4f5; margin:0 auto;">
	<tbody>
		<tr>
			<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px; padding:10px;">	
				<a href="<?php echo base_url();?>" title="<?php echo DEFAULT_SITE_TITLE;?>"><img  alt="<?php echo DEFAULT_SITE_TITLE;?>" src="<?php echo base_url('assets/images/logo-text.png');?>" height="30"></a>
			</td>
		</tr>
		<tr>
			<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px; padding: 10px;" bgcolor="#fbfbfb">
				<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; padding:10px;">
					<tbody>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px;" height="10" valign="top" width="540">&nbsp;</td>
						</tr>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px;" valign="top" width="540">
								<h1 style="font-size:24px;">Welcome to <?php echo DEFAULT_SITE_TITLE;?></h1>
							</td>
						</tr>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px;" valign="top" width="540"><br></td>
						</tr>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px; line-height:18px;" valign="top" width="540">
								<p>Dear <?php echo $user_name;?>,</p>
								<p>Thank you for activating your <b><?php echo DEFAULT_SITE_TITLE;?></b> Account.</p>
								<p>Here are your account details and login information</p>
								<ul style="margin-left:-15px;">
									<li>Web address: <?php echo $website_url;?></li>
									<li>Username: <?php echo $user_email;?></li>
									<li>Password: as entered at activation</li>
								</ul>
								<br/>
								<p>If you forget your password then you can reset it via the login screen on the website or click: <a style="color:#44a0b3; text-decoration:none;" href="<?php echo base_url('reset_password');?>"><?php echo base_url('reset_password');?></a>.</p>
								<h3 style="font-size:20px; font-weight:bold; line-height:2em;">Getting Started</h3>
								<h4 style="font-size:16px; font-weight:bold; line-height:2em;">Dashboard</h4>
								<p>When you login to <?php echo DEFAULT_SITE_TITLE;?> the notifications Dashboard alerts you to all the recent activity:</p>
								<ul style="margin-left:-15px;">
									<li>Files uploaded by your clients that you have not downloaded</li>
									<li>Files that are with clients awaiting their approval</li>
									<li>Files that have recently been approved (or rejected) by clients</li>
								</ul>
							</td>
						</tr>
					</tbody>	
				</table>
			</td>
		</tr>
		<tr>
			<td style="font-family:tahoma, geneva, sans-serif; color:#000000; font-size:12px;" bgcolor="#f1f4f5">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td align="center" style="padding:10px;">
								<p>Email: <a href="mailto:<?php echo DEFAULT_SITE_EMAIL;?>" style="color:#000000; text-decoration:none;"><?php echo DEFAULT_SITE_EMAIL;?></a> &nbsp; | &nbsp; Phone: <a href="tel:<?php echo DEFAULT_PHONE_NO;?>" style="color:#000000; text-decoration:none;"><?php echo DEFAULT_PHONE_NO;?></a></p>
								<p style="color:#000000;"><small>&copy; <?php echo date('Y'). ' '.DEFAULT_SITE_TITLE;?>. All Rights Reserved.</small></p>
							</td>
						</tr>
					</tbody>
				</table>	
			</td>
		</tr>
	</tbody>
</table>
</html>