<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
<head>
</head>
 
<body bgcolor="#ffffff">
<table class="body-wrap" align="center" border="0" cellpadding="0" cellspacing="0" width="620" bgcolor="#f1f4f5" style="border:solid 1px #f1f4f5; margin:0 auto;">
	<tbody>
		<tr>
			<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px; padding:10px;">	
				<a href="<?php echo base_url();?>" title="<?php echo DEFAULT_SITE_TITLE;?>"><img  alt="<?php echo DEFAULT_SITE_TITLE;?>" src="<?php echo base_url('assets/images/logo-hor.svg');?>" height="30"></a>
			</td>
		</tr>
		<tr>
			<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px; padding: 10px;" bgcolor="#fbfbfb">
				<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; padding:10px;">
					<tbody>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px;" height="10" valign="top" width="540">&nbsp;</td>
						</tr>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px;" valign="top" width="540">
								<h1 style="font-size:24px;">Activate Your <?php echo DEFAULT_SITE_TITLE;?> Account</h1>
							</td>
						</tr>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px;" valign="top" width="540"><br></td>
						</tr>
						<tr>
							<td style="font-family:tahoma, geneva, sans-serif;color:rgb(67, 67, 68);font-size:12px; line-height:18px;" valign="top" width="540">
								<p>Dear <?php echo $user_name;?>,</p>
								<p>Thank you for signing up to <b><?php echo DEFAULT_SITE_TITLE;?></b>. We're really excited that you have choosed our service.</p>
								<p>To complete your registration click <a style="color:#9e5bbd; text-decoration:none;" href="<?php echo $account_activation_url;?>"><?php echo $account_activation_url;?></a></p>
								<br />
								<p style="text-align:center;">
									<a style="text-decoration:none; color:#ffffff; background-color:#9e5bbd; width:200px; padding:10px 20px;font-weight:bold; text-align:center;cursor:pointer; display:inline-block;" href="<?php echo $account_activation_url;?>" title="Activate My Account">Activate My Account</a>
								</p>
								<br/>
								<p>If you have any questions regarding the product please contact our Customer Support team or email <a style="color:#9e5bbd; text-decoration:none;" href="mailto:<?php echo DEFAULT_SITE_EMAIL;?>"><?php echo DEFAULT_SITE_EMAIL;?></a>.</p>
								<p>As a reminder, please note that your use of <?php echo DEFAULT_SITE_TITLE;?> is governed by the terms and conditions you agreed to when you signed up, available here: <a style="color:#9e5bbd; text-decoration:none;" href="<?php echo base_url('terms-and-conditions');?>"><?php echo base_url('terms-and-conditions');?></a>.</p>
							</td>
						</tr>
					</tbody>	
				</table>
			</td>
		</tr>
		<tr>
			<td style="font-family:tahoma, geneva, sans-serif; color:#000000; font-size:12px;" bgcolor="#f1f4f5">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td align="center" style="padding:10px;">
								<p>Email: <a href="mailto:<?php echo DEFAULT_SITE_EMAIL;?>" style="color:#000000; text-decoration:none;"><?php echo DEFAULT_SITE_EMAIL;?></a> &nbsp; | &nbsp; Phone: <a href="tel:<?php echo DEFAULT_PHONE_NO;?>" style="color:#000000; text-decoration:none;"><?php echo DEFAULT_PHONE_NO;?></a></p>
								<p style="color:#000000;"><small>&copy; <?php echo date('Y'). ' '.DEFAULT_SITE_TITLE;?>. All rights reserved.</small></p>
							</td>
						</tr>
					</tbody>
				</table>	
			</td>
		</tr>
	</tbody>
</table>
</html>