<div class="user-dashboard-section">
	<div class="container"> 
		<div class="row">
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="user-info-box">
					<div class="user-profile">
                    	<div class="profile-img"> 
                    		<?php if($user_query->num_rows()){ $user_img = $user_query->row();
                    			$file_headers = @get_headers($user_img->profile_image);
                    			
                    			if(stripos($file_headers[0],"200 OK")){
                    		 ?>
                    			<img src="<?php echo $user_img->profile_image;?>" class="img-responsive profile_img"> 
                    			<?php } else{ ?>	
                    				<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    			<?php } ?>
                    		<?php }else{ ?>
                    			<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    		<?php } ?>
                    	</div>
	                    <div class="profile-text"><?php echo $this->session->userdata('user_name');?></div>
	                </div>

	                <?php 

					$sess_address = $this->session->userdata('address');
					$sess_phone = $this->session->userdata('phone');
					$sess_user_email = $this->session->userdata('user_email');

					?>
	                <div class="user-info-content-box">
	                <?php if(!empty($sess_address) || !empty($sess_phone) ||!empty($sess_user_email)){  ?>
	                	<h2>Contact Information</h2>
	                	<?php if(!empty($sess_address)){ ?>
		                	<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('address');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_phone)){ ?>
							<div class="user-info-list">
							   <div class="user-info-list-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p>	 <?php echo $this->session->userdata('phone');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_user_email)){ ?>
							<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('user_email');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php } ?>
						<div class="user-info-list">
						    <div class="user-info-list-icon">
						    	<a href="<?php echo base_url(); ?>" class="btn btn-sm btn-success" style="float: right;">Go to home</a>
						    </div>
						    <div class="user-info-list-content">
						        <a href="<?php echo base_url('sign_out'); ?>" class="btn btn-sm btn-info" style="float: right;">Logout</a>
						    </div>
						</div>
	                </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-lg-8">
				<div class="user-info-box">
					<div class="user-tabs-box">
						<ul class="nav nav-tabs" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link active" href="<?php echo base_url('dashboard'); ?>" >Profile
						    </a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="<?php echo base_url('my_account'); ?>" >Account</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="<?php echo base_url('my_venues'); ?>" >My Venues</a>
						  </li>						 
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane  in active" id="profile">
								<div class="tab-content-body">
									<div class="profile-content-box">
									<?php if($user_query->num_rows()){ $user_data= $user_query->row(); ?>
					    				<div class="user-profile-media">
					    					<?php if($user_data->profile_image==''){?>
					    						<img src="<?php echo base_url('assets/images/default-profile.jpg');?>" class="m-x-auto img-fluid profile_img" alt="avatar"  />
					    					<?php }else{ 
					    						$file_headers = @get_headers($user_img->profile_image);
                    							if(stripos($file_headers[0],"200 OK")){
					    						?>
					    						<img src="<?php echo $user_data->profile_image;?>" class="m-x-auto img-fluid profile_img"   alt="avatar" />
					    						<?php }else{ ?>
					    							<img src="<?php echo base_url('assets/images/default-profile.jpg');?>" class="m-x-auto img-fluid profile_img" alt="avatar"  />
					    					<?php } }?>
				                            <input type="hidden" name="old_image_id" value="<?php echo $user_data->profile_image;?>" id="old_image_id">
										</div>
										<div class="text-center">
											<div class="upload-btn-wrapper">
											  <button class="btn">Upload a file</button>
											  <input type="file" name="myfile" id="profile_image" />
											</div>
										</div>
										<div class="profile-form-content">
											<h2>Basic Information</h2>
											<div class="clearfix">
												<form  method="post" id="save-user">
					    							<div class="row">
					    								<div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                    <label class="flot">First Name</label>
							                                    <input type="text" class="form-control" name="first_name" value="<?php echo  $user_data->first_name;?>" required="">
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                	<label class="flot">Last Name</label>
							                                    <input type="text" name="last_name" class="form-control" value="<?php echo  $user_data->last_name;?>" required="">
							                                </div>
							                            </div>
							                            <div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                	<label class="flot">Email</label>
							                                    <p class="form-control" ><?php echo  $user_data->email;?></p>
							                                </div>
							                            </div>
							                             <div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                	<label class="flot">Phone</label>
							                                    <input type="text" name="phone" value="<?php echo  $user_data->phone;?>" class="form-control" required="">
							                                </div>
							                            </div>
							                            <div class="col-md-12 col-sm-12 col-lg-12">
							                                <div class="form-group">
							                                	<label class="flot">Address</label>
							                                    <input type="text" name="address" value="<?php echo  $user_data->address;?>" class="form-control" required="">
							                                </div>
							                            </div>
							                            <div class="col-md-12 col-sm-12 col-lg-12">
							                                <div class="form-group">
							                                	<label class="flot">Description</label>
							                                    <textarea class="form-control" name="description"><?php echo  $user_data->description;?></textarea>
							                                </div>
							                            </div>
							                            <div class="col-md-4 col-sm-4 col-lg-4">
							                                <div class="form-group submit">
							                                    <button type="submit" value="Send" class="save-button save-user-details">Save Changes</button>
							                                </div>
							                            </div>
							                            <div class="col-md-8 col-sm-8 col-lg-8 alert_massage">
							                                
							                            </div>
					                            	</div>
					                            </form>
											</div>
										</div>
									<?php }?>
									</div>

								</div>
							</div>
														
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.save-user-details').on('click',function(e){
		e.preventDefault();
		$( "#save-user" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "radio" ) {
					error.insertAfter( "#ownership_type_field" );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});

		if ($('#save-user').valid()){
			var user = $("#save-user").serialize();
			$.ajax({
				type : 'post',
				data : user,
				url : "<?php echo base_url(); ?>user/update_user",
				beforeSend : function(){
					$('.alert_massage').html('<div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please wait.......</div>');
					$('.save-button').css('pointer-events','none');
				},
				success : function(res){
					$('.alert_massage').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Updated successfully.</div>');
					$('.save-button').removeAttr('style');
				}
			});
		}
	});

		$("#profile_image").change(function(){
			console.log('fdfd');
			var im = $('#old_image_id').val();
			var old = im.substring(im.lastIndexOf('/') + 1);
			var url = this.value;
		    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
	    if (this.files && this.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
	        var reader = new FileReader();
	        
	        var fd = new FormData();
	        var file_name = this.files[0];
	        fd.append('file',file_name);
	        fd.append('old_image',old);
	       
	        reader.onload = function (e) {
	            $('.profile_img').attr('src', e.target.result);
	                        
	            $.ajax({
		            url: "<?php echo base_url('site/upload_profile_image');?>", 
		           	data: fd,
		            processData: false,
		            contentType: false,
		            type: 'POST',       
		            success: function(data)   
		            {
		            	console.log(data);
		            }
	            });
	        }
	       
	        reader.readAsDataURL(this.files[0]);
	    }
	    else{
	    	alert("Please choose valid image");
	    	return false;
	    }
	});
</script>
<style type="text/css">
	.error.help-block {
	    color: #dc3545;
	    font-size: 90%;
	    position: absolute;
	    left: unset!important;
	    top: unset!important;
	}
</style>