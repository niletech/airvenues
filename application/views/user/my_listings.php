<div class="user-dashboard-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="user-info-box">
					<div class="user-profile">
                    	<div class="profile-img"> 
                    		<img src="<?php echo base_url();?>assets/images/user-profile.jpg"> 
                    	</div>
	                    <div class="profile-text">John doe</div>
	                </div>

	                <div class="user-info-content-box">
	                	<h2>Contact Information</h2>
	                	<div class="user-info-list">
						    <div class="user-info-list-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
						    <div class="user-info-list-content">
						        <p>Greater Denver Area, Colorado</p>
						    </div>
						</div>

						<div class="user-info-list">
						    <div class="user-info-list-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
						    <div class="user-info-list-content">
						        <p>	 +1 303-xxx-xxxx</p>
						    </div>
						</div>

						<div class="user-info-list">
						    <div class="user-info-list-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
						    <div class="user-info-list-content">
						        <p> jone.doe@gmail.com</p>
						    </div>
						</div>
	                </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-lg-8">
				<div class="user-info-box">
					<div class="user-tabs-box">
						<ul class="nav nav-tabs" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link" href="#profile" role="tab" data-toggle="tab">Profile</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="#Account" role="tab" data-toggle="tab">Account</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link active" href="#Listings" role="tab" data-toggle="tab">My Listings</a>
						  </li>						 
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane" id="profile">
								<div class="tab-content-body">
									<div class="profile-content-box">

										<div class="user-profile-media">
				                            <img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-fluid">
										</div>
										<div class="text-center">
											<div class="upload-btn-wrapper">
											  <button class="btn">Upload a file</button>
											  <input type="file" name="myfile" />
											</div>
										</div>
										<div class="profile-form-content">
											<h2>Basic Information</h2>
											<div class="clearfix">
					    						<form action="" method="post">
					    							<div class="row">
					    								<div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                    <label class="flot">First Name</label>
							                                    <input type="text" class="form-control" name="FName" required="">
															</div>
														</div>
														<div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                	<label class="flot">Last Name</label>
							                                    <input type="text" name="LName" class="form-control" required="">
							                                </div>
							                            </div>
							                            <div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                	<label class="flot">Email</label>
							                                    <input type="text" name="Email" class="form-control" required="">
							                                </div>
							                            </div>
							                             <div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group">
							                                	<label class="flot">Phone</label>
							                                    <input type="text" name="Phone" class="form-control" required="">
							                                </div>
							                            </div>
							                            <div class="col-md-12 col-sm-12 col-lg-12">
							                                <div class="form-group">
							                                	<label class="flot">Address</label>
							                                    <input type="text" name="Phone" class="form-control" required="">
							                                </div>
							                            </div>
							                            <div class="col-md-12 col-sm-12 col-lg-12">
							                                <div class="form-group">
							                                	<label class="flot">Description</label>
							                                    <textarea class="form-control" name="Description"></textarea>
							                                </div>
							                            </div>
							                            <div class="col-md-6 col-sm-6 col-lg-6">
							                                <div class="form-group submit">
							                                    <button type="submit" value="Send" class="save-button">Save Changes</button>
							                                </div>
							                            </div>
					                            	</div>
					                            </form>
					                        </div>
										</div>
									</div>

								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="Account">
								<div class="tab-content-body">
									<div class="tab-content-account-info">
										<div class="checkout-section-form-panel">
										    <div class="checkout-payment">
										        <ul class="checkout-payment_methods">
										            <li>										            	
										            	<form action="#" method="post">
										                	<div class="payment_box_method_bacs">
										                		<h2>Owner Account Details</h2>
										                        <p>Lorem Ipsum copy in various charsets and languages for layouts.</p>
										                      	<div class="row">
										                            <div class="col-md-12 col-sm-12 form-group">
										                                <label>Bank Name<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="Bank Name">
										                                </div>
										                            </div>
										                            <div class="col-md-12 col-sm-12 form-group">
										                                <label>Account Holder Name<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="Account Holder Name">
										                                </div>
										                            </div>

										                             <div class="col-md-12 col-sm-12 form-group">
										                                <label>Bank A/c No.<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="password" placeholder="xxxx xxxx xxxx xxxx">
										                                </div>
										                            </div>


										                            <div class="col-md-6 col-sm-6 form-group">
										                                <label>SWIFT Code<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="SWIFT Code">
										                                </div>
										                            </div>
										                            
										                            <div class="col-md-6 col-sm-6 form-group">
										                                <label>Zip / Postal code<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="password" placeholder="Zip / Postal code">
										                                </div>
										                            </div>
																</div>
																<h2>PayPal Account Details</h2>
																<div class="row">																	
										                            <div class="col-md-12 col-sm-12 form-group">
																		<div class="dh-form-box">
																			<label>Business Email</label>
											                                <input class="form-control" type="text" placeholder="Email">
																		</div>
																	</div>
										                            <div class="col-md-12 col-sm-12 form-group">
										                                <div class="dh-form-box">
										                                    <input type="submit" class="save-button" name="woocommerce_checkout_place_order" value="Save" data-value="Place order">
										                                </div>
										                            </div>
										                        </div>
									                        </div>
										                </form>
								            		</li>
										        </ul>
										      </div>
										</div>
									</div>
								</div>														
							</div>
							<div role="tabpanel" class="tab-pane in active" id="Listings">
								<div class="tab-content-body">
									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal3.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Charming Industrial Loft Close</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$150 - $500</h4>
				                                        <a href="#" class="btn">Edit Listing</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="preview" title="preview"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>

									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal1.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Pike Place Market Studio</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$275 - $750</h4>
				                                        <a href="#" class="btn">Edit Listing</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="preview" title="preview"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>


									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal2.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Large Focus Room Located</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$100 - $300</h4>
				                                        <a href="#" class="btn">Edit Listing</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="preview" title="preview"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="Favourite">
								<div class="tab-content-body">
									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal1.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Downtown Loft with Skyline View</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$1200 - $1300</h4>
				                                        <a href="#" class="btn">View Details</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>			                                    		                                    		
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>


									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal1.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Downtown Loft with Skyline View</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$1200 - $1300</h4>
				                                        <a href="#" class="btn">View Details</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>

									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal1.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Downtown Loft with Skyline View</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$1200 - $1300</h4>
				                                        <a href="#" class="btn">View Details</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>


									<div class="tab-content-list">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo base_url();?>assets/images/deal1.png" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="#" title="">Downtown Loft with Skyline View</a></h2>
					                                    <p>In publishing and graphic design, lorem ipsum is a placeholder text </p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : 21.06.2016</span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$1200 - $1300</h4>
				                                        <a href="#" class="btn">View Details</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="delete " title="delete "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>