<div class="user-dashboard-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="user-info-box">
					<div class="user-profile">
                    	<div class="profile-img"> 
                    		<?php if($user_query->num_rows()){ $user_img = $user_query->row();
                    			$file_headers = @get_headers($user_img->profile_image);
                    			
                    			if(stripos($file_headers[0],"200 OK")){
                    		 ?>
                    			<img src="<?php echo $user_img->profile_image;?>" class="img-responsive profile_img"> 
                    			<?php } else{ ?>	
                    				<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    			<?php } ?>
                    		<?php }else{ ?>
                    			<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    		<?php } ?>
                    	</div>
	                    <div class="profile-text"><?php echo $this->session->userdata('user_name');?></div>
	                </div>

	                <?php 
					$sess_address = $this->session->userdata('address');
					$sess_phone = $this->session->userdata('phone');
					$sess_user_email = $this->session->userdata('user_email'); ?>

	                <div class="user-info-content-box">
					<?php if(!empty($sess_address) || !empty($sess_phone) ||!empty($sess_user_email)){ ?>
	                	<h2>Contact Information</h2>
	                	<?php if(!empty($sess_address)){ ?>
		                	<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('address');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_phone)){ ?>
							<div class="user-info-list">
							   <div class="user-info-list-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p>	 <?php echo $this->session->userdata('phone');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_user_email)){ ?>
							<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('user_email');?></p>
							    </div>
							</div>
						<?php } ?>
	            	<?php } ?>
		            	<div class="user-info-list">
						    <div class="user-info-list-icon">
						    	<a href="<?php echo base_url(); ?>" class="btn btn-sm btn-success" style="float: right;">Go to home</a>
						    </div>
						    <div class="user-info-list-content">
						        <a href="<?php echo base_url('sign_out'); ?>" class="btn btn-sm btn-info" style="float: right;">Logout</a>
						    </div>
						</div>
	                </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-lg-8">
				<div class="user-info-box">
					<div class="user-tabs-box">
						<ul class="nav nav-tabs" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link " href="<?php echo base_url('dashboard'); ?>" >Profile
						    </a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link active" href="<?php echo base_url('my_account'); ?>" >Account</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link " href="<?php echo base_url('my_venues'); ?>" >My Venues</a>
						  </li>						 
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							
							<div role="tabpanel" class="tab-pane in active" id="Account">
								<div class="tab-content-body">
									<div class="tab-content-account-info">
										<div class="checkout-section-form-panel">
										    <div class="checkout-payment">
										        <ul class="checkout-payment_methods">
										            <li>										            
										            	<form method="post" id="account-form">
										                	<div class="payment_box_method_bacs">
										                		<h2>Owner Account Details</h2>
										                		
										                      	<div class="row">
										                            <div class="col-md-12 col-sm-12 form-group">
										                                <label>Bank Name<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="Bank Name" required="" name="bank_name" value="<?php if($bank){ echo $bank->bank_name;} ?>">
										                                </div>
										                            </div>
										                            <div class="col-md-12 col-sm-12 form-group">
										                                <label>Account Holder Name<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="Account Holder Name" required="" name="account_holder_name" value="<?php if($bank){ echo $bank->account_holder_name;} ?>">
										                                </div>
										                            </div>

										                             <div class="col-md-12 col-sm-12 form-group">
										                                <label>Bank A/c No.<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="xxxx xxxx xxxx xxxx" required="" name="account_number" value="<?php if($bank){ echo $bank->account_number;} ?>">
										                                </div>
										                            </div>


										                            <div class="col-md-6 col-sm-6 form-group">
										                                <label>SWIFT Code<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="SWIFT Code" required="" name="swift_code" value="<?php if($bank){ echo $bank->swift_code;} ?>">
										                                </div>
										                            </div>
										                            
										                            <div class="col-md-6 col-sm-6 form-group">
										                                <label>Zip / Postal code<span>*</span></label>
										                                <div class="dh-form-box">
										                                    <input class="form-control" type="text" placeholder="Zip / Postal code" required="" name="zip_code" value="<?php if($bank){ echo $bank->zip_code;} ?>">
										                                </div>
										                            </div>
																</div>
																<h2>PayPal Account Details</h2>
																<div class="row">																	
										                            <div class="col-md-12 col-sm-12 form-group">
																		<div class="dh-form-box">
																			<label>Business Email</label>
											                                <input class="form-control" type="email" placeholder="Email" required="" name="bussiness_email" value="<?php if($bank){ echo $bank->bussiness_email;} ?>">
																		</div>
																	</div>
										                            <div class="col-md-4 col-sm-4 form-group">
										                                <div class="dh-form-box">
										                                    <input type="submit" class="save-button account-save" name="woocommerce_checkout_place_order" value="Save" data-value="Place order">
										                                </div>
										                            </div>
										                            <div class="col-md-8 col-sm-8 col-lg-8 alert_massage">
										                                
										                            </div>
										                        </div>
									                        </div>
										                </form>
								            		</li>
										        </ul>
										      </div>
										</div>
									</div>
								</div>														
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.account-save').on('click',function(e){
		e.preventDefault();
		$( "#account-form" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "radio" ) {
					//error.insertAfter( "#ownership_type_field" );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});

		if ($('#account-form').valid()){
			var account = $("#account-form").serialize();
			$.ajax({
				type : 'post',
				data : account,
				url : "<?php echo base_url(); ?>user/save_user_account",
				beforeSend : function(){
					$('.alert_massage').html('<div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please wait.......</div>');
					$('.save-button').css('pointer-events','none');
				},
				success : function(res){
					$('.alert_massage').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Saved successfully.</div>');
					$('.save-button').removeAttr('style');
				}
			});
		}
	});
</script>
<style type="text/css">
	.error.help-block {
	    position: unset!important;
	}
	.error {
	    font-size: initial!important;
	}
</style>