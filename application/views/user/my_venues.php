<div class="user-dashboard-section">
	<div class="container"> 
		<div class="row">
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="user-info-box">
					<div class="user-profile">
                    	<div class="profile-img"> 
                    		<?php if($user_query->num_rows()){ $user_img = $user_query->row();
                    			$file_headers = @get_headers($user_img->profile_image);
                    			
                    			if(stripos($file_headers[0],"200 OK")){
                    		 ?>
                    			<img src="<?php echo $user_img->profile_image;?>" class="img-responsive profile_img"> 
                    			<?php } else{ ?>	
                    				<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    			<?php } ?>
                    		<?php }else{ ?>
                    			<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    		<?php } ?>
                    	</div>
	                    <div class="profile-text"><?php echo $this->session->userdata('user_name');?></div>
	                </div>

	                <?php 
					$sess_address = $this->session->userdata('address');
					$sess_phone = $this->session->userdata('phone');
					$sess_user_email = $this->session->userdata('user_email'); ?>

	                <div class="user-info-content-box">
					<?php if(!empty($sess_address) || !empty($sess_phone) ||!empty($sess_user_email)){ ?>
	                	<h2>Contact Information</h2>
	                	<?php if(!empty($sess_address)){ ?>
		                	<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('address');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_phone)){ ?>
							<div class="user-info-list">
							   <div class="user-info-list-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p>	 <?php echo $this->session->userdata('phone');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_user_email)){ ?>
							<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('user_email');?></p>
							    </div>
							</div>
						<?php } ?>
	            	<?php } ?>
		            	<div class="user-info-list">
						    <div class="user-info-list-icon">
						    	<a href="<?php echo base_url(); ?>" class="btn btn-sm btn-success" style="float: right;">Go to home</a>
						    </div>
						    <div class="user-info-list-content">
						        <a href="<?php echo base_url('sign_out'); ?>" class="btn btn-sm btn-info" style="float: right;">Logout</a>
						    </div>
						</div>
	                </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-lg-8">
				<div class="user-info-box">
					<div class="user-tabs-box">
						<ul class="nav nav-tabs" role="tablist">
						  <li class="nav-item">
						    <a class="nav-link " href="<?php echo base_url('dashboard'); ?>" >Profile
						    </a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link" href="<?php echo base_url('my_account'); ?>" >Account</a>
						  </li>
						  <li class="nav-item">
						    <a class="nav-link active" href="<?php echo base_url('my_venues'); ?>" >My Venues</a>
						  </li>						 
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
														
							<div role="tabpanel" class="tab-pane active" id="Listings">
								<div class="tab-content-body">
									<?php if($venue_lists->num_rows() > 0){ foreach($venue_lists->result() as $list){ ?>
									<div class="tab-content-list delete-<?php echo $list->id; ?>">
										<div class="tab-content-list-inner">
				                            <div class="row">
				                                <div class="col-md-2 col-sm-2 col-xs-12">
				                                    <div class="tab-content-media">
				                                    <a href="#">
				                                    	<img src="<?php echo $list->image; ?>" class="img-fluid">
				                                    </a>
				                                    </div>
				                                </div>

				                                <div class="col-md-6 col-sm-6 col-xs-12">
				                                	<div class="tab-content-information">
					                                    <h2><a href="<?php echo base_url('edit_venue/'.$list->slug); ?>" title=""><?php echo $list->space_title; ?></a></h2>
					                                    <p><?php echo word_limiter($list->space_description,15); ?></p>

					                                    <div class="Status-box">
					                                        <span>Status : In Progress</span> 
					                                        <span>Date : <?php echo date('j, M Y', strtotime($list->created_at)); ?></span>
					                                    </div>
				                               		 </div>
				                            	</div>
				                              
				                                <div class="col-md-4 col-sm-4 col-xs-12">
				                                    <div class="tab-content-information-price text-center">
				                                        <h4>$<?php echo $list->hourly_rate; ?>/Hour</h4>
				                                        <a href="<?php echo base_url('edit_venue/'.$list->slug); ?>" class="btn"><i class="fa fa-pencil"></i> Edit Venue</a>
				                                    </div>
				                                    <div class="btn-box">
				                                    	<ul>
				                                    		<li>
				                                    			 <a href="#" class="btn" alt="preview" title="preview"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    		<li>
				                                    			 <a href="javascript:void(0)" class="btn delete-venue" alt="delete " title="delete" data-id="<?php echo $list->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				                                    		</li>
				                                    		
				                                    	</ul>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
									</div>

									<?php 
										} 
									}else{ ?>
										<h2 class="text-center">Record not found !!!</h2>
									<?php } ?>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).on('click','.delete-venue',function(){
		var id = $(this).data('id');
		var self = $(this);
		if(id && confirm('Are you sure to delete record?')){
			$.ajax({
				type : 'post',
				url : "<?php echo base_url('user/delete_venue') ?>",
				data : {'id':id},
				beforeSend : function(){self.html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>')},
				success : function(res) {
					$('.delete-'+id).remove();
				}
			});
		}
	});
</script>
