<?php $type_of_space = json_decode(TYPE_OF_SPACE);  ?>
<link href="<?php echo base_url('assets/css/dropzone.min.css');?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/dropzone.min.js');?>"></script>
<div class="user-dashboard-section">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6 col-lg-4">
				<div class="user-info-box">
					<div class="user-profile">
                    	<div class="profile-img"> 
                    		<?php if($user_query->num_rows()){ $user_img = $user_query->row();
                    			$file_headers = @get_headers($user_img->profile_image);
                    			
                    			if(stripos($file_headers[0],"200 OK")){
                    		 ?>
                    			<img src="<?php echo $user_img->profile_image;?>" class="img-responsive profile_img"> 
                    			<?php } else{ ?>	
                    				<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    			<?php } ?>
                    		<?php }else{ ?>
                    			<img src="<?php echo base_url();?>assets/images/user-profile.jpg" class="img-responsive profile_img"> 
                    		<?php } ?>
                    	</div>
	                    <div class="profile-text"><?php echo $this->session->userdata('user_name');?></div>
	                </div>
					
					<?php 
					$sess_address = $this->session->userdata('address');
					$sess_phone = $this->session->userdata('phone');
					$sess_user_email = $this->session->userdata('user_email'); ?>

	                <div class="user-info-content-box">
					<?php if(!empty($sess_address) || !empty($sess_phone) ||!empty($sess_user_email)){ ?>
	                	<h2>Contact Information</h2>
	                	<?php if(!empty($sess_address)){ ?>
		                	<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('address');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_phone)){ ?>
							<div class="user-info-list">
							   <div class="user-info-list-icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p>	 <?php echo $this->session->userdata('phone');?></p>
							    </div>
							</div>
						<?php } ?>
						<?php if(!empty($sess_user_email)){ ?>
							<div class="user-info-list">
							    <div class="user-info-list-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
							    <div class="user-info-list-content">
							        <p> <?php echo $this->session->userdata('user_email');?></p>
							    </div>
							</div>
						<?php } ?>
	            	<?php } ?>
		            	<div class="user-info-list">
						    <div class="user-info-list-icon">
						    	<a href="<?php echo base_url(); ?>" class="btn btn-sm btn-success" style="float: right;">Go to home</a>
						    </div>
						    <div class="user-info-list-content">
						        <a href="<?php echo base_url('sign_out'); ?>" class="btn btn-sm btn-info" style="float: right;">Logout</a>
						    </div>
						</div>
	                </div>
				</div>
			</div>
			<div class="col-md-8 col-sm-8 col-lg-8">
				<div class="user-info-box">
					<div class="user-tabs-box">
						<div class="row">
						    <div class="col-md-6"><h2 class="update-title">Update Venue</h2></div>
						    <div class="col-md-6"><a href="<?php echo base_url('/my_venues'); ?>" class="btn btn-sm btn-secondary" style="float: right;"><i class="fa fa-arrow-left"></i> Back</a></div>
						</div>
						<!-- Tab panes -->
						    <div class="venue-div">
						    	<?php if($venue_data->num_rows()>0){ $venue = $venue_data->row();  ?>
						        <form name="venue-details" id="venue-details">
						        	<input type="hidden" name="venue_id" value="<?php echo $venue->id; ?>">
						        	<div class="row">
						        		<div class="col-md-8 alert_massage">
						        			
						        		</div>
						        		<div class="col-md-4">
						        			<button class="save-venue-button" type="button">Update</button>
						        		</div>
						        	</div>
						        	<div id="accordion">
						        	    <div class="card">
						        	      <div class="card-header">
						        	        <a class="card-link" data-toggle="collapse" href="#collapseOne">
						        	          ABOUT YOUR SPACE #1
						        	        </a>
						        	      </div>
						        	      <div id="collapseOne" class="collapse show" data-parent="#accordion">
						        	        <div class="card-body">
						        	          <p>Tell us about your event space so that we can promote it to the world.</p>
						        	          <p>What kind of event space do you have?</p>
						        	          <select name="space_type" id="space_type" class="form-control">
												<option value="">Select</option>
						        	          	<?php if(!empty($venue_spaces)){ foreach($venue_spaces as $space){ ?>
													<option value="<?php echo $space->id;  ?>" <?php if($venue->venues_space_id==$space->id){ echo "selected";} ?>><?php echo $space->space_title;  ?></option>
						        	          	<?php }}else{ ?>
						        	          		<option value="">Record not found !!</option>
						        	          	<?php } ?>
						        	          </select>
						        	          <p>What type of space is it?</p>
												<div class="row">
						        	          	<?php if(!empty($type_of_space)){ foreach($type_of_space as $space_key=>$space_value){ ?>
						        	          		<div class="col-md-6">
						        	              		<div class="form-check-inline">
						        	                		<div class="custom-control custom-radio">
						        	                    		<input type="radio" class="custom-control-input" id="<?php echo $space_key; ?>" name="ownership_type" value="<?php echo $space_value; ?>" <?php if($venue->ownership_type==$space_value){ echo "checked"; } ?>>
						        	                    		<label class="custom-control-label" for="<?php echo $space_key; ?>"><?php echo $space_value; ?></label>
						        	                  		</div> 
						        	              		</div>
						        	              	</div>
						        	            <?php } } ?>
						        	        	</div>

												<p>Space details</p>
												<div class="form-group">
													<label>Listing Title*</label> 
													<input type="text" name="title" class="form-control" placeholder="Listing Title" value="<?php echo $venue->space_title; ?>" required>
													<input type="hidden" name="old_title" value="<?php echo $venue->space_title; ?>">
												</div>
												<div class="form-group">
													<label>Listing Details*</label>
													<textarea name="description" class="form-control" placeholder="Listing Details"><?php echo $venue->space_description; ?></textarea>
												</div>
												<div class="form-group">
													<label>Location*</label>
													<input type="text" name="location" class="form-control" id="pac-input" placeholder="Location" value="<?php echo $venue->location; ?>">
													<input type="hidden" name="lattitude" id="lattitude" value="<?php echo $venue->lattitude; ?>">
													<input type="hidden" name="longitude" id="longitude" value="<?php echo $venue->longitude; ?>">
												</div>

						        	        </div>
						        	      </div> 
						        	    </div> <!-- first card end -->
						        	    <div class="card">
						        	      <div class="card-header">
						        	        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
						        	        AVAILABILITY #2
						        	      </a>
						        	      </div>
						        	      <div id="collapseTwo" class="collapse" data-parent="#accordion">
						        	        <div class="card-body">
						        	        	<?php 
						        	        		$days = explode('/', $venue->available_days);
						        	        		$days_arr = json_decode(DAYS);

						        	        	?>
						        	          <p>When is your space normally open for bookings?</p>
						        	          <div class="currency-change-list">
						        	          	<p>Select when your space is bookable</p>
						        	          	<em id="week_days_error"></em>
						        	          	<ul>
						        	          		<?php foreach($days_arr as $day){ ?>
							        	          		<li>
							        	          			<div class="checkbox-box">
							        	          				<input type="checkbox" value="<?php echo $day; ?>" name='day[]' id="<?php echo $day; ?>" required <?php if(in_array($day, $days)){echo 'checked';} ?> >
							        	          				<label for="<?php echo $day; ?>"><?php echo $day; ?></label>
							        	          			</div>
							        	          		</li>
													<?php } ?>
						        	          	</ul>
						        	          </div>
						        	          <?php
						        	          $time_slot_array = array(
						        	          	"01:00:00" => "1:00am", "01:15:00" => "1:15am", "01:30:00" => "1:30am", "01:45:00" => "1:45am",
						        	          	"02:00:00" => "2:00am", "02:15:00" => "2:15am", "02:30:00" => "2:30am", "02:45:00" => "2:45am",
						        	          	"03:00:00" => "3:00am", "03:15:00" => "3:15am", "03:30:00" => "3:30am", "03:45:00" => "3:45am",
						        	          	"04:00:00" => "4:00am", "04:15:00" => "4:15am", "04:30:00" => "4:30am", "04:45:00" => "4:45am",
						        	          	"05:00:00" => "5:00am", "05:15:00" => "5:15am", "05:30:00" => "5:30am", "05:45:00" => "5:45am",
						        	          	"06:00:00" => "6:00am", "06:15:00" => "6:15am", "06:30:00" => "6:30am", "06:45:00" => "6:45am",
						        	          	"07:00:00" => "7:00am", "07:15:00" => "7:15am", "07:30:00" => "7:30am", "07:45:00" => "7:45am",
						        	          	"08:00:00" => "8:00am", "08:15:00" => "8:15am", "08:30:00" => "8:30am", "08:45:00" => "8:45am",
						        	          	"09:00:00" => "9:00am", "09:15:00" => "9:15am", "09:30:00" => "9:30am", "09:45:00" => "9:45am",
						        	          	"10:00:00" => "10:00am", "10:15:00" => "10:15am", "10:30:00" => "10:30am", "10:45:00" => "10:45am",
						        	          	"11:00:00" => "11:00am", "11:15:00" => "11:15am", "11:30:00" => "11:30am", "11:45:00" => "11:45am",
						        	          	"12:00:00" => "12:00pm", "12:15:00" => "12:15pm", "12:30:00" => "12:30pm", "12:45:00" => "12:45pm",
						        	          	"13:00:00" => "1:00pm", "13:15:00" => "1:15pm", "13:30:00" => "1:30pm", "13:45:00" => "1:45pm",
						        	          	"14:00:00" => "2:00pm", "14:15:00" => "2:15pm", "14:30:00" => "2:30pm", "14:45:00" => "2:45pm",
						        	          	"15:00:00" => "3:00pm", "15:15:00" => "3:15pm", "15:30:00" => "3:30pm", "15:45:00" => "3:45pm",
						        	          	"16:00:00" => "4:00pm", "16:15:00" => "4:15pm", "16:30:00" => "4:30pm", "16:45:00" => "4:45pm",
						        	          	"17:00:00" => "5:00pm", "17:15:00" => "5:15pm", "17:30:00" => "5:30pm", "17:45:00" => "5:45pm",
						        	          	"18:00:00" => "6:00pm", "18:15:00" => "6:15pm", "18:30:00" => "6:30pm", "18:45:00" => "6:45pm",
						        	          	"19:00:00" => "7:00pm", "19:15:00" => "7:15pm", "19:30:00" => "7:30pm", "19:45:00" => "7:45pm",
						        	          	"20:00:00" => "8:00pm", "20:15:00" => "8:15pm", "20:30:00" => "8:30pm", "20:45:00" => "8:45pm",
						        	          	"21:00:00" => "9:00pm", "21:15:00" => "9:15pm", "21:30:00" => "9:30pm", "21:45:00" => "9:45pm",
						        	          	"22:00:00" => "10:00pm", "22:15:00" => "10:15pm", "22:30:00" => "10:30pm", "22:45:00" => "10:45pm",
						        	          	"23:00:00" => "11:00pm", "23:15:00" => "11:15pm", "23:30:00" => "11:30pm", "23:45:00" => "11:45pm",
						        	          );
						        	          ?>
						        	          <div class="row">
						        	          	<div class="col-md-6">
						        	          		<div class="form-group">
						        	          			<select name="start_time" id="start_time" class="form-control select2" required>
						        	          				<option value="">Select Start Time</option>
						        	          				<?php foreach ($time_slot_array as $key => $val) { ?>
						        	          				<option value="<?php echo $key; ?>" <?php if($venue->available_from==$key){ echo "selected"; } ?>  ><?php echo $val; ?></option>
						        	          				<?php } ?>										
						        	          			</select>
						        	          		</div>
						        	          	</div>
						        	          </div>
						        	          <div class="row">
						        	          	<div class="col-md-6">
						        	          		<div class="form-group">
						        	          			<select name="end_time" id="end_time" class="form-control" required>
						        	          				<option value="">Select End Time</option>
						        	          				<?php foreach ($time_slot_array as $key => $val) { ?>
						        	          				<option value="<?php echo $key; ?>" <?php if($venue->available_to==$key){ echo "selected"; } ?> ><?php echo $val; ?></option>
						        	          				<?php } ?>										
						        	          			</select>
						        	          		</div>
						        	          	</div>
						        	          </div>

						        	        </div>
						        	      </div>
						        	    </div> <!-- second card end -->
						        	    <div class="card">
						        	      <div class="card-header">
						        	        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
						        	          PRICING & ACCESSIBILITY #3
						        	        </a>
						        	      </div>
						        	      <div id="collapseThree" class="collapse" data-parent="#accordion">
						        	        <div class="card-body">
						        	          
						        	          <div class="row">
						        	          	<div class="col-md-6">
						        	          		<p>Pricing</p>
						        	          		<input type="text" class="form-control pricing_area-input" value="<?php echo $venue->hourly_rate; ?>" name="hourly_rate" required placeholder="Hourly rate">
						        	          	</div>
						        	          	<div class="col-md-6">
						        	          		<p>8+ hour discount</p>
						        	          		<div class="form-group">
						        	          			<div class="sqft_area">
						        	          				<input type="number" class="form-control sqft_input" value="<?php echo $venue->discount; ?>" name="discount" placeholder="Discount">
						        	          			
						        	          				<div class="sqft_icon"><span class="icon">% off</span></div>
						        	          			</div>
						        	          		</div>
						        	          	</div>
						        	          </div>

						        	          	<div class="row">
						        	          		<div class="col-md-12">
						        	          			<p>Capacity</p>
						        	          		</div>
						        	          		<div class="col-md-6">
						        	          			<p>Maximum number of guests</p>
						        	          			<div class="form-group">
						        	          				<div class="pricing_area">
						        	          					<div class="pricing_area-icon"><span class="icon"><i class="fa fa-user" aria-hidden="true"></i></span></div>
						        	          					<input type="text" class="form-control pricing_area-input" value="<?php echo $venue->capacity; ?>" name="guests" required="" placeholder="Guests">
						        	          					
						        	          				</div>
						        	          			</div>
						        	          		</div>
						        	          		<div class="col-md-6">
						        	          			<div class="form-group">
						        	          				<p>Minimum number of hours</p>
						        	          				<div class="pricing_area">
						        	          					<div class="pricing_area-icon"><span class="icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span></div>
						        	          					<input type="text" class="form-control pricing_area-input" value="<?php echo $venue->hours; ?>" name="hours" required="" placeholder="Hours">
						        	          					
						        	          				</div>
						        	          			</div>
						        	          		</div>

						        	          		<div class="col-md-6">
						        	          			<div class="form-group">
						        	          				<p>Maximum number of space</p>
						        	          				<div class="sqft_area">
						        	          					<input type="number" value="<?php echo $venue->space_area; ?>" class="form-control sqft_input" name="space_area" required placeholder="Available Space">
						        	          					
						        	          					<div class="sqft_icon"><span class="icon">Sq. ft.</span></div>
						        	          				</div>
						        	          			</div>	
						        	          		</div>	
						        	          		<div class="col-md-6">
						        	          			<div class="form-group">
						        	          				<p>Maximum number of rooms</p>
						        	          				<div class="sqft_area">
						        	          					<input type="number" value="<?php echo $venue->total_rooms; ?>" class="form-control" name="total_rooms" required min='1' placeholder="Number of rooms" >
						        	          					
						        	          				</div>
						        	          			</div>	
						        	          		</div>
						        	          		
						        	          	</div>
						        	          	
						        	          	<div class="row">
						        	          		<div class="col-md-12 col-lg-12 col-sm-12">
						        	          			<div class="step-infor-box-content">
						        	          			
						        	          				<h2>Off-Site Amenities</h2>
						        	          				<p><b>At least one amenity must be selected</b></p>
						        	          				<div class="clearfix"></div>
						        	          				<?php

						        	          				$emenities = json_decode(EMENITIES);
						        	          				$emenities_arr = explode('/', $venue->amenities);
						        	          				?>
						        	          				<ul class="amens-checkboxe-list" id="amt_lists">
						        	          					<?php foreach($emenities as $amt){?>
						        	          					<li>
						        	          						<div class="checkbox-box-amenities">
						        	          							<input type="checkbox" name="amenities[]" value="<?php echo $amt; ?>" <?php if(in_array($amt, $emenities_arr)){ echo "checked"; }  ?> >
						        	          							<label for="<?php echo $amt;?>"></label>
						        	          							<?php echo $amt;?>
						        	          						</div>
						        	          					</li>
						        	          					<?php }?>
						        	          				</ul>	
						        	          				<div class="step-infor-form-box we-form-box">
						        	          					<p><b>Add your own amenities</b></p>
						        	          					<div class="form-group">
						        	          						<input type="text" class="form-control" name="own_amenities" id="own_amt" />
						        	          					</div>
						        	          					<div class="form-group">
						        	          						<input type="button" class="btn-button" onclick="add_own_amt()" value="+ Add" />
						        	          					</div>

						        	          				</div>
						        	          			</div>
						        	          		</div>
						        	          	</div>
						        	          	
						        	          </div>
						        	        </div>
						        	     
						        	    </div> <!-- third card end -->
						        	    </form>
						        	    <div class="card">
						        	      <div class="card-header">
						        	        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
						        	          SPACE PHOTOS #4
						        	        </a>
						        	      </div>
						        	      <div id="collapseFour" class="collapse" data-parent="#accordion">
						        	        <div class="card-body">
						        	          <div class="we-form-box">
													<h2>Please add space photos</h2>
													<?php if($venue_gallery->num_rows() > 0){   ?>
													<div class="space-media-info">
														<div class="row">
														<?php foreach($venue_gallery->result() as $img){ 
														
														$fname = pathinfo($img->thumb_url);
														if(file_exists(FCPATH.'uploads/venue_gallery_thumbnail/'.$fname['basename'])){
													?>
													<div class="col-md-3 col-sm-6 col-lg-3 main-<?php echo $img->id; ?>">
													<div class="space-media-inner">
															<img src="<?php echo $img->thumb_url; ?>" class="img-thumbnail">
															<div class="space-media-content">
																<a class="cz-remove fa fa-trash text-danger" href="javascript:undefined;"  data-id="<?php echo $img->id; ?>" data-name="<?php echo $fname['basename']; ?>"></a>
																<a class="cz-view fa fa-eye text-muted" href="javascript:undefined;"  data-id="<?php echo $img->id; ?>" style="<?php if($img->is_main==1){ echo 'border: 1px solid #1e7e34'; } ?>" ></a>
															</div>
														</div>
													</div>
													<?php 
																} 
															} ?>

													</div>
												</div>
															<?php
														} 
													?> 
													
													<form action="<?php echo base_url('user/upload_venue_image/'.$venue->id);?>" class="dropzone" id="profile">
														
													</form>						
												</div>
						        	        </div>
						        	      </div>
						        	    </div> <!-- fourth card end -->
						        	  </div>
						        
						    	<?php }else{ ?>
						    		<p class="text-center">Record not found !!</p>
						    	<?php } ?>
						    </div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	.venue-div p {
	    margin: 10px 0;
	    font-size: 16px;
	    color: #000;
	    font-weight: bold;
	}
	.checkbox-box input[type=checkbox]+label:after {
    	width: 55px!important;
    }
    .checkbox-box input[type=checkbox]+label {
        width: 55px!important;
    }
	
	.space-media-content {
	    margin-top: 5px;
	    margin-bottom: 5px;
	}
	.space-media-inner {
	    text-align: center;
	}
	.space-media-content a.cz-remove.fa.fa-trash.text-danger {
	    box-shadow: 0 0 30px #eee;
	    width: 30px;
	    height: 30px;
	    line-height: 30px;
	    border-radius: 3px;
	    border: 1px solid #eee;
	}	
	a.dz-view.fa.fa-eye{
		box-shadow: 0 0 30px #eee;
		width: 30px;
		height: 30px;
		line-height: 30px;
		border-radius: 3px;
		border: 1px solid #eee;
		position: relative;
		left: -32px;
		top: -25px;
		cursor: pointer;
    	float: right;
	}
	a.dz-view.fa.fa-eye:before {
	    padding-left: 7px;
	}
	a.dz-remove.fa.fa-trash{
		box-shadow: 0 0 30px #eee;
		width: 30px;
		height: 30px;
		line-height: 30px;
		border-radius: 3px;
		border: 1px solid #eee;
		position: relative;
		left: 24px;
		top: 5px;
	}
	a.cz-view.fa.fa-eye {
	    box-shadow: 0 0 30px #eee;
	    width: 30px;
	    height: 30px;
	    line-height: 30px;
	    border-radius: 3px;
	    border: 1px solid #eee;
	}
	.space-media-info {
	    border: 1px solid #eee;
	    padding: 10px;
	    margin-bottom: 10px;
	}
	.save-venue-button{
	    text-align: right;
        background: #a05cbf;
        color: #fff;
        padding: 4px 20px;
        font-weight: bolder;
        box-shadow: 0 4px 15px 0 rgba(119, 61, 199, 0.26);
        border: 0;
        float: right;
        margin: 0 4px 9px 0;
        text-transform: uppercase;
        cursor: pointer;
	}
	.error {
	    font-size: initial!important;
	}
	.error.help-block {
	    font-size: initial!important;
	    position: unset!important;
	}
</style>

<script  src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_MAP_API_KEY; ?>&libraries=places"></script>
<script type="text/javascript">
var autocomplete = new google.maps.places.Autocomplete($("#pac-input")[0], {});

google.maps.event.addListener(autocomplete, 'place_changed', function() {
    var places = autocomplete.getPlace();
    $('#lattitude').val(places.geometry.location.lat());
    $('#longitude').val(places.geometry.location.lng());
});

function add_own_amt(){
	var custom_amt = $('#own_amt').val();
	if(custom_amt!=''){
		$('#amt_lists').append(
			$('<li>').append(
			'<div class="checkbox-box-amenities"><input type="checkbox" name="amenities[]" value="'+custom_amt+'"><label for="'+custom_amt+'"></label>'+custom_amt+'</div>'
			)); 
		$('#own_amt').val('');
	}else{
		alert('Please enter own amenities first.');
		$('#own_amt').focus();
	}
}

	Dropzone.options.profile = {
		
	  init: function() {
	  	
	  	this.id = 0;
	  
	    this.on("success", function(file,resp) { 
	    	var res = $.parseJSON(resp);
	    	if(res.msg=='success'){
	    		this.id = res.id;
	    		
	    		$(file.previewTemplate).children('.dz-remove').attr('image_id',res.id);
	    		$('<a class="dz-view fa fa-eye text-muted" href="javascript:undefined;" data-id="'+this.id+'"></a>').insertAfter($(file.previewTemplate).children('.dz-remove'));
	    	}
	    }),

	    this.on("complete", function(file) {
           $(".dz-remove").text('').addClass('fa fa-trash text-danger');
           
       	}),
       	this.on('addedfile',function(file){
       		
       	})
	  }
	};


	Dropzone.autoDiscover = false; 
	$(".dropzone").dropzone({
	 addRemoveLinks: true,
	
	 removedfile: function(file) { 
	 	var name = file.name;
	 var image_id = $(file.previewTemplate).children('.dz-remove').attr('image_id');
	  $.ajax({
		   type: 'POST',
		   url: '<?php echo base_url(); ?>site/remove_drop_box_file/'+image_id,
		   data: {name: name,'folder_name':'venue_gallery'},
		   success: function(data){
	    
	   	   }
	  });
	  var _ref;
	  return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
	 }
	});

	$(document).on('click','.dz-view, .cz-view',function(){
		var image_id = $(this).data('id');
		var self = $(this);
		if(confirm('Are you sure to make this image is default image?')){
			$.ajax({
				type : 'post',
				url : "<?php echo base_url('site/make_default_image') ?>/"+image_id,
				success : function(res){
					
					if(self.hasClass('text-muted')){
						$('.dz-view, .cz-view').removeAttr('style').removeClass('text-success').addClass('text-muted');
						self.removeClass('text-muted');
						self.addClass('text-success').css('border','1px solid #1e7e34');
					}else{
						$('.dz-view, .cz-view').removeAttr('style').removeClass('text-success').addClass('text-muted');
						self.addClass('text-success').css('border','1px solid #1e7e34');
					}
					
				}
			});
		}
		
	})

	$(document).on('click','.cz-remove',function(){
		var image_id = $(this).data('id');
		var name = $(this).data('name');
		  $.ajax({
			   type: 'POST',
			   url: '<?php echo base_url(); ?>site/remove_drop_box_file/'+image_id,
			   data: {name: name,'folder_name':'venue_gallery'},
			   success: function(data){
			   	console.log('deleted');
		    		$(".main-"+image_id).remove();
		   	   }
		  });
	});

	$(document).on('click','.save-venue-button',function(){
		$( "#venue-details" ).validate( {
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				error.addClass( "help-block" );

				if ( element.prop( "type" ) === "checkbox" ) {
					//error.insertAfter( element.parent( "em" ) );
					error.insertAfter( '#week_days_error' );
				} else {
					error.insertAfter( element );
				}
			},
			focusInvalid: false,
			invalidHandler: function(form, validator) {
				
				if (!validator.numberOfInvalids())
					return;
				
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top
				}, 1000);
				
			}
		});

		if($("#venue-details").valid()){
			var formData = $("#venue-details").serialize();
			$.ajax({
				type : 'post',
				url : "<?php echo base_url('user/update_venue'); ?>",
				data : formData,
				beforeSend : function(){
					$('.alert_massage').html('<div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please wait.......</div>');
					$('.save-venue-button').css('pointer-events','none');
				},
				success : function(res){
					$('.alert_massage').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Updated successfully.</div>');
					$('.save-venue-button').removeAttr('style');
				}
			});
		}
	})
</script>