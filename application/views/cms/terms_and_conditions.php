<h2>Terms and Conditions</h2>
<p>
THIS AGREEMENT is BETWEEN:

Secure Space, incorporated and registered in England and Wales with company number XXXXXXX, whose registered office is at Riding Court House, Riding Court Road, Datchet, Slough SL3 9JT ("We/Us/Our")

AND:

The person, firm or company that has registered to use the Services ("Customer/You/Your")

You should print a copy of these terms and conditions for future reference but please note clause 17.2. 

Use of the Services constitutes acceptance of these terms and conditions. Please understand that if you refuse to accept these terms and conditions, you will not be able to use the Services.

1. Definitions & Interpretation

1.1 Definitions

"Agreement" means these terms and conditions for the supply of Services as may be amended by Us from time to time and any other documents expressly incorporated by Us;

"Charges" means any charges due under this Agreement as set out on Our Website or otherwise made known to you at the time of registration;

“Client End User” means any client of Yours using the OpenSpace product to facilitate the provision of accountancy services by You to them, including for the purpose of sharing files between You and them. The relationship between such client and Us will be governed by a separate agreement;

"Commencement Date" means the date You accept these terms;

"Consultants" means the employees, subcontractors and consultants We use to perform any of the Services;

"Delivery Date" means the estimated date(s) when the Services shall first be made available to You;

"Documentation" means (where available) the operating manuals, user instructions, technical literature and other related materials We supply to You in any form pursuant to this Agreement for aiding the use of the Services;

"Group Company" means (in relation to each party) any subsidiary, group or parent company from time to time of a party (as such words are defined in the Companies Act 2006);

"IPR" means all intellectual property rights including, without limitation, all patents, copyright, design rights, database rights (including rights in the design or structure of any database) trade marks, confidential know-how, database rights and all other similar rights (whether registered or unregistered) and all applications for the same anywhere in the world;

"License" means the right of access to the Services specified in clause 2;

"Reseller" means an approved reseller for the Services appointed by Us;

“Services” meansthe services provided by Us to allow You to access Secure Space through the use of internet enabled devices;

"Software" means the computer programs used to provide the Services excluding source code material and all preparatory design material;

"Specification" means the functional specification (if any) for the Services or minimum and/or optimum system environment or hardware specifications for use of the Services published by Us in hard copy format or on the Website;

"Standard Support Hours" means the standard hours during which the Support will be provided as specified in clause 6.4;

"Support" means the support service provided as part of the Services, comprising advice by telephone, email, the Website or other means available to Us (excluding on site visits) as may be appropriate and necessary to resolve Your difficulties and queries in relation to the Services in accordance with clause 6;

"Term" means the duration of this Agreement;

"Third Party Software" means all software owned by a third party but legally licensed for use as part of the Services;

"UK" means the United Kingdom;

"Users" means any employee, temporary staff agent or contractor of Yours that accesses the Services with Your permission;

"Website" means the website at www.securesapce.co.uk

1.2 Interpretation

In the event of any conflict or inconsistency between any of the parts of this Agreement (unless expressly stated otherwise) the terms of the part first appearing below shall prevail to the extent of the inconsistency:

1.2.1 the terms and conditions of the main body of this Agreement;

1.2.3 any other documents expressly incorporated in this Agreement by Us.

2. Grant of License

2.1 Subject to the terms of this Agreement, in consideration of the payment to Us by You of the Charges, We grant You a non-exclusive and non-transferable license ('the License') during the Term to use and access the Services for Your internal business purposes only. For the avoidance of doubt this shall include sharing files with Client End Users.

2.2 We may use a third party hosting service provider in order to provide the Services. If We do, then the standard terms and conditions of that third party hosting service provider will apply and will take precedence over the terms of this Agreement to the extent that there is a conflict between the terms of this Agreement and those of that third party hosting service provider, except for clause 11, which shall take precedence over the standard terms and conditions of that third party hosting service provider.

2.3 We may offer you a level of the Services that is without charge. If you are using a free level of the Services, you will not be able to access the Services if you exceed the level of use designated as without charge. Your use of the Services at a level that is without charge shall be governed by the terms and conditions of this Agreement. We reserve the right to withdraw or modify the offer of a level of Services without charge at any time.

2.4 If You have been introduced to or provided with access to the Services through a Reseller;

2.4.1 the Reseller's own terms and conditions will apply to Your relationship with them but Your relationship with Us will continue to be governed by this Agreement; and

2.4.2 if You have contracted with a Reseller for the performance of any other services, then We will not be obliged to perform those services; and

2.4.3 We may perform any of Our obligations under this Agreement through the Reseller.

3. Permitted Use

3.1 The License will be restricted to the license metrics agreed during registration, which will be one, unless otherwise agreed.

3.2 You may increase or decrease the number of Users and Client End Users covered by the License during the Term of this Agreement.

3.3 You may access and use the Services for processing Your own data and any Client End User data including files.

3.4 You shall not:

3.4.1 use or attempt to use the Services or permit any third party to do so to provide a data processing service to any third party, or otherwise, or contrary to any other restrictions stated in this Agreement without Our prior written consent, provided that nothing in this sub-clause shall prevent You from using the Services to process Client End User data;

3.4.2 translate or adapt the Services for any purpose nor arrange or create derivative products or works based on the Services without Our express prior written consent in each case;

3.4.3 transfer or distribute (whether by license, loan, rental, sale or otherwise) or otherwise deal in, charge or encumber all or any part of the Services to any other person or use the Services on behalf of any third party or make available the same to any third party, provided that nothing in this sub-clause shall prevent You from using the Services to process Client End User data;

3.4.4 make, or permit any third party to make for any purpose (including without limitation for error correction), any alterations, modifications, additions or enhancements to the Services;

3.4.5 yourself or permit any third party to, alter, adapt, make error corrections to, decompile, reverse engineer or disassemble the Services or permit the Services to be combined with any other programs.

3.5 You shall follow all lawful and reasonable instructions and directions given by Us from time to time in relation to the use of the Services.

3.6 We shall be entitled to inspect Your use of the Services at any time without prior notice. If necessary, We may require You to operate and run a tool or programme provided by Us on Your equipment in order to verify that Your use of the Services complies with the terms of this Agreement.

3.7 You shall use appropriate hardware and software to access the Services in accordance with the specifications notified to You and will take appropriate security precautions to prevent unauthorised access to Your computer systems.

3.8 You may not access or use the Services other than as specified in this Agreement without Our prior written consent.

3.9 You shall not make or permit others to make any copies of the Documentation without Our prior written consent, excluding the printing of help files which is permitted in so far as the making of such copies are necessary for the use of the Services permitted by the License. Such copies will belong to Us.

4. Services

4.1 We shall use reasonable endeavours to maintain twenty four (24) hour online presence for the Services. However, we cannot guarantee continuous, uninterrupted use. There will be times when We will be required to interrupt the provision of the Services in order to carry out routine maintenance, repairs, reconfigurations or upgrades on a regular basis or in circumstances beyond Our control. We shall notify You in advance of any planned interruptions.

4.2 We may suspend the Services without notice and without any liability to You if:

4.2.1 the Services are being used in breach of this Agreement (including without limitation failure by You to pay any Charges due to Us in respect of Your use of the Services (or that of any Client End User));

4.2.2 there is a breach of security in respect of which We reasonably believe that the suspension of the Services is necessary to protect Your or Our network or a third party network;

4.2.3 due to unavailability of third party networks and/or services, including without limitation telecommunications and ISP services; or

4.2.4 if required by law, regulation or court order or as compelled by a law enforcement or government agency or other relevant regulatory agency.

4.3 In the event the hosting service provider suspends its service due to Your act or omission (or that of any Client End User), We reserve the right to charge You for a reconnection fee prior to resuming provision of the Services.

4.4 We may from time to time upgrade Our hosting facility and it may become necessary to relocate the hosting equipment within the same location or to another location. In each such case, We shall give You reasonable advance notice and use reasonable endeavours to minimise the effect that any such change will have on the Services.

4.5 You shall (a) provide a communications device of the type specified by Us; and (b) arrange appropriate internet access for all Users. Such internet access shall be in accordance with the Specification (if any). We shall not be liable for any failure to provide the Services if You fail to comply with this clause.

4.6 We shall use commercially reasonable efforts to safeguard and accurately maintain Your data stored as part of the Services in accordance with general industry standards.

4.7 The provision of the Services is subject to any limit on the amount of disk space and/or bandwidth made available to You and/or any Client End User. If You exceed this usage limit You may, at Our discretion, either (a) purchase extra disk or bandwidth capacity at Our standard rates from time to time, where possible or (b) purge unwanted data from the system with Our guidance, for which We shall be entitled to charge in accordance with Our standard daily rates for those services from time to time. Repeated or material breaches of this clause 4.7 may result in termination of this Agreement by us under clause 10.4.1.

4.8 By using the Services you consent to your data to be uploaded to the on-line system and acknowledge and agree that the accuracy or completeness of such upload of data is not guaranteed nor warranted and You shall be responsible for backing up your data prior to starting the upload process. You also acknowledge and agree that in the event You delete your data whether intentionally or accidently such data will be permanently deleted and You will be unable to retrieve such deleted data.

5. Proprietary rights

5.1 All copyright, database rights and other IPR in the Services or Documentation and rights in any copies of them shall belong to Us and You shall have no rights in respect of any of them except the right, as expressly granted under this Agreement, to use them in accordance with this Agreement. You shall do or procure to be done all such further acts and things and shall execute or procure the execution of all such other documents as We may from time to time require for the purpose of giving Us the full benefit of the provisions of this clause.

5.2 You agree not to remove, suppress or modify in any way any proprietary marking, including any trade mark or copyright notice, on or in the Website or any physical media or on any Documentation. You agree to include any pre-existing proprietary marks in any copies of the Documentation made by You in compliance with this Agreement. Nothing in this sub-clause shall prevent You from applying Your own branding to the Services through the use of the customisation features of the Service.

5.3 You shall notify Us immediately if You become aware of any unauthorised access to, use, of any part of the Services or Documentation.

6. Support

6.1 In consideration of Your payment of the Charges and the performance of all Your other obligations pursuant to this Agreement, We agree to provide the Support in accordance with the terms of this Agreement.

6.2 You are required to notify Us of any support issues referred to in clause 6.3 that You or any Client End Users may have. We shall provide Support to You directly and to Your Client End Users only indirectly through You, unless otherwise agreed.

6.3 Support covers assistance in relation to (i) availability of the Service and (ii) significant operational errors that make the Services unusable when operated in conformity with the online user instructions in the help function in the program or the Documentation (as the case may be). Such errors or unavailability shall be notified by You to Our Customer Support department as published on the Website. We will use reasonable endeavours to attempt to correct or assist You or your Client End Users indirectly through You to avoid errors or unavailability thought to be suitable to the problem or at Our option resort to other means toward a mutually satisfactory solution. For the avoidance of doubt, Support does not include support for Third Party Software or hardware or problems relating to internet connectivity.

6.4 The Support will be provided during the hours published on the Website excluding UK public holidays and where applicable any Republic of Ireland public holidays and any company shutdowns. Any such company shutdowns will be notified in advance on the Website.

6.5 The provision of any Support outside the Standard Support Hours is at Our sole discretion. Charges in respect of all time spent in providing any Support to You and/or Your Client End Users outside the Standard Support Hours will be invoiced to You at Our discretion at Our then current rates.

6.6 We will use reasonable endeavours to provide the Support promptly having regard to the availability of personnel, necessary supplies and facilities.

6.7 If You and/or Your Client End Users make unreasonable, excessive or inappropriate use of the Support, then We may at Our absolute discretion either suspend or charge extra for such Support and invoice You, and You agree to pay, for the additional Charges in respect of time spent supplying such Support at the then current rates.

6.8 The Support is compulsory as part of Services.

6.9 It is not within the scope of Our obligations to enquire as to, or to verify the accuracy or completeness of information that We receive from You or any third parties. We shall not be liable for any failure or delay in providing Services in the event of Your failure to provide all requested information and data fully and accurately within the required timescales.

6.10 We will use reasonable endeavours to ensure that the Services are supplied promptly or (if applicable) by the Delivery Date or such other dates as agreed by the parties but any delivery dates or times quoted for delivery, commencement or completion of any part of the Services will be estimates only and time will not be of the essence.

6.11 The provision of Consultants or Our agents to perform Our obligations under this Agreement shall be at Our discretion.

7. Customer obligations

7.1 You undertake:

7.1.1 to satisfy Yourself that the Services meet the needs of Your business. If You are not qualified to make these assessments Yourself, it is Your responsibility to engage the services of someone with requisite expertise who can make that assessment for You;

7.1.2 ensure that the licence metrics do not at any time exceed those agreed during the registration process;

7.1.3 to maintain accurate and up-to-date records of the number and locations of all Users;

7.1.4 to ensure each User keeps a secure password for their use and access to the Services and does not disclose it to any third party;

7.1.5 not to store, distribute or transmit any viruses or unsolicited emails, or any material through the Services that are unlawful, harmful, threatening, defamatory, obscene, infringing, harassing or racially or ethnically offensive; facilitate illegal activity; depict sexually explicit images; or promote unlawful violence, discrimination based on race, gender, colour, religious belief, sexual orientation, disability, breach third party intellectual property rights; include personal data of any person that was not collected or is not being stored in accordance with applicable legislation and guidance from regulatory authorities, breach third party confidentiality or privacy rights or any other illegal or actionable activities;

7.1.6 not to carry out any activity that interferes with Our other customers' use of the Services;

7.1.7 to comply with the acceptable usage policy as may be notified to You by Us from time to time;

7.1.8 to comply with all licensing terms in respect of any Third Party Software You will indemnify Us for any breach of this clause 7.1.8;

7.1.9 not to provide or otherwise make available the Services in whole or in part (including, but not limited to, program listings, object and source program listings, object code and source code) in any form to any person other than Your employees, temporary staff, agents or sub-contractors or your Client End Users who need it for the purposes of this Agreement without Our prior written consent;

7.1.10 to accept full responsibility for the acts or omissions of any of Your employees, sub-contractors, consultants and/or agents or your Client End Users given access to the Services and/or Documentation as if they were Your acts or omissions;

7.1.11 to comply with all applicable laws and regulations in relation to Your activities under this Agreement;

7.1.12 to allow Us to study Your information and data used with the Services for the purpose of rectifying any problems with the Services in relation to provision of Support;

7.1.13 to ensure that the operating system and compiler and any other software with which the Services will be used is either Your property or it is legally licensed to You for use with the Services. You will indemnify Us in respect of any claims by third parties and all related costs, expenses or damages in the event of any alleged violation of third party proprietary right which results in any claims against Us;

7.1.14 to ensure that Users log in using their own username and password only and do not share usernames and passwords, and if You believe that any other User or a third party may have obtained a User's username or password, please report the matter to Us immediately by telephoning Our helpdesk;

7.1.15 to be responsible for ensuring that the Services are compatible with Your existing systems and software. You must only access the Services using a broadband internet connection. We shall not be required to configure the Services or Your systems and software (or those relating to any Client End User) to be compatible with one another. If, in Our sole discretion, We agree to do so, You shall pay Us additional Charges in respect of time spent on such work at the then current rate;

7.1.16 not to bypass any security and/or access feature of the Services;

7.1.17 make daily back-ups of Your data as necessary and to the extent possible to ensure that Your data can be restored in the event of a need for disaster recovery;

7.1.18 to be solely responsible for the accuracy of Your data; and

7.1.19 to ensure that Your own system or equipment or any files that You may upload as part of using the Services do not contain any trojan horse, worm, logic bomb, time bomb, back door, trap door or other common viruses and You shall indemnify Us in respect of any liability We incur as a result of Your breach of this clause.

7.2 In relation to the Support, You shall:

7.2.1 use all reasonable endeavours to ensure that the Services are used in a proper manner by competent trained employees only or by persons under their supervision;

7.2.2 notify Us promptly by notice in writing if the Services are not operating correctly or problems with the availability of the Services;

7.2.3 co-operate to a reasonable extent with Our staff as reasonably required to perform the Support;

7.2.4 designate primary and secondary contacts and procure that the contacts whose details are submitted during registration shall be those who deal with Us with regard to any matters reported in connection with the Support and the only contacts authorised to use the telephone helpline, and inform Us as soon as reasonably possible if contacts or their details change

7.3 If any of Our staff work on Your premises, You will ensure that Our staff are provided with suitable and safe office accommodation, suitable services (including telephone, facsimile and photocopying facilities) and any computing and ancillary facilities, and use free of charge such items, third party software, facilities and services (if any) as may be required to perform the Support.

7.4 We reserve the right to refuse to provide any Services to You, if in Our sole opinion You are abusive to Our staff, do not comply with clause 7.3 or Your other obligations under this Agreement.

7.5 You shall indemnify Us against any losses, damages, costs (including legal and other professional fees) and expenses incurred by or awarded against Us as a result of Your breach of this Agreement or any negligent or wrongful act by You or Your officers, employees, contractors or agents.

7.6 You hereby acknowledge and agree that this Agreement will not be enforceable against any Group or associated company of the Alchemy Group, and Your sole recourse and/or any rights or remedies You may have whether in contract, tort or otherwise arising from Our failure to comply with the terms of this Agreement will be against Us alone.

7.7 You will promptly provide Us with full and accurate information, data and explanations as and when Support is required. Where applicable You will also provide Us appropriate test scripts, tests and test data.

7.8 You shall procure all necessary rights from third parties (including, without limitation intellectual property licences in relation to computer software) which are from time to time required in order for Us to be able to provide the Services.

7.9 If We are delayed or impeded or obliged to spend additional time or incur additional expenses in the performance of any of Our obligations under this Agreement or any agreement with Your Client End Users, by reason of Your (or their) acts or omissions (including the provision of any incorrect or inadequate data or the provision delay or failure to provide information or instructions or perform Your (or their) obligations under this (or their) Agreement), then You shall pay Us any additional reasonable costs and expenses incurred by or on Our behalf and any timetable agreed for the performance by Us of any of Our obligations shall be extended accordingly.

7.10 You accept and acknowledge that We are not responsible for the acts or omissions of any third party suppliers, including but not limited to telecommunications, third party hosting providers, internet service providers and/or Your third party suppliers.

7.11 You shall not re-sell or permit the resale directly or indirectly (whether or not for profit) of the Services (or any part of either) to any third party, or to allow any third party to receive or make use of the Services (or any part of either) directly or indirectly (whether or not for profit).

7.12 We shall follow the archiving procedure for Your data as may be notified to You. In the event of any loss or damage to Your data, Your sole and exclusive remedy shall be for Us to use reasonable endeavours to restore the lost or damaged data from the latest back-up of such data maintained by us in accordance with the archiving procedure.

7.13 We are not responsible for virus checking files that You and/or other users upload to or download as part of the Services provided to You or them. We shall not be responsible or liable for any loss, destruction, alteration or disclosure of Your data or damage to your business systems, software or hardware caused by any third party whether contracted by Us or otherwise, including where a third party has introduced a virus to the Services by uploading or downloading files while using the Services. For the avoidance of any doubt, the data storage facility is provided by a third party and We do not accept responsibility or liability howsoever arising for any loss, destruction, alteration or disclosure of Your data.)

7.14 You will notify Us of each new Client End User that You wish to be registered to use the Services so that we can initiate the set-up process in respect of each such new user.

8. Supplier Obligations

8.1 We will use reasonable care and skill in performing and providing the Services.

8.2 We will investigate any failure or error in any provision of Services provided that You notify Us in writing within seven (7) days following You becoming aware of such failure or error giving Us all necessary information to be able to investigate the failure or error and We limit Our liability to an obligation to use reasonable commercial endeavours to correct such failure or error.

8.3 We will not be liable for Support unless You notify Us in accordance with clause 8.2 or if the failure or error has been caused by incorrect use or abuse or corruption of the Services.

8.4 Except as expressly provided in this Agreement no further warranty, condition, undertaking or term, express or implied, statutory or otherwise as to the condition, quality, availability, reliability, suitability, performance or fitness for purpose of the and Services provided hereunder is given or assumed by Us.

9. Liability

9.1 Nothing in this Agreement shall in any way exclude or limit Our liability for death or personal injury caused by negligence, or liability for fraudulent misrepresentation, or for any breach of Our obligations as to title under section 12 of the Sale of Goods Act 1979 or section 2 of Supply of Goods and Services Act 1982 or for any other liability which by law it is not possible to exclude or limit.

9.2 Our liability for the loss or damage to tangible property, whether or not the same are under warranty, shall be limited in accordance with clause 9.3 of this Agreement.

9.3 Subject to sub-clauses 9.1 and 9.6 below, Our total liability in any contract year for direct losses in contract, tort, misrepresentation or otherwise in connection with this Agreement and/or the provision of the Services, shall be limited to the total Charges paid (excluding VAT and expenses) by You to Us in that contract year or£5,000, whichever is the greater.

9.4 We shall have no liability to You in respect of defaults covered by clause 9.3 unless You notify Us within six (6) months of the date You became aware of the circumstances giving rise to the event(s) complained of. We shall have not less than ninety (90) days following written notice by You) or such other notice period notified to You in which to remedy any default.

9.5 In no event will We be liable to You in contract, tort, misrepresentation or otherwise, for any indirect or consequential loss or damage, costs, expenses or other claims for consequential compensation whatsoever, nor for any direct or indirect loss of profit, loss of anticipated profits, loss of revenue, loss of anticipated revenue, loss of savings or anticipated savings, loss of business opportunity, increases in cost of working whether anticipated or not, loss or corruption of data, loss of use or loss of operating time and any costs and expenses associated therewith, depletion of goodwill or reputation or otherwise which arise out of or in connection with this Agreement and whether or not foreseeable or made known to Us.

9.6 We shall indemnify You against any claim that the normal use or possession of the Services or Documentation infringes the intellectual property rights of any third party which are effective in the UK provided We are notified promptly of any claim, We are given control of any claim, You do not prejudice Our defence of any claim and You give Us all reasonable assistance (at Our reasonable cost) and that the claim does not arise as a result of (a) the use of the Services or Documentation in combination with equipment or software not approved by Us, (b) by reason of alteration or modification not approved by Us or (c) where the claim arises because of a feature specified and requested by You. We shall have the right to procure the continuing use of the infringing part, modify or replace the infringing part provided that exercise of any of these options shall operate as an entire discharge of Our liability to You under this sub-clause.

9.7 You will indemnify and keep Us (and any of Our Group Companies) indemnified against any loss, damage, claim or expense arising out of (i) the physical injury of or death of any of Our consultants, employees, agents or authorised representatives arising by reason of defective equipment supplied by You, Your failure to provide a safe place of work or otherwise by reason of any negligent act or default on Your part or Your employees, agents or authorised representatives; (ii) Your failure to comply with the terms and conditions governing the use of any Third Party Software; (c) any claim that the storage of Your data via the Services by Us infringes the intellectual property rights of any third party.

10. Term & Termination

10.1 This Agreement will commence on the Commencement Date. The Services will continue until terminated in accordance with these terms.

10.2 We may terminate this Agreement (or at Our discretion, the supply to You of any Services) (a) immediately if You fail to pay any sum due to Us under this Agreement; or (b) on sixty (60) days prior written notice.

10.3 Unless You are using OpenSpace as a monthly subscription service, You may only terminate this Agreement by giving not less than ninety (90) days' prior written notice upon which You will not be entitled to any refund of any Charges paid under this Agreement. If You are using OpenSpace as a monthly subscription service, You may only terminate this Agreement at the end of a monthly term.

10.4 Either party shall be entitled to terminate this Agreement forthwith by notice in writing to the other if the other:

10.4.1 is in material breach of this Agreement and either that breach is incapable of remedy, or the other party fails to remedy the breach within thirty (30) days of receipt of written notice setting out the breach and indicating that failure to remedy the breach may result in termination of this Agreement;

10.4.2 becomes the subject of a voluntary arrangement under section 1 of the Insolvency Act 1986, or is unable to pay its debts within the meaning of Section 123 of the Insolvency Act 1986, or notice has been received of a pending appointment of or the appointment of a receiver, manager, administrator or administrative receiver over all or any part of its undertaking, assets or income, intends to pass or has passed a resolution for its winding-up, or has a petition presented to any court for its winding-up or for an administration order, or has ceased or threatened to cease to trade.

10.5 Termination of the Agreement, however caused, shall not affect the rights of either party under this Agreement which may have accrued up to the date of termination.

10.6 On termination of this Agreement however caused the License shall terminate and accordingly Your right to use the Services will automatically cease. If the Agreement is terminated by You pursuant to clause 10.4.1, We will, at Our sole discretion, either make Your data available to You in standard readable form via email, CD-ROM or DVD or allow You access to the Services for a limited period following such termination to retrieve and store a read only copy of Your data, after which Your data will be permanently deleted. If the Agreement is terminated by Us or by You under any other clause, then We reserve the right to make a reasonable charge for such service or access.

11. Payment terms

If you are using OpenSpace under an annual contract purchased through your account manager.

11.1 You agree to pay Us the Charges within 30 days of the date of invoice or if agreed otherwise in accordance with the terms for payment set out in the Invoice, Project Proposal or the terms for payment set out in any invoice We send to You.

11.2 Where the Term is for periods of 3 months or less:

11.2.1 You agree to pay Us the Charges invoiced by Us monthly in advance in three (3) equal instalments, the first such payment being due on or prior to the Commencement Date and subsequent payments being due monthly thereafter on the same day each month until expiry of the Term.

11.2.2 Activation of the Services will automatically expire at the end of the Term.

11.3 All Charges payable by You to Us should be paid in full in accordance with terms stated on Our invoice.

11.4 All amounts due under this Agreement are exclusive of VAT and any other taxes, duties or levies and where relevant are exclusive of any travel, subsistence and other out-of-pocket expenses reasonably incurred by Us in respect of the provision of such Professional Services, which We shall be entitled to invoice to You, and You agree to pay, at Our then current rates for such Professional Services.

11.5 We will be entitled to increase any Charges due under this Agreement, by giving You notice in the renewal invoice We send to You or if sooner by giving You thirty (30) days prior notice of such increase. In the event that You pay any amount due under this Agreement in instalments You agree that any increase in price during any relevant Term due to any change requested by You will be applied pro-rata to all subsequent instalments.

11.6 We will not activate the Services until payment of all Charges due under this Agreement have been received by Us in full and in cleared funds.

11.7 If any payment due under this Agreement or any other Agreement with Us is or are in arrears, We reserve the right without prejudice to any other right or remedy to:

11.7.1 charge interest on such overdue sum on a daily basis from the original due date until payment is received in full as well as after any judgment at a rate of 3% per annum above Lloyds Bank plc's base lending rate in force from time to time and alternatively reserve the right to claim interest under the Late Payment of Commercial Debts (Interest) Act 1998; and/or

11.7.2 suspend the provision of the Services and/or Support or limit access to the Services and/or Support under this Agreement with or without prior notice to You; and/or

11.7.3 withhold the activation of the Services until any outstanding payment is both received and cleared; and/or

11.7.4 terminate this Agreement pursuant to clause 10.

11.8 You will notify Us in writing within fourteen (14) days of receipt of an invoice if You consider such invoice incorrect or invalid for any reason and the reasons for withholding payment, failing which such invoice will be deemed accepted and You will make full payment in accordance with it.

11.9 We reserve the right to refuse to provide Services at any time without refunding the Charges paid by You, if any attempt is made, other than by Us, to tamper with the Services, or if You have failed to pay an invoice from Us in accordance with this Agreement or where, in Our reasonable opinion, Your system and/or the equipment has ceased to be capable of receiving the Services successfully for any reason.

11.10 If Your use of the storage facilities of the Software and/or Services is unreasonable, excessive or inappropriate, then We may at Our absolute discretion either suspend or make extra Charges for such Services and invoice You, and You agree to pay, for the additional Charges at the then current rates.

If you are using OpenSpace as a subscription service:

11.11 You are required to enter your payment details directly into the Service.

11.12 You agree to be charged for Your access to the Services and that of Your End User Clients on a monthly basis by us using the payment details referred to in clause 11.11.

11.13 You also agree to be charged in accordance with Your data storage usage on a monthly basis and You may increase the data storage capacity available to you at any time through the client portal accessed as part of the Services.

11.14 Subject to clause 11.18, activation of the Services shall take effect and continue on and subject to payment as set out in 11.11 to 11.13.

11.15 The Services will be deactivated when you reach Your data storage limit unless You have paid to increase that limit in accordance with 11.13.

11.16 We will be entitled to increase any Charges due under this Agreement by giving You notice which may be posted on our Website.

11.17 We will not activate the Services until payment of all Charges due under this Agreement have been received by Us in full and in cleared funds.

11.18 We reserve the right to refuse to provide the Services at any time without refunding the Charges paid by You, if any attempt is made, other than by Us, to tamper with the Services, or where, in Our reasonable opinion, Your system and/or the equipment has ceased to be capable of receiving the Services successfully for any reason.

11.19 If Your use of the data storage facilities is unreasonable, excessive or inappropriate, then We may at Our absolute discretion either suspend or make extra Charges for such Services and You agree to pay, for the additional Charges promptly at the then current rates.

12. Force majeure

No party shall be liable to the other for any delay or non-performance of its obligations under this Agreement arising from any cause beyond its control. For the avoidance of doubt, nothing in this clause 12 shall excuse You from any payment obligations under this Agreement. If any such event continues for more than ninety (90) days and provided substantial performance is still impeded either party may terminate this Agreement forthwith by prior written notice without prejudice to the accrued rights of either party.

13. Assignment

We may assign, sub-contract or otherwise transfer any of Our rights or obligations under this Agreement without Your consent. You may only assign, sub-contract or otherwise transfer any of Your rights or obligations with Our prior written consent. If that is to an outsourcing provider, the Software must remain in the UK and the outsourcing provider must dial in to the Software to meet its obligations.

14. Notices

14.1 Any notice required to be given pursuant to this Agreement shall unless otherwise stated in it, be in writing, sent to the other party marked for the attention of the person at the address specified in this Agreement (or to such other address as either party may from time to time notify to the other in writing in accordance with this clause).

14.2 For the purpose of notices to be given by Us in writing, the expression "writing" or "written" shall be deemed to include email communications or facsimile transmissions. At Our option, We may send You written notice addressed to the facsimile number or by email at the email address You supply to Us specified in the Invoice or Project Proposal.

14.3 A correctly addressed notice sent by first-class post shall be deemed to have been delivered 72 hours after posting, correctly directed faxes shall be deemed to have been received instantaneously on transmission, and correctly addressed emails shall be deemed to have been delivered 24 hours after sending.

15. Severability

If any provision of this Agreement is judged to be illegal or unenforceable, the continuation in full force and effect of the remainder of the provisions shall not be prejudiced.

16. Waiver

No forbearance or delay by either party in enforcing its rights shall prejudice or restrict the rights of that party and no waiver of any such rights or of any breach of any contractual terms shall be deemed to be a waiver of any other right or of any later breach.

17. Entire Agreement

17.1 This Agreement and any document expressly incorporated in it contains the entire and only agreement between the parties and supersedes all previous agreements between the parties with respect to the subject matter hereof. Each party acknowledges that in entering into this Agreement, it has not relied on any representation, undertaking, promise or statement whether oral or in writing which is not expressly set out in this Agreement. Except as expressly provided in this Agreement all conditions, warranties, stipulations and other statements whatsoever that would otherwise be implied or imposed by statute, at common law, or otherwise howsoever are excluded to the fullest extent permitted by law. Nothing in the foregoing shall however affect any liability for fraudulent misrepresentation.

17.2 You will be notified of any changes to this Agreement on the Website. Changes to the Agreement will take effect immediately upon such notification.

18. Third party rights

A person who is not party to this Agreement shall have no right under the Contracts (Rights of Third Parties) Act 1999 to enforce any term of this Agreement.

19. Data Protection

19.1 Each party shall comply with its obligations under the Data Protection Act 1998 ("Act"). For the purposes of this Agreement We shall be a data processor and you shall be a data controller, both as defined under the Act. Neither party shall by any act or omission, deliberately put the other party in breach of the Act and each party shall do and execute, or arrange to be done and executed each act, document and thing necessary or desirable to ensure that it does not put the other party in breach of the Act. You shall indemnify Us for any breach by You of this clause 19.1.

19.2 We will use Your personal details and any information We obtain from You or other sources to provide You with Our Services, for administration and customer services, to analyse Your purchasing preferences and to ensure that the content, services and direct marketing that We offer are tailored to Your needs and interests. We may keep Your information for a reasonable period for these purposes. We may disclose Your personal information to any member of Our group, which means Our subsidiaries, Our ultimate holding company and its subsidiaries as defined in section 1159 of the Companies Act 2006, to joint venture partners and resellers of any of Our group companies, Our service providers, partners and agents for these purposes. They or We may use Your information to write, phone or contact You by other means to offer products and services or promotions. Any information provided to third parties for this purpose will be under Our strict supervision and within the requirements on Us under the Act. We may transfer Your information outside the UK if necessary for the above purposes and You acknowledge that if the receiving country is outside the European Economic Area, it may not have the same standards of data protection as the UK. We may record telephone calls to improve Our service to You and assist with training. The information You give Us is not sold on to other companies and We have a serious and dedicated commitments to the security of Your information. By submitting Your personal details You confirm that You consent to the contents of this clause and that You will obtain all necessary licences and consents to enable Us to use your data as set out in this clause and you shall indemnify Us against any liability that we may incur as a result of Your failure to obtain such licences and/or consents.

19.3 You have a right at any time to stop Us from contacting You for direct marketing by writing to Us at Our registered office.

19.4 By using our Services You agree to the terms of Our Privacy Policy set out within the client portal accessed as part of the Services.

20. Confidentiality

20.1 Both parties agree not to use or disclose confidential information relating to or owned by the other, received or disclosed to it by the other party during the Term of this Agreement, save for use or disclosure required in order to perform their respective obligations under this Agreement. Disclosure shall be limited to such of the receiving party's employees, officers, agents or contractors directly involved in performing the receiving party's obligations.

20.2 The parties agree that information is not to be regarded as confidential and that the receiving party will have no obligation regarding confidentiality where that information is already in the public domain or enters the public domain through no fault of the receiving party, or is received from a third party without any obligations of confidentiality, or is used or disclosed with the prior written consent of the owner of that information, or is disclosed in compliance with a legal requirement, or is independently developed by the receiving party.

20.3 Any confidential information will be returned or destroyed by the receiving party forthwith at the prior written request of the owner.

20.4 We will be allowed to refer to You in any publicity after performance of the Services.

20.5 In the event that the parties execute a separate confidentiality agreement, the terms of that agreement shall prevail.

21. Governing law

This Agreement shall be construed in accordance with and governed by the law of England and Wales and each party agrees to submit to the exclusive jurisdiction of the courts of England and Wales.

221. File sharing

The Services include the ability for You to share files with third parties including your Client End Users and for them to share files with You. You agree to obtain all necessary licences and consents to enable Us to share such files between You and such third parties and You shall indemnify Us against any liability that we may incur as a result of Your failure to obtain such licences and/or consents. We accept no responsibly for the content of files uploaded by You or any third parties. While We provide a document storage and exchange service this does not involve checking for malicious software, which shall be Your responsibility. We reserve Our rights to remove any files that You and/or such third parties may share or immediately disable Your access and/or terminate Your account should Your file sharing activities (or those of such third parties) be deemed in Our sole discretion to be inappropriate and/or threaten the security of other customers.
</p>
