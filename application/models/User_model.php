<?php
class User_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function check_user_login($email,$password){
		$this->db->where( array('email'=>$email,'password'=>md5($password) ) );
		$query = $this->db->get('users');
		
		return $query;
	}
	public function save($table,$data){
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}
	public function update($table,$where, $data){
		$this->db->update($table, $data, $where);
		return $this->db->last_query();
	}
	function getResultsByCondtion($table,$condition){
		$this->db->where($condition);
		$query = $this->db->get($table);
		return $query;
	}
	public function getSingleRecord($table,$condition){
		$this->db->where($condition);
		$query = $this->db->get($table);
		return $query;
	}
	public function delete_by_condition($table,$where){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function insert_venues($data){
		$post_data = array(
						'venues_space_id'	=>	$data['space_id'],
						'ownership_type'	=>	$data['ownership_type'],
						'space_title'		=>	$data['space_title'],
						'space_description'	=>	$data['space_description'],
						'space_area'	=>	$data['space_area'],
						'total_rooms'	=>	$data['total_rooms'],
						'available_days'	=>	$data['available_days'],
						'available_from'	=>	$data['available_from'],
						'available_to'	=>	$data['available_to'],
						'hourly_rate'	=>	$data['hourly_rate'],
						'discount'	=>	$data['discount'],
						'capacity'	=>	$data['capacity'],
						'hours'	=>	$data['hours'],
						'amenities'	=>	$data['amenities'],
						'lattitude'	=>	$data['lattitude'],
						'longitude'	=>	$data['longitude'],
						'location'	=>	$data['location'],
						'user_id'	=>	$data['user_id'],
					);
		$this->db->insert('venues',$post_data);
		$last_id=$this->db->insert_id();
		/*
		$this->db->where( array('user_id'=>$data['user_id'],'venue_id'=>'0') );
		$this->db->update('venue_gallery',array('venue_id'=>$last_id));*/
		
		return $last_id;
	}
	
	function get_venues_spaces_list(){ 
		$venue_spaces = $this->db->get('venues_spaces');
		return $venue_spaces->result();
	}

	public function get_venues_by_user_id($user_id){
		$sql = "SELECT V.*,VS.space_title as plan_title,(SELECT file_name from venue_gallery where venue_gallery.venue_id=V.id limit 1) as image FROM venues AS V INNER JOIN venues_spaces VS on VS.id=V.venues_space_id WHERE V.user_id='".$user_id."'";
		$result = $this->db->query($sql);
		return $result;
	}

	public function filter_venues($data){
		$where = '1=1';
		if(isset($data['p']) && !empty($data['p'])  ){
			$where .= " AND LOWER(V.ownership_type) ='".strtolower($data['p'])."' ";
		}
		if(isset($data['n']) && !empty($data['n'])){
			$where .= " AND V.capacity >='".$data['n']."'";
		}

		if(isset($data['space']) && !empty($data['space'])){
			
			$where .=" AND V.venues_space_id IN (".implode(',', $data['space']).") ";
		}

		if(isset($data['sqr']) && !empty($data['sqr'])){
			$where .= " AND V.space_area >='".$data['sqr']."'";
		}

		if(isset($data['high']) && !empty($data['high'])){
			$low = (isset($data['low']) && !empty($data['low'])) ? $data['low'] : 1;
			$where .= " AND V.hourly_rate BETWEEN '".$low."' AND '".$data['high']."'";
		}


		$sql = "SELECT V.*,VS.space_title as plan_title,(SELECT file_name from venue_gallery where venue_gallery.venue_id=V.id limit 1) as image FROM venues AS V INNER JOIN venues_spaces VS on VS.id=V.venues_space_id WHERE ".$where;
		$result = $this->db->query($sql);

		//echo $this->db->last_query();exit;
		return $result;
	}

	public function get_venue_by_id($venueId,$userId){
		$sql = "SELECT V.*,VS.space_title as plan_title FROM venues AS V INNER JOIN venues_spaces VS on VS.id=V.venues_space_id WHERE V.user_id='".$userId."' AND V.id='".$venueId."'" ;
		$result = $this->db->query($sql);
		//echo $this->db->last_query();
		return $result;
	}
	
}