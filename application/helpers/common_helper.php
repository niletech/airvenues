<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('site_setting')){
    function site_setting(){
        $ci =& get_instance();
        $ci->load->model(array('user_model','site_model'));
        $site_details   =   $ci->user_model->getResultByCondtion('site_setting',array('id'=>'1'));
        return $site_details;
    }   
}
if ( ! function_exists('getUserDetails')){
    function getUserDetails($user_id){
        $ci =& get_instance();
        $ci->load->model(array('user_model'));
        $query   =   $ci->user_model->getUserPractice($user_id);
		return $query;
    }   
}
if ( ! function_exists('getFileStatusTotal')){
    function getFileStatusTotal(){
        $ci =& get_instance();
        $ci->load->model(array('user_model'));
        $query   =   $ci->user_model->get_approval_documents_count();
		return $query->row();
    }   
}
if ( ! function_exists('userViewRestriction')){
	function userViewRestriction($groupID){
		$ci =& get_instance();
		$user_group = $ci->session->userdata('user_group');
		if(in_array($user_group, $groupID) ){
			return true;
		}else{ return false;}
	}
}

if ( ! function_exists('remove_directory')){
    function remove_directory($directory, $empty=FALSE){
        if(substr($directory,-1) == '/') {
            $directory = substr($directory,0,-1);
        }

        if(!file_exists($directory) || !is_dir($directory)) {
            return FALSE;
        } elseif(!is_readable($directory)) {

        return FALSE;

        } else {

            $handle = opendir($directory);
            while (FALSE !== ($item = readdir($handle)))
            {
                if($item != '.' && $item != '..') {
                    $path = $directory.'/'.$item;
                    if(is_dir($path)) {
                        remove_directory($path);
                    }else{
                        unlink($path);
                    }
                }
            }
            closedir($handle);
            if($empty == FALSE)
            {
                if(!rmdir($directory))
                {
                    return FALSE;
                }
            }
        return TRUE;
        }
    }
}